from enum import Enum
# noinspection PyPep8Naming
from .Internal.RepeatedCapability import VALUE_DEFAULT as DefaultRepCap
# noinspection PyPep8Naming
from .Internal.RepeatedCapability import VALUE_EMPTY as EmptyRepCap


# noinspection SpellCheckingInspection
class Channel(Enum):
	"""Repeated capability Channel"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ch1 = 1
	Ch2 = 2
	Ch3 = 3
	Ch4 = 4
	Ch5 = 5
	Ch6 = 6
	Ch7 = 7
	Ch8 = 8
	Ch9 = 9
	Ch10 = 10
	Ch11 = 11
	Ch12 = 12
	Ch13 = 13
	Ch14 = 14
	Ch15 = 15
	Ch16 = 16
	Ch17 = 17
	Ch18 = 18
	Ch19 = 19
	Ch20 = 20
	Ch21 = 21
	Ch22 = 22
	Ch23 = 23
	Ch24 = 24
	Ch25 = 25
	Ch26 = 26
	Ch27 = 27
	Ch28 = 28
	Ch29 = 29
	Ch30 = 30
	Ch31 = 31
	Ch32 = 32
	Ch33 = 33
	Ch34 = 34
	Ch35 = 35
	Ch36 = 36
	Ch37 = 37
	Ch38 = 38
	Ch39 = 39
	Ch40 = 40
	Ch41 = 41
	Ch42 = 42
	Ch43 = 43
	Ch44 = 44
	Ch45 = 45
	Ch46 = 46
	Ch47 = 47
	Ch48 = 48
	Ch49 = 49
	Ch50 = 50
	Ch51 = 51
	Ch52 = 52
	Ch53 = 53
	Ch54 = 54
	Ch55 = 55
	Ch56 = 56
	Ch57 = 57
	Ch58 = 58
	Ch59 = 59
	Ch60 = 60
	Ch61 = 61
	Ch62 = 62
	Ch63 = 63
	Ch64 = 64


# noinspection SpellCheckingInspection
class Colors(Enum):
	"""Repeated capability Colors"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ix1 = 1
	Ix2 = 2
	Ix3 = 3
	Ix4 = 4


# noinspection SpellCheckingInspection
class Component(Enum):
	"""Repeated capability Component"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ix1 = 1
	Ix2 = 2
	Ix3 = 3
	Ix4 = 4
	Ix5 = 5
	Ix6 = 6
	Ix7 = 7
	Ix8 = 8
	Ix9 = 9
	Ix10 = 10
	Ix11 = 11
	Ix12 = 12
	Ix13 = 13
	Ix14 = 14
	Ix15 = 15
	Ix16 = 16
	Ix17 = 17
	Ix18 = 18
	Ix19 = 19
	Ix20 = 20
	Ix21 = 21
	Ix22 = 22
	Ix23 = 23
	Ix24 = 24
	Ix25 = 25
	Ix26 = 26
	Ix27 = 27
	Ix28 = 28
	Ix29 = 29
	Ix30 = 30
	Ix31 = 31
	Ix32 = 32


# noinspection SpellCheckingInspection
class CornerFrequency(Enum):
	"""Repeated capability CornerFrequency"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5


# noinspection SpellCheckingInspection
class DeltaMarker(Enum):
	"""Repeated capability DeltaMarker"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class DisplayLine(Enum):
	"""Repeated capability DisplayLine"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class ExternalGen(Enum):
	"""Repeated capability ExternalGen"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8


# noinspection SpellCheckingInspection
class ExternalPort(Enum):
	"""Repeated capability ExternalPort"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3


# noinspection SpellCheckingInspection
class ExternalRosc(Enum):
	"""Repeated capability ExternalRosc"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class FileList(Enum):
	"""Repeated capability FileList"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32
	Nr33 = 33
	Nr34 = 34
	Nr35 = 35
	Nr36 = 36
	Nr37 = 37
	Nr38 = 38
	Nr39 = 39
	Nr40 = 40
	Nr41 = 41
	Nr42 = 42
	Nr43 = 43
	Nr44 = 44
	Nr45 = 45
	Nr46 = 46
	Nr47 = 47
	Nr48 = 48
	Nr49 = 49
	Nr50 = 50
	Nr51 = 51
	Nr52 = 52
	Nr53 = 53
	Nr54 = 54
	Nr55 = 55
	Nr56 = 56
	Nr57 = 57
	Nr58 = 58
	Nr59 = 59
	Nr60 = 60
	Nr61 = 61
	Nr62 = 62
	Nr63 = 63
	Nr64 = 64


# noinspection SpellCheckingInspection
class FilterPy(Enum):
	"""Repeated capability FilterPy"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class FreqLine(Enum):
	"""Repeated capability FreqLine"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4


# noinspection SpellCheckingInspection
class FreqOffset(Enum):
	"""Repeated capability FreqOffset"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8


# noinspection SpellCheckingInspection
class GapChannel(Enum):
	"""Repeated capability GapChannel"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class GateRange(Enum):
	"""Repeated capability GateRange"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32
	Nr33 = 33
	Nr34 = 34
	Nr35 = 35
	Nr36 = 36
	Nr37 = 37
	Nr38 = 38
	Nr39 = 39
	Nr40 = 40
	Nr41 = 41
	Nr42 = 42
	Nr43 = 43
	Nr44 = 44
	Nr45 = 45
	Nr46 = 46
	Nr47 = 47
	Nr48 = 48
	Nr49 = 49
	Nr50 = 50
	Nr51 = 51
	Nr52 = 52
	Nr53 = 53
	Nr54 = 54
	Nr55 = 55
	Nr56 = 56
	Nr57 = 57
	Nr58 = 58
	Nr59 = 59
	Nr60 = 60
	Nr61 = 61
	Nr62 = 62
	Nr63 = 63
	Nr64 = 64


# noinspection SpellCheckingInspection
class Generator(Enum):
	"""Repeated capability Generator"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class InputIx(Enum):
	"""Repeated capability InputIx"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class Item(Enum):
	"""Repeated capability Item"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ix1 = 1
	Ix2 = 2
	Ix3 = 3
	Ix4 = 4
	Ix5 = 5
	Ix6 = 6
	Ix7 = 7
	Ix8 = 8
	Ix9 = 9
	Ix10 = 10
	Ix11 = 11
	Ix12 = 12
	Ix13 = 13
	Ix14 = 14
	Ix15 = 15
	Ix16 = 16
	Ix17 = 17
	Ix18 = 18
	Ix19 = 19
	Ix20 = 20
	Ix21 = 21
	Ix22 = 22
	Ix23 = 23
	Ix24 = 24
	Ix25 = 25
	Ix26 = 26
	Ix27 = 27
	Ix28 = 28
	Ix29 = 29
	Ix30 = 30
	Ix31 = 31
	Ix32 = 32
	Ix33 = 33
	Ix34 = 34
	Ix35 = 35
	Ix36 = 36
	Ix37 = 37
	Ix38 = 38
	Ix39 = 39
	Ix40 = 40
	Ix41 = 41
	Ix42 = 42
	Ix43 = 43
	Ix44 = 44
	Ix45 = 45
	Ix46 = 46
	Ix47 = 47
	Ix48 = 48
	Ix49 = 49
	Ix50 = 50
	Ix51 = 51
	Ix52 = 52
	Ix53 = 53
	Ix54 = 54
	Ix55 = 55
	Ix56 = 56
	Ix57 = 57
	Ix58 = 58
	Ix59 = 59
	Ix60 = 60
	Ix61 = 61
	Ix62 = 62
	Ix63 = 63
	Ix64 = 64


# noinspection SpellCheckingInspection
class LimitIx(Enum):
	"""Repeated capability LimitIx"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class Line(Enum):
	"""Repeated capability Line"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ix1 = 1
	Ix2 = 2
	Ix3 = 3
	Ix4 = 4
	Ix5 = 5
	Ix6 = 6
	Ix7 = 7
	Ix8 = 8


# noinspection SpellCheckingInspection
class Marker(Enum):
	"""Repeated capability Marker"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16


# noinspection SpellCheckingInspection
class MarkerDestination(Enum):
	"""Repeated capability MarkerDestination"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16


# noinspection SpellCheckingInspection
class OutputConnector(Enum):
	"""Repeated capability OutputConnector"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4


# noinspection SpellCheckingInspection
class Port(Enum):
	"""Repeated capability Port"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class PowerClass(Enum):
	"""Repeated capability PowerClass"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30


# noinspection SpellCheckingInspection
class PowerMeter(Enum):
	"""Repeated capability PowerMeter"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16


# noinspection SpellCheckingInspection
class Probe(Enum):
	"""Repeated capability Probe"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8


# noinspection SpellCheckingInspection
class RangePy(Enum):
	"""Repeated capability RangePy"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ix1 = 1
	Ix2 = 2
	Ix3 = 3
	Ix4 = 4
	Ix5 = 5
	Ix6 = 6
	Ix7 = 7
	Ix8 = 8
	Ix9 = 9
	Ix10 = 10
	Ix11 = 11
	Ix12 = 12
	Ix13 = 13
	Ix14 = 14
	Ix15 = 15
	Ix16 = 16
	Ix17 = 17
	Ix18 = 18
	Ix19 = 19
	Ix20 = 20
	Ix21 = 21
	Ix22 = 22
	Ix23 = 23
	Ix24 = 24
	Ix25 = 25
	Ix26 = 26
	Ix27 = 27
	Ix28 = 28
	Ix29 = 29
	Ix30 = 30
	Ix31 = 31
	Ix32 = 32
	Ix33 = 33
	Ix34 = 34
	Ix35 = 35
	Ix36 = 36
	Ix37 = 37
	Ix38 = 38
	Ix39 = 39
	Ix40 = 40
	Ix41 = 41
	Ix42 = 42
	Ix43 = 43
	Ix44 = 44
	Ix45 = 45
	Ix46 = 46
	Ix47 = 47
	Ix48 = 48
	Ix49 = 49
	Ix50 = 50
	Ix51 = 51
	Ix52 = 52
	Ix53 = 53
	Ix54 = 54
	Ix55 = 55
	Ix56 = 56
	Ix57 = 57
	Ix58 = 58
	Ix59 = 59
	Ix60 = 60
	Ix61 = 61
	Ix62 = 62
	Ix63 = 63
	Ix64 = 64


# noinspection SpellCheckingInspection
class RefMeasurement(Enum):
	"""Repeated capability RefMeasurement"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class Source(Enum):
	"""Repeated capability Source"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class SPortPair(Enum):
	"""Repeated capability SPortPair"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ix1 = 1
	Ix2 = 2
	Ix3 = 3
	Ix4 = 4


# noinspection SpellCheckingInspection
class Status(Enum):
	"""Repeated capability Status"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class Step(Enum):
	"""Repeated capability Step"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class Store(Enum):
	"""Repeated capability Store"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Pos1 = 1
	Pos2 = 2
	Pos3 = 3
	Pos4 = 4
	Pos5 = 5
	Pos6 = 6
	Pos7 = 7
	Pos8 = 8
	Pos9 = 9
	Pos10 = 10
	Pos11 = 11
	Pos12 = 12
	Pos13 = 13
	Pos14 = 14
	Pos15 = 15
	Pos16 = 16
	Pos17 = 17
	Pos18 = 18
	Pos19 = 19
	Pos20 = 20
	Pos21 = 21
	Pos22 = 22
	Pos23 = 23
	Pos24 = 24
	Pos25 = 25
	Pos26 = 26
	Pos27 = 27
	Pos28 = 28
	Pos29 = 29
	Pos30 = 30
	Pos31 = 31
	Pos32 = 32


# noinspection SpellCheckingInspection
class SubBlock(Enum):
	"""Repeated capability SubBlock"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8


# noinspection SpellCheckingInspection
class SubWindow(Enum):
	"""Repeated capability SubWindow"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class TouchStone(Enum):
	"""Repeated capability TouchStone"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ix1 = 1
	Ix2 = 2
	Ix3 = 3
	Ix4 = 4
	Ix5 = 5
	Ix6 = 6
	Ix7 = 7
	Ix8 = 8
	Ix9 = 9
	Ix10 = 10
	Ix11 = 11
	Ix12 = 12
	Ix13 = 13
	Ix14 = 14
	Ix15 = 15
	Ix16 = 16
	Ix17 = 17
	Ix18 = 18
	Ix19 = 19
	Ix20 = 20
	Ix21 = 21
	Ix22 = 22
	Ix23 = 23
	Ix24 = 24
	Ix25 = 25
	Ix26 = 26
	Ix27 = 27
	Ix28 = 28
	Ix29 = 29
	Ix30 = 30
	Ix31 = 31
	Ix32 = 32


# noinspection SpellCheckingInspection
class Trace(Enum):
	"""Repeated capability Trace"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Tr1 = 1
	Tr2 = 2
	Tr3 = 3
	Tr4 = 4
	Tr5 = 5
	Tr6 = 6
	Tr7 = 7
	Tr8 = 8
	Tr9 = 9
	Tr10 = 10
	Tr11 = 11
	Tr12 = 12
	Tr13 = 13
	Tr14 = 14
	Tr15 = 15
	Tr16 = 16


# noinspection SpellCheckingInspection
class TriggerPort(Enum):
	"""Repeated capability TriggerPort"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8


# noinspection SpellCheckingInspection
class UpperAltChannel(Enum):
	"""Repeated capability UpperAltChannel"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32
	Nr33 = 33
	Nr34 = 34
	Nr35 = 35
	Nr36 = 36
	Nr37 = 37
	Nr38 = 38
	Nr39 = 39
	Nr40 = 40
	Nr41 = 41
	Nr42 = 42
	Nr43 = 43
	Nr44 = 44
	Nr45 = 45
	Nr46 = 46
	Nr47 = 47
	Nr48 = 48
	Nr49 = 49
	Nr50 = 50
	Nr51 = 51
	Nr52 = 52
	Nr53 = 53
	Nr54 = 54
	Nr55 = 55
	Nr56 = 56
	Nr57 = 57
	Nr58 = 58
	Nr59 = 59
	Nr60 = 60
	Nr61 = 61
	Nr62 = 62
	Nr63 = 63


# noinspection SpellCheckingInspection
class UserRange(Enum):
	"""Repeated capability UserRange"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16


# noinspection SpellCheckingInspection
class Window(Enum):
	"""Repeated capability Window"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16


# noinspection SpellCheckingInspection
class ZoomWindow(Enum):
	"""Repeated capability ZoomWindow"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32

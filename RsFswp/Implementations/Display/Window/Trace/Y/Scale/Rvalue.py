from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions
from ....... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class RvalueCls:
	"""Rvalue commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("rvalue", core, parent)

	def set(self, ref_value: float, window=repcap.Window.Default, trace=repcap.Trace.Default) -> None:
		"""SCPI: DISPlay[:WINDow<n>]:TRACe<t>:Y[:SCALe]:RVALue \n
		Snippet: driver.display.window.trace.y.scale.rvalue.set(ref_value = 1.0, window = repcap.Window.Default, trace = repcap.Trace.Default) \n
		This command defines the value assigned to the reference position.
			INTRO_CMD_HELP: Prerequisites for this command \n
			- Phase result display must be available and selected. \n
			:param ref_value: numeric value Unit: Depends on the selected unit (deg or rad)
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Window')
			:param trace: optional repeated capability selector. Default value: Tr1 (settable in the interface 'Trace')
		"""
		param = Conversions.decimal_value_to_str(ref_value)
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		trace_cmd_val = self._cmd_group.get_repcap_cmd_value(trace, repcap.Trace)
		self._core.io.write(f'DISPlay:WINDow{window_cmd_val}:TRACe{trace_cmd_val}:Y:SCALe:RVALue {param}')

	def get(self, window=repcap.Window.Default, trace=repcap.Trace.Default) -> float:
		"""SCPI: DISPlay[:WINDow<n>]:TRACe<t>:Y[:SCALe]:RVALue \n
		Snippet: value: float = driver.display.window.trace.y.scale.rvalue.get(window = repcap.Window.Default, trace = repcap.Trace.Default) \n
		This command defines the value assigned to the reference position.
			INTRO_CMD_HELP: Prerequisites for this command \n
			- Phase result display must be available and selected. \n
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Window')
			:param trace: optional repeated capability selector. Default value: Tr1 (settable in the interface 'Trace')
			:return: ref_value: numeric value Unit: Depends on the selected unit (deg or rad)"""
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		trace_cmd_val = self._cmd_group.get_repcap_cmd_value(trace, repcap.Trace)
		response = self._core.io.query_str(f'DISPlay:WINDow{window_cmd_val}:TRACe{trace_cmd_val}:Y:SCALe:RVALue?')
		return Conversions.str_to_float(response)

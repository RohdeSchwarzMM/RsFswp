from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions
from ....... import enums
from ....... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ModeCls:
	"""Mode commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("mode", core, parent)

	def set(self, reference_mode: enums.ReferenceMode, window=repcap.Window.Default, trace=repcap.Trace.Default) -> None:
		"""SCPI: DISPlay[:WINDow<n>]:TRACe<t>:Y[:SCALe]:MODE \n
		Snippet: driver.display.window.trace.y.scale.mode.set(reference_mode = enums.ReferenceMode.ABSolute, window = repcap.Window.Default, trace = repcap.Trace.Default) \n
		This command selects how frequencies are displayed on the y-axis.
			INTRO_CMD_HELP: Prerequisites for this command \n
			- Frequency result display must be available and selected. \n
			:param reference_mode: No help available
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Window')
			:param trace: optional repeated capability selector. Default value: Tr1 (settable in the interface 'Trace')
		"""
		param = Conversions.enum_scalar_to_str(reference_mode, enums.ReferenceMode)
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		trace_cmd_val = self._cmd_group.get_repcap_cmd_value(trace, repcap.Trace)
		self._core.io.write(f'DISPlay:WINDow{window_cmd_val}:TRACe{trace_cmd_val}:Y:SCALe:MODE {param}')

	# noinspection PyTypeChecker
	def get(self, window=repcap.Window.Default, trace=repcap.Trace.Default) -> enums.ReferenceMode:
		"""SCPI: DISPlay[:WINDow<n>]:TRACe<t>:Y[:SCALe]:MODE \n
		Snippet: value: enums.ReferenceMode = driver.display.window.trace.y.scale.mode.get(window = repcap.Window.Default, trace = repcap.Trace.Default) \n
		This command selects how frequencies are displayed on the y-axis.
			INTRO_CMD_HELP: Prerequisites for this command \n
			- Frequency result display must be available and selected. \n
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Window')
			:param trace: optional repeated capability selector. Default value: Tr1 (settable in the interface 'Trace')
			:return: reference_mode: No help available"""
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		trace_cmd_val = self._cmd_group.get_repcap_cmd_value(trace, repcap.Trace)
		response = self._core.io.query_str(f'DISPlay:WINDow{window_cmd_val}:TRACe{trace_cmd_val}:Y:SCALe:MODE?')
		return Conversions.str_to_scalar_enum(response, enums.ReferenceMode)

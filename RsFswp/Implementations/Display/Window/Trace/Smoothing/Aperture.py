from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions
from ...... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ApertureCls:
	"""Aperture commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("aperture", core, parent)

	def set(self, ref_value: float, window=repcap.Window.Default, trace=repcap.Trace.Default) -> None:
		"""SCPI: DISPlay[:WINDow<n>]:TRACe<t>:SMOothing:APERture \n
		Snippet: driver.display.window.trace.smoothing.aperture.set(ref_value = 1.0, window = repcap.Window.Default, trace = repcap.Trace.Default) \n
		This command defines the magnitude (aperture) of trace smoothing.
			INTRO_CMD_HELP: Prerequisites for this command \n
			- Turn on trace smoothing (method RsFswp.Display.Window.Trace.Smoothing.State.set) .
		In the Spot Noise vs Tune measurement, trace smoothing applies to either all traces or none.
		Use [SENSe:]SMOothing[:STATe] in that case. \n
			:param ref_value: No help available
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Window')
			:param trace: optional repeated capability selector. Default value: Tr1 (settable in the interface 'Trace')
		"""
		param = Conversions.decimal_value_to_str(ref_value)
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		trace_cmd_val = self._cmd_group.get_repcap_cmd_value(trace, repcap.Trace)
		self._core.io.write(f'DISPlay:WINDow{window_cmd_val}:TRACe{trace_cmd_val}:SMOothing:APERture {param}')

	def get(self, window=repcap.Window.Default, trace=repcap.Trace.Default) -> float:
		"""SCPI: DISPlay[:WINDow<n>]:TRACe<t>:SMOothing:APERture \n
		Snippet: value: float = driver.display.window.trace.smoothing.aperture.get(window = repcap.Window.Default, trace = repcap.Trace.Default) \n
		This command defines the magnitude (aperture) of trace smoothing.
			INTRO_CMD_HELP: Prerequisites for this command \n
			- Turn on trace smoothing (method RsFswp.Display.Window.Trace.Smoothing.State.set) .
		In the Spot Noise vs Tune measurement, trace smoothing applies to either all traces or none.
		Use [SENSe:]SMOothing[:STATe] in that case. \n
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Window')
			:param trace: optional repeated capability selector. Default value: Tr1 (settable in the interface 'Trace')
			:return: ref_value: No help available"""
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		trace_cmd_val = self._cmd_group.get_repcap_cmd_value(trace, repcap.Trace)
		response = self._core.io.query_str(f'DISPlay:WINDow{window_cmd_val}:TRACe{trace_cmd_val}:SMOothing:APERture?')
		return Conversions.str_to_float(response)

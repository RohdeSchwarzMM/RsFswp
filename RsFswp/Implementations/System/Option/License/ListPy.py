from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal.StructBase import StructBase
from .....Internal.ArgStruct import ArgStruct
from ..... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ListPyCls:
	"""ListPy commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("listPy", core, parent)

	# noinspection PyTypeChecker
	class GetStruct(StructBase):
		"""Response structure. Fields: \n
			- Option: str: No parameter help available
			- State: enums.OptionState: No parameter help available
			- Days: float: No parameter help available"""
		__meta_args_list = [
			ArgStruct.scalar_str('Option'),
			ArgStruct.scalar_enum('State', enums.OptionState),
			ArgStruct.scalar_float('Days')]

		def __init__(self):
			StructBase.__init__(self, self)
			self.Option: str = None
			self.State: enums.OptionState = None
			self.Days: float = None

	def get(self) -> GetStruct:
		"""SCPI: SYSTem:OPTion:LICense[:LIST] \n
		Snippet: value: GetStruct = driver.system.option.license.listPy.get() \n
		No command help available \n
			:return: structure: for return value, see the help for GetStruct structure arguments."""
		return self._core.io.query_struct(f'SYSTem:OPTion:LICense:LIST?', self.__class__.GetStruct())

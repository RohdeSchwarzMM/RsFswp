from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class VfirmwareCls:
	"""Vfirmware commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("vfirmware", core, parent)

	def set(self, valid_device: bool) -> None:
		"""SCPI: SYSTem:COMMunicate:RDEVice:OSCilloscope:VFIRmware \n
		Snippet: driver.system.communicate.rdevice.oscilloscope.vfirmware.set(valid_device = False) \n
		No command help available \n
			:param valid_device: No help available
		"""
		param = Conversions.bool_to_str(valid_device)
		self._core.io.write(f'SYSTem:COMMunicate:RDEVice:OSCilloscope:VFIRmware {param}')

	def get(self) -> bool:
		"""SCPI: SYSTem:COMMunicate:RDEVice:OSCilloscope:VFIRmware \n
		Snippet: value: bool = driver.system.communicate.rdevice.oscilloscope.vfirmware.get() \n
		No command help available \n
			:return: valid_device: No help available"""
		response = self._core.io.query_str(f'SYSTem:COMMunicate:RDEVice:OSCilloscope:VFIRmware?')
		return Conversions.str_to_bool(response)

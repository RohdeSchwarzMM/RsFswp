from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class WbFrequencyCls:
	"""WbFrequency commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("wbFrequency", core, parent)

	def set(self, frequency: float) -> None:
		"""SCPI: DIAGnostic:SERVice:INPut:PULSed:WBFRequency \n
		Snippet: driver.diagnostic.service.inputPy.pulsed.wbFrequency.set(frequency = 1.0) \n
		No command help available \n
			:param frequency: No help available
		"""
		param = Conversions.decimal_value_to_str(frequency)
		self._core.io.write(f'DIAGnostic:SERVice:INPut:PULSed:WBFRequency {param}')

	def get(self) -> float:
		"""SCPI: DIAGnostic:SERVice:INPut:PULSed:WBFRequency \n
		Snippet: value: float = driver.diagnostic.service.inputPy.pulsed.wbFrequency.get() \n
		No command help available \n
			:return: frequency: No help available"""
		response = self._core.io.query_str(f'DIAGnostic:SERVice:INPut:PULSed:WBFRequency?')
		return Conversions.str_to_float(response)

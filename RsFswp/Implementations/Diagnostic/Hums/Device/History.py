from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class HistoryCls:
	"""History commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("history", core, parent)

	def get(self) -> bytes:
		"""SCPI: DIAGnostic:HUMS:DEVice:HISTory \n
		Snippet: value: bytes = driver.diagnostic.hums.device.history.get() \n
		No command help available \n
			:return: device_history: No help available"""
		response = self._core.io.query_bin_block_ERROR(f'DIAGnostic:HUMS:DEVice:HISTory?')
		return response

	def delete_all(self) -> None:
		"""SCPI: DIAGnostic:HUMS:DEVice:HISTory:DELete:ALL \n
		Snippet: driver.diagnostic.hums.device.history.delete_all() \n
		No command help available \n
		"""
		self._core.io.write(f'DIAGnostic:HUMS:DEVice:HISTory:DELete:ALL')

	def delete_all_with_opc(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: DIAGnostic:HUMS:DEVice:HISTory:DELete:ALL \n
		Snippet: driver.diagnostic.hums.device.history.delete_all_with_opc() \n
		No command help available \n
		Same as delete_all, but waits for the operation to complete before continuing further. Use the RsFswp.utilities.opc_timeout_set() to set the timeout value. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'DIAGnostic:HUMS:DEVice:HISTory:DELete:ALL', opc_timeout_ms)

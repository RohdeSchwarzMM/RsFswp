from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions
from .... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class XdistribCls:
	"""Xdistrib commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("xdistrib", core, parent)

	def set(self, xdistribution: enums.Xdistribution) -> None:
		"""SCPI: FORMat:DEXPort:XDIStrib \n
		Snippet: driver.formatPy.dexport.xdistrib.set(xdistribution = enums.Xdistribution.BINCentered) \n
		No command help available \n
			:param xdistribution: No help available
		"""
		param = Conversions.enum_scalar_to_str(xdistribution, enums.Xdistribution)
		self._core.io.write(f'FORMat:DEXPort:XDIStrib {param}')

	# noinspection PyTypeChecker
	def get(self) -> enums.Xdistribution:
		"""SCPI: FORMat:DEXPort:XDIStrib \n
		Snippet: value: enums.Xdistribution = driver.formatPy.dexport.xdistrib.get() \n
		No command help available \n
			:return: xdistribution: No help available"""
		response = self._core.io.query_str(f'FORMat:DEXPort:XDIStrib?')
		return Conversions.str_to_scalar_enum(response, enums.Xdistribution)

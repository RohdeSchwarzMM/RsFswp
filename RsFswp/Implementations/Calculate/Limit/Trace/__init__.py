from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from .....Internal.RepeatedCapability import RepeatedCapability
from ..... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class TraceCls:
	"""Trace commands group definition. 2 total commands, 1 Subgroups, 1 group commands
	Repeated Capability: Trace, default value after init: Trace.Tr1"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("trace", core, parent)
		self._cmd_group.rep_cap = RepeatedCapability(self._cmd_group.group_name, 'repcap_trace_get', 'repcap_trace_set', repcap.Trace.Tr1)

	def repcap_trace_set(self, trace: repcap.Trace) -> None:
		"""Repeated Capability default value numeric suffix.
		This value is used, if you do not explicitely set it in the child set/get methods, or if you leave it to Trace.Default
		Default value after init: Trace.Tr1"""
		self._cmd_group.set_repcap_enum_value(trace)

	def repcap_trace_get(self) -> repcap.Trace:
		"""Returns the current default repeated capability for the child set/get methods"""
		# noinspection PyTypeChecker
		return self._cmd_group.get_repcap_enum_value()

	@property
	def check(self):
		"""check commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_check'):
			from .Check import CheckCls
			self._check = CheckCls(self._core, self._cmd_group)
		return self._check

	def set(self, trace_limit: float, window=repcap.Window.Default, limitIx=repcap.LimitIx.Default, trace=repcap.Trace.Default) -> None:
		"""SCPI: CALCulate<n>:LIMit<li>:TRACe<t> \n
		Snippet: driver.calculate.limit.trace.set(trace_limit = 1.0, window = repcap.Window.Default, limitIx = repcap.LimitIx.Default, trace = repcap.Trace.Default) \n
		This command links a limit line to one or more traces. Note that this command is maintained for compatibility reasons
		only. Limit lines no longer need to be assigned to a trace explicitly. The trace to be checked can be defined directly
		(as a suffix) in the new command to activate the limit check (see method RsFswp.Calculate.Limit.Trace.Check.set) . \n
			:param trace_limit: No help available
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Calculate')
			:param limitIx: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Limit')
			:param trace: optional repeated capability selector. Default value: Tr1 (settable in the interface 'Trace')
		"""
		param = Conversions.decimal_value_to_str(trace_limit)
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		limitIx_cmd_val = self._cmd_group.get_repcap_cmd_value(limitIx, repcap.LimitIx)
		trace_cmd_val = self._cmd_group.get_repcap_cmd_value(trace, repcap.Trace)
		self._core.io.write(f'CALCulate{window_cmd_val}:LIMit{limitIx_cmd_val}:TRACe{trace_cmd_val} {param}')

	def get(self, window=repcap.Window.Default, limitIx=repcap.LimitIx.Default, trace=repcap.Trace.Default) -> float:
		"""SCPI: CALCulate<n>:LIMit<li>:TRACe<t> \n
		Snippet: value: float = driver.calculate.limit.trace.get(window = repcap.Window.Default, limitIx = repcap.LimitIx.Default, trace = repcap.Trace.Default) \n
		This command links a limit line to one or more traces. Note that this command is maintained for compatibility reasons
		only. Limit lines no longer need to be assigned to a trace explicitly. The trace to be checked can be defined directly
		(as a suffix) in the new command to activate the limit check (see method RsFswp.Calculate.Limit.Trace.Check.set) . \n
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Calculate')
			:param limitIx: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Limit')
			:param trace: optional repeated capability selector. Default value: Tr1 (settable in the interface 'Trace')
			:return: trace_limit: No help available"""
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		limitIx_cmd_val = self._cmd_group.get_repcap_cmd_value(limitIx, repcap.LimitIx)
		trace_cmd_val = self._cmd_group.get_repcap_cmd_value(trace, repcap.Trace)
		response = self._core.io.query_str(f'CALCulate{window_cmd_val}:LIMit{limitIx_cmd_val}:TRACe{trace_cmd_val}?')
		return Conversions.str_to_float(response)

	def clone(self) -> 'TraceCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = TraceCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group

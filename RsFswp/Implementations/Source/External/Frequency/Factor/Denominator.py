from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions
from ...... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DenominatorCls:
	"""Denominator commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("denominator", core, parent)

	def set(self, value: float, externalGen=repcap.ExternalGen.Nr1) -> None:
		"""SCPI: SOURce:EXTernal<gen>:FREQuency[:FACTor]:DENominator \n
		Snippet: driver.source.external.frequency.factor.denominator.set(value = 1.0, externalGen = repcap.ExternalGen.Nr1) \n
		No command help available \n
			:param value: No help available
			:param externalGen: optional repeated capability selector. Default value: Nr1
		"""
		param = Conversions.decimal_value_to_str(value)
		externalGen_cmd_val = self._cmd_group.get_repcap_cmd_value(externalGen, repcap.ExternalGen)
		self._core.io.write(f'SOURce:EXTernal{externalGen_cmd_val}:FREQuency:FACTor:DENominator {param}')

	def get(self, externalGen=repcap.ExternalGen.Nr1) -> float:
		"""SCPI: SOURce:EXTernal<gen>:FREQuency[:FACTor]:DENominator \n
		Snippet: value: float = driver.source.external.frequency.factor.denominator.get(externalGen = repcap.ExternalGen.Nr1) \n
		No command help available \n
			:param externalGen: optional repeated capability selector. Default value: Nr1
			:return: value: No help available"""
		externalGen_cmd_val = self._cmd_group.get_repcap_cmd_value(externalGen, repcap.ExternalGen)
		response = self._core.io.query_str(f'SOURce:EXTernal{externalGen_cmd_val}:FREQuency:FACTor:DENominator?')
		return Conversions.str_to_float(response)

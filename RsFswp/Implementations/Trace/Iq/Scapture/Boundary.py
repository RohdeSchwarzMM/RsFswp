from typing import List

from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from ..... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class BoundaryCls:
	"""Boundary commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("boundary", core, parent)

	def get(self, window=repcap.Window.Default) -> List[int]:
		"""SCPI: TRACe<n>:IQ:SCAPture:BOUNdary \n
		Snippet: value: List[int] = driver.trace.iq.scapture.boundary.get(window = repcap.Window.Default) \n
		No command help available \n
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Trace')
			:return: indices: No help available"""
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		response = self._core.io.query_bin_or_ascii_int_list(f'TRACe{window_cmd_val}:IQ:SCAPture:BOUNdary?')
		return response

from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class BandwidthCls:
	"""Bandwidth commands group definition. 11 total commands, 3 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("bandwidth", core, parent)

	@property
	def resolution(self):
		"""resolution commands group. 4 Sub-classes, 1 commands."""
		if not hasattr(self, '_resolution'):
			from .Resolution import ResolutionCls
			self._resolution = ResolutionCls(self._core, self._cmd_group)
		return self._resolution

	@property
	def video(self):
		"""video commands group. 3 Sub-classes, 1 commands."""
		if not hasattr(self, '_video'):
			from .Video import VideoCls
			self._video = VideoCls(self._core, self._cmd_group)
		return self._video

	@property
	def demod(self):
		"""demod commands group. 1 Sub-classes, 1 commands."""
		if not hasattr(self, '_demod'):
			from .Demod import DemodCls
			self._demod = DemodCls(self._core, self._cmd_group)
		return self._demod

	def clone(self) -> 'BandwidthCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = BandwidthCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group

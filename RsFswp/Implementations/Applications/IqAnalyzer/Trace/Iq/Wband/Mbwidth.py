from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class MbwidthCls:
	"""Mbwidth commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("mbwidth", core, parent)

	def set(self, max_bandwidth: int) -> None:
		"""SCPI: TRACe:IQ:WBANd:MBWidth \n
		Snippet: driver.applications.iqAnalyzer.trace.iq.wband.mbwidth.set(max_bandwidth = 1) \n
		Defines the maximum analysis bandwidth. Any value can be specified; the next higher fixed bandwidth is used. \n
			:param max_bandwidth: 80 MHz Restricts the analysis bandwidth to a maximum of 80 MHz. The bandwidth extension option R&S FSWP-B320 is deactivated. method RsFswp.Applications.IqAnalyzer.Trace.Iq.Wband.State.set is set to OFF. 160 MHz Restricts the analysis bandwidth to a maximum of 160 MHz. The bandwidth extension option R&S FSWP-B320 is deactivated. method RsFswp.Applications.IqAnalyzer.Trace.Iq.Wband.State.set is set to ON. 160 MHz | MAX The bandwidth extension option is activated. The currently available maximum bandwidth is allowed. method RsFswp.Applications.IqAnalyzer.Trace.Iq.Wband.State.set is set to ON. Unit: Hz
		"""
		param = Conversions.decimal_value_to_str(max_bandwidth)
		self._core.io.write(f'TRACe:IQ:WBANd:MBWidth {param}')

	def get(self) -> int:
		"""SCPI: TRACe:IQ:WBANd:MBWidth \n
		Snippet: value: int = driver.applications.iqAnalyzer.trace.iq.wband.mbwidth.get() \n
		Defines the maximum analysis bandwidth. Any value can be specified; the next higher fixed bandwidth is used. \n
			:return: max_bandwidth: No help available"""
		response = self._core.io.query_str(f'TRACe:IQ:WBANd:MBWidth?')
		return Conversions.str_to_int(response)

from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class SpanCls:
	"""Span commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("span", core, parent)

	def set(self, span: float) -> None:
		"""SCPI: [SENSe]:FREQuency:SPAN \n
		Snippet: driver.applications.k30NoiseFigure.sense.frequency.span.set(span = 1.0) \n
		This command defines the frequency span. If you change the span, the application creates a new frequency list. \n
			:param span: Unit: Hz
		"""
		param = Conversions.decimal_value_to_str(span)
		self._core.io.write(f'SENSe:FREQuency:SPAN {param}')

	def get(self) -> float:
		"""SCPI: [SENSe]:FREQuency:SPAN \n
		Snippet: value: float = driver.applications.k30NoiseFigure.sense.frequency.span.get() \n
		This command defines the frequency span. If you change the span, the application creates a new frequency list. \n
			:return: span: Unit: Hz"""
		response = self._core.io.query_str(f'SENSe:FREQuency:SPAN?')
		return Conversions.str_to_float(response)

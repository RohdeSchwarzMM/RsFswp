from typing import List

from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions
from ...... import enums
from ...... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class XCls:
	"""X commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("x", core, parent)

	def get(self, trace_type: enums.TraceTypeNumeric, window=repcap.Window.Default) -> List[float]:
		"""SCPI: TRACe<n>[:DATA]:X \n
		Snippet: value: List[float] = driver.applications.k50Spurious.trace.data.x.get(trace_type = enums.TraceTypeNumeric.TRACe1, window = repcap.Window.Default) \n
		This remote control command returns the X values only for the trace in the selected result display. Depending on the type
		of result display and the scaling of the x-axis, this can be either the pulse number or a timestamp for each detected
		pulse in the capture buffer. This command is only available for graphical displays, except for the Magnitude Capture
		display. \n
			:param trace_type: No help available
			:param window: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Trace')
			:return: trace_xdata: No help available"""
		param = Conversions.enum_scalar_to_str(trace_type, enums.TraceTypeNumeric)
		window_cmd_val = self._cmd_group.get_repcap_cmd_value(window, repcap.Window)
		response = self._core.io.query_bin_or_ascii_float_list(f'FORMAT REAL,32;TRACe{window_cmd_val}:DATA:X? {param}')
		return response

from ........Internal.Core import Core
from ........Internal.CommandsGroup import CommandsGroup
from ........Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class StopCls:
	"""Stop commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("stop", core, parent)

	def set(self, stop: float) -> None:
		"""SCPI: [SENSe]:CREFerence:PDETect:RANGe:STOP \n
		Snippet: driver.applications.k50Spurious.sense.creference.pdetect.range.stop.set(stop = 1.0) \n
		Defines the end of the range in which the maximum peak is searched. \n
			:param stop: Unit: HZ
		"""
		param = Conversions.decimal_value_to_str(stop)
		self._core.io.write(f'SENSe:CREFerence:PDETect:RANGe:STOP {param}')

	def get(self) -> float:
		"""SCPI: [SENSe]:CREFerence:PDETect:RANGe:STOP \n
		Snippet: value: float = driver.applications.k50Spurious.sense.creference.pdetect.range.stop.get() \n
		Defines the end of the range in which the maximum peak is searched. \n
			:return: stop: Unit: HZ"""
		response = self._core.io.query_str(f'SENSe:CREFerence:PDETect:RANGe:STOP?')
		return Conversions.str_to_float(response)

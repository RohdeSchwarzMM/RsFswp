from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DisplayCls:
	"""Display commands group definition. 15 total commands, 3 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("display", core, parent)

	@property
	def window(self):
		"""window commands group. 5 Sub-classes, 0 commands."""
		if not hasattr(self, '_window'):
			from .Window import WindowCls
			self._window = WindowCls(self._core, self._cmd_group)
		return self._window

	@property
	def wselect(self):
		"""wselect commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_wselect'):
			from .Wselect import WselectCls
			self._wselect = WselectCls(self._core, self._cmd_group)
		return self._wselect

	@property
	def mtable(self):
		"""mtable commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_mtable'):
			from .Mtable import MtableCls
			self._mtable = MtableCls(self._core, self._cmd_group)
		return self._mtable

	def clone(self) -> 'DisplayCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = DisplayCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group

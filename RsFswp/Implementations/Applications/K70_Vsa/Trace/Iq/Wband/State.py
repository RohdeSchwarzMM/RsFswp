from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class StateCls:
	"""State commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("state", core, parent)

	def set(self, state: bool) -> None:
		"""SCPI: TRACe:IQ:WBANd[:STATe] \n
		Snippet: driver.applications.k70Vsa.trace.iq.wband.state.set(state = False) \n
		This command determines whether the wideband provided by bandwidth extension options is used or not (if installed) . \n
			:param state: ON | OFF | 0 | 1 OFF | 0 Switches the function off ON | 1 Switches the function on
		"""
		param = Conversions.bool_to_str(state)
		self._core.io.write(f'TRACe:IQ:WBANd:STATe {param}')

	def get(self) -> bool:
		"""SCPI: TRACe:IQ:WBANd[:STATe] \n
		Snippet: value: bool = driver.applications.k70Vsa.trace.iq.wband.state.get() \n
		This command determines whether the wideband provided by bandwidth extension options is used or not (if installed) . \n
			:return: state: ON | OFF | 0 | 1 OFF | 0 Switches the function off ON | 1 Switches the function on"""
		response = self._core.io.query_str(f'TRACe:IQ:WBANd:STATe?')
		return Conversions.str_to_bool(response)

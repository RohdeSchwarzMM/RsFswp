from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class AutoCls:
	"""Auto commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("auto", core, parent)

	def set(self, state: bool) -> None:
		"""SCPI: [SENSe]:DDEMod:MFILter:AUTO \n
		Snippet: driver.applications.k70Vsa.sense.ddemod.mfilter.auto.set(state = False) \n
		If this command is set to 'ON', the measurement filter is defined automatically depending on the transmit filter (see
		[SENSe:]DDEMod:TFILter:NAME) . \n
			:param state: No help available
		"""
		param = Conversions.bool_to_str(state)
		self._core.io.write(f'SENSe:DDEMod:MFILter:AUTO {param}')

	def get(self) -> bool:
		"""SCPI: [SENSe]:DDEMod:MFILter:AUTO \n
		Snippet: value: bool = driver.applications.k70Vsa.sense.ddemod.mfilter.auto.get() \n
		If this command is set to 'ON', the measurement filter is defined automatically depending on the transmit filter (see
		[SENSe:]DDEMod:TFILter:NAME) . \n
			:return: state: No help available"""
		response = self._core.io.query_str(f'SENSe:DDEMod:MFILter:AUTO?')
		return Conversions.str_to_bool(response)

from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions
from ....... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class BandCls:
	"""Band commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("band", core, parent)

	def set(self, band: enums.BandB) -> None:
		"""SCPI: [SENSe]:MIXer:HARMonic:BAND \n
		Snippet: driver.applications.k60Transient.sense.mixer.harmonic.band.set(band = enums.BandB.D) \n
		No command help available \n
			:param band: No help available
		"""
		param = Conversions.enum_scalar_to_str(band, enums.BandB)
		self._core.io.write(f'SENSe:MIXer:HARMonic:BAND {param}')

	# noinspection PyTypeChecker
	def get(self) -> enums.BandB:
		"""SCPI: [SENSe]:MIXer:HARMonic:BAND \n
		Snippet: value: enums.BandB = driver.applications.k60Transient.sense.mixer.harmonic.band.get() \n
		No command help available \n
			:return: band: No help available"""
		response = self._core.io.query_str(f'SENSe:MIXer:HARMonic:BAND?')
		return Conversions.str_to_scalar_enum(response, enums.BandB)

	def preset(self) -> None:
		"""SCPI: [SENSe]:MIXer:HARMonic:BAND:PRESet \n
		Snippet: driver.applications.k60Transient.sense.mixer.harmonic.band.preset() \n
		No command help available \n
		"""
		self._core.io.write(f'SENSe:MIXer:HARMonic:BAND:PRESet')

	def preset_with_opc(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: [SENSe]:MIXer:HARMonic:BAND:PRESet \n
		Snippet: driver.applications.k60Transient.sense.mixer.harmonic.band.preset_with_opc() \n
		No command help available \n
		Same as preset, but waits for the operation to complete before continuing further. Use the RsFswp.utilities.opc_timeout_set() to set the timeout value. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'SENSe:MIXer:HARMonic:BAND:PRESet', opc_timeout_ms)

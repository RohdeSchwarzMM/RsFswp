from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class StartCls:
	"""Start commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("start", core, parent)

	def get(self) -> float:
		"""SCPI: [SENSe]:MIXer:FREQuency:STARt \n
		Snippet: value: float = driver.applications.k60Transient.sense.mixer.frequency.start.get() \n
		No command help available \n
			:return: frequency: No help available"""
		response = self._core.io.query_str(f'SENSe:MIXer:FREQuency:STARt?')
		return Conversions.str_to_float(response)

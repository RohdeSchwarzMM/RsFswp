from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions
from .......Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CommentCls:
	"""Comment commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("comment", core, parent)

	def set(self, text: str) -> None:
		"""SCPI: [SENSe]:CORRection:CVL:COMMent \n
		Snippet: driver.applications.k60Transient.sense.correction.cvl.comment.set(text = '1') \n
		No command help available \n
			:param text: No help available
		"""
		param = Conversions.value_to_quoted_str(text)
		self._core.io.write(f'SENSe:CORRection:CVL:COMMent {param}')

	def get(self, text: str) -> str:
		"""SCPI: [SENSe]:CORRection:CVL:COMMent \n
		Snippet: value: str = driver.applications.k60Transient.sense.correction.cvl.comment.get(text = '1') \n
		No command help available \n
			:param text: No help available
			:return: text: No help available"""
		param = Conversions.value_to_quoted_str(text)
		response = self._core.io.query_str(f'SENSe:CORRection:CVL:COMMent? {param}')
		return trim_str_response(response)

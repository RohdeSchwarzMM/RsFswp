Percent
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:Y:PERCent

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:Y:PERCent



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Y.Percent.PercentCls
	:members:
	:undoc-members:
	:noindex:
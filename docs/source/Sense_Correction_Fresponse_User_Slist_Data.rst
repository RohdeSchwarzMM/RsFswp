Data
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Slist.Data.DataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.correction.fresponse.user.slist.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Correction_Fresponse_User_Slist_Data_Frequency.rst
	Sense_Correction_Fresponse_User_Slist_Data_Magnitude.rst
	Sense_Correction_Fresponse_User_Slist_Data_Phase.rst
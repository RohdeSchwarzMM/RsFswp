State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:SQUelch:STATe

.. code-block:: python

	SENSe:ADEMod:SQUelch:STATe



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Squelch.State.StateCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GENerator:CONNection:STATe

.. code-block:: python

	CONFigure:GENerator:CONNection:STATe



.. autoclass:: RsFswp.Implementations.Configure.Generator.Connection.State.StateCls
	:members:
	:undoc-members:
	:noindex:
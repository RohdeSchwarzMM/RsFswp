Acquire
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:COLLect:ACQuire

.. code-block:: python

	SENSe:CORRection:COLLect:ACQuire



.. autoclass:: RsFswp.Implementations.Sense.Correction.Collect.Acquire.AcquireCls
	:members:
	:undoc-members:
	:noindex:
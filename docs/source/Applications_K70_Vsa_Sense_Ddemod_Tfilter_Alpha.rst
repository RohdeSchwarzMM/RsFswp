Alpha
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:TFILter:ALPHa

.. code-block:: python

	SENSe:DDEMod:TFILter:ALPHa



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Tfilter.Alpha.AlphaCls
	:members:
	:undoc-members:
	:noindex:
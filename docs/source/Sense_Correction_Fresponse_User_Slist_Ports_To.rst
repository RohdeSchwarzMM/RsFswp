To
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:PORTs:TO

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:PORTs:TO



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Slist.Ports.To.ToCls
	:members:
	:undoc-members:
	:noindex:
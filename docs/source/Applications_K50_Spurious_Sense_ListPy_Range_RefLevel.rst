RefLevel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:RLEVel

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:RLEVel



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.ListPy.Range.RefLevel.RefLevelCls
	:members:
	:undoc-members:
	:noindex:
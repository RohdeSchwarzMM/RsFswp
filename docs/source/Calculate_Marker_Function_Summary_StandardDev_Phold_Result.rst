Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:SDEViation:PHOLd:RESult

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:SDEViation:PHOLd:RESult



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Summary.StandardDev.Phold.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
Text
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:FRAMe:EDIT:TEXT

.. code-block:: python

	SENSe:DDEMod:PATTern:FRAMe:EDIT:TEXT



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.Frame.Edit.Text.TextCls
	:members:
	:undoc-members:
	:noindex:
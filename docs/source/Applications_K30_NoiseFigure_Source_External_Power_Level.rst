Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EXTernal:POWer:LEVel

.. code-block:: python

	SOURce:EXTernal:POWer:LEVel



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Source.External.Power.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:
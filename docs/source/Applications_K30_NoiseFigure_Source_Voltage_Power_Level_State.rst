State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:POWer<Source>:LEVel:STATe

.. code-block:: python

	SOURce:VOLTage:POWer<Source>:LEVel:STATe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Source.Voltage.Power.Level.State.StateCls
	:members:
	:undoc-members:
	:noindex:
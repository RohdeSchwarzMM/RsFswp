Info
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:SYSTem:INFO

.. code-block:: python

	DIAGnostic:HUMS:SYSTem:INFO



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.System.Info.InfoCls
	:members:
	:undoc-members:
	:noindex:
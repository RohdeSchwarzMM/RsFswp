CmOffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PROBe<Probe>:SETup:CMOFfset

.. code-block:: python

	SENSe:PROBe<Probe>:SETup:CMOFfset



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Sense.Probe.Setup.CmOffset.CmOffsetCls
	:members:
	:undoc-members:
	:noindex:
RefPosition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe:Y:SCALe:RPOSition

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe:Y:SCALe:RPOSition



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Display.Window.Trace.Y.Scale.RefPosition.RefPositionCls
	:members:
	:undoc-members:
	:noindex:
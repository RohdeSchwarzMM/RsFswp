Window
----------------------------------------





.. autoclass:: RsFswp.Implementations.Calculate.Rtms.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.rtms.window.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Rtms_Window_Ival.rst
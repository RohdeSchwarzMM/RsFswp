Resolution
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:BANDwidth:RESolution

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:BANDwidth:RESolution



.. autoclass:: RsFswp.Implementations.Sense.ListPy.Range.Bandwidth.Resolution.ResolutionCls
	:members:
	:undoc-members:
	:noindex:
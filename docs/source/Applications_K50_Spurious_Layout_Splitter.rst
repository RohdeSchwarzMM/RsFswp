Splitter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:SPLitter

.. code-block:: python

	LAYout:SPLitter



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Layout.Splitter.SplitterCls
	:members:
	:undoc-members:
	:noindex:
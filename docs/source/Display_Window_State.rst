State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:STATe

.. code-block:: python

	DISPlay:WINDow<Window>:STATe



.. autoclass:: RsFswp.Implementations.Display.Window.State.StateCls
	:members:
	:undoc-members:
	:noindex:
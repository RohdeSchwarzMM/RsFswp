Window
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:CATalog:WINDow

.. code-block:: python

	LAYout:CATalog:WINDow



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Layout.Catalog.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:
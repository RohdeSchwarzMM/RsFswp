McFrequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:INPut:PULSed:MCFRequency

.. code-block:: python

	DIAGnostic:SERVice:INPut:PULSed:MCFRequency



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.InputPy.Pulsed.McFrequency.McFrequencyCls
	:members:
	:undoc-members:
	:noindex:
Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:FERRor:PCURrent:VALue

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:FERRor:PCURrent:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.FreqError.Pcurrent.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
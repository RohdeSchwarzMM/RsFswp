State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:SBAR:STATe

.. code-block:: python

	DISPlay:SBAR:STATe



.. autoclass:: RsFswp.Implementations.Display.Sbar.State.StateCls
	:members:
	:undoc-members:
	:noindex:
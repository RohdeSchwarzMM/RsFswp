RefLevel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:Y:SCALe:RLEVel

.. code-block:: python

	DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:Y:SCALe:RLEVel



.. autoclass:: RsFswp.Implementations.Display.Window.Subwindow.Trace.Y.Scale.RefLevel.RefLevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.window.subwindow.trace.y.scale.refLevel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_Window_Subwindow_Trace_Y_Scale_RefLevel_Offset.rst
Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:PERRor:RMEan:RESult

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:PERRor:RMEan:RESult



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Perror.Rmean.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
Ratio
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: UNIT:PMETer<PowerMeter>:POWer:RATio

.. code-block:: python

	UNIT:PMETer<PowerMeter>:POWer:RATio



.. autoclass:: RsFswp.Implementations.Unit.Pmeter.Power.Ratio.RatioCls
	:members:
	:undoc-members:
	:noindex:
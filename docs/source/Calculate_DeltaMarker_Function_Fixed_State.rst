State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:FIXed:STATe

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:FIXed:STATe



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Function.Fixed.State.StateCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:STATe

.. code-block:: python

	SENSe:MIXer:STATe



.. autoclass:: RsFswp.Implementations.Sense.Mixer.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:NDBDown:TIME

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:NDBDown:TIME



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.NdbDown.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:
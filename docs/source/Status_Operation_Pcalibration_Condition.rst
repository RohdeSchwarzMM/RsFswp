Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:PCALibration:CONDition

.. code-block:: python

	STATus:OPERation:PCALibration:CONDition



.. autoclass:: RsFswp.Implementations.Status.Operation.Pcalibration.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
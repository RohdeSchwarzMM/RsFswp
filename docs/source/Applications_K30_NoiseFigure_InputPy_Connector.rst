Connector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:CONNector

.. code-block:: python

	INPut<InputIx>:CONNector



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.InputPy.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex:
Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:POWer:SEQuence:RESult

.. code-block:: python

	SOURce:POWer:SEQuence:RESult



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Source.Power.Sequence.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
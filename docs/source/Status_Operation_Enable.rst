Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:ENABle

.. code-block:: python

	STATus:OPERation:ENABle



.. autoclass:: RsFswp.Implementations.Status.Operation.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
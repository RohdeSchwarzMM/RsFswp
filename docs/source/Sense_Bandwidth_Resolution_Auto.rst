Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BWIDth:RESolution:AUTO

.. code-block:: python

	SENSe:BWIDth:RESolution:AUTO



.. autoclass:: RsFswp.Implementations.Sense.Bandwidth.Resolution.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
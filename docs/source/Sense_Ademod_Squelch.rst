Squelch
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.Ademod.Squelch.SquelchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ademod.squelch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Ademod_Squelch_Level.rst
	Sense_Ademod_Squelch_State.rst
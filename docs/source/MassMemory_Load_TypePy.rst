TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:LOAD:TYPE

.. code-block:: python

	MMEMory:LOAD:TYPE



.. autoclass:: RsFswp.Implementations.MassMemory.Load.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
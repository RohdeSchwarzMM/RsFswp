Llines
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:LLINes

.. code-block:: python

	INSTrument:COUPle:LLINes



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Llines.LlinesCls
	:members:
	:undoc-members:
	:noindex:
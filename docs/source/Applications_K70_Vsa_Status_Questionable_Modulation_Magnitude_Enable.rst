Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:MODulation<Window>:MAGNitude:ENABle

.. code-block:: python

	STATus:QUEStionable:MODulation<Window>:MAGNitude:ENABle



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Status.Questionable.Modulation.Magnitude.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
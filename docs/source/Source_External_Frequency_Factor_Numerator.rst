Numerator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EXTernal<ExternalGen>:FREQuency:FACTor:NUMerator

.. code-block:: python

	SOURce:EXTernal<ExternalGen>:FREQuency:FACTor:NUMerator



.. autoclass:: RsFswp.Implementations.Source.External.Frequency.Factor.Numerator.NumeratorCls
	:members:
	:undoc-members:
	:noindex:
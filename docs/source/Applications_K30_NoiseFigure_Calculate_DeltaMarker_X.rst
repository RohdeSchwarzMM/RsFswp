X
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:X

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:X



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.DeltaMarker.X.XCls
	:members:
	:undoc-members:
	:noindex:
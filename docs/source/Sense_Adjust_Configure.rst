Configure
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.Adjust.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.adjust.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Adjust_Configure_Duration.rst
	Sense_Adjust_Configure_Frequency.rst
	Sense_Adjust_Configure_Hysteresis.rst
	Sense_Adjust_Configure_Level.rst
	Sense_Adjust_Configure_Trigger.rst
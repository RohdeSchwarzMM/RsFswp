Rpm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:PNOise<Trace>:USER<UserRange>:RPM

.. code-block:: python

	FETCh:PNOise<Trace>:USER<UserRange>:RPM



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Fetch.Pnoise.User.Rpm.RpmCls
	:members:
	:undoc-members:
	:noindex:
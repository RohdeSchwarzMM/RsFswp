State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:AUX:LEVel:STATe

.. code-block:: python

	SOURce:VOLTage:AUX:LEVel:STATe



.. autoclass:: RsFswp.Implementations.Source.Voltage.Auxiliary.Level.State.StateCls
	:members:
	:undoc-members:
	:noindex:
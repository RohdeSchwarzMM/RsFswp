TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:IQ:OSC:TYPE

.. code-block:: python

	INPut<InputIx>:IQ:OSC:TYPE



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.InputPy.Iq.Osc.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
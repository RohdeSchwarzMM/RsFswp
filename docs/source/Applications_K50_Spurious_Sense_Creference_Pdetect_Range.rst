Range
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Pdetect.Range.RangeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.sense.creference.pdetect.range.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Sense_Creference_Pdetect_Range_Center.rst
	Applications_K50_Spurious_Sense_Creference_Pdetect_Range_Span.rst
	Applications_K50_Spurious_Sense_Creference_Pdetect_Range_Start.rst
	Applications_K50_Spurious_Sense_Creference_Pdetect_Range_Stop.rst
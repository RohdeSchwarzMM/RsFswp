Phase
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMAT REAL,32;SENSe:CORRection:FRESponse:USER:IQ:DATA:PHASe

.. code-block:: python

	FORMAT REAL,32;SENSe:CORRection:FRESponse:USER:IQ:DATA:PHASe



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Iq.Data.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex:
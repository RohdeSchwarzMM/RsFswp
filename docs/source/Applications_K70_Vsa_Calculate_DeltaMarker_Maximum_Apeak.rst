Apeak
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:MAXimum:APEak

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:MAXimum:APEak



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.DeltaMarker.Maximum.Apeak.ApeakCls
	:members:
	:undoc-members:
	:noindex:
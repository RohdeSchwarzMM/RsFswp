Blighting
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:BLIGhting

.. code-block:: python

	DISPlay:BLIGhting



.. autoclass:: RsFswp.Implementations.Display.Blighting.BlightingCls
	:members:
	:undoc-members:
	:noindex:
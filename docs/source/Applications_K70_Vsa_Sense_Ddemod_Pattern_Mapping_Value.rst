Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:MAPPing:VALue

.. code-block:: python

	SENSe:DDEMod:PATTern:MAPPing:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.Mapping.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
Sw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:SW

.. code-block:: python

	DIAGnostic:HUMS:SW



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.Sw.SwCls
	:members:
	:undoc-members:
	:noindex:
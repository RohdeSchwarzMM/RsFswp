Item
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TABLe:ITEM

.. code-block:: python

	DISPlay:WINDow<Window>:TABLe:ITEM



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Display.Window.Table.Item.ItemCls
	:members:
	:undoc-members:
	:noindex:
Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:SPACing:GAP<GapChannel>:AUTO

.. code-block:: python

	SENSe:POWer:ACHannel:SPACing:GAP<GapChannel>:AUTO



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Spacing.Gap.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
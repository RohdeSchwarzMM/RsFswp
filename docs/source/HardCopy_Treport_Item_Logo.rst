Logo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:ITEM:LOGO

.. code-block:: python

	HCOPy:TREPort:ITEM:LOGO



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Item.Logo.LogoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hardCopy.treport.item.logo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	HardCopy_Treport_Item_Logo_Control.rst
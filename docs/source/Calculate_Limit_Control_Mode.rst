Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:CONTrol:MODE

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:CONTrol:MODE



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Control.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
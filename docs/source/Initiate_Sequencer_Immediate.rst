Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:SEQuencer:IMMediate

.. code-block:: python

	INITiate:SEQuencer:IMMediate



.. autoclass:: RsFswp.Implementations.Initiate.Sequencer.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
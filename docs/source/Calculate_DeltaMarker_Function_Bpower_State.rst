State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:BPOWer:STATe

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:BPOWer:STATe



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Function.Bpower.State.StateCls
	:members:
	:undoc-members:
	:noindex:
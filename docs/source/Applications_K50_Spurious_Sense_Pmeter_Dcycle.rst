Dcycle
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Pmeter.Dcycle.DcycleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.sense.pmeter.dcycle.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Sense_Pmeter_Dcycle_State.rst
	Applications_K50_Spurious_Sense_Pmeter_Dcycle_Value.rst
Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ESPectrum<SubBlock>:PCLass<PowerClass>:COUNt

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ESPectrum<SubBlock>:PCLass<PowerClass>:COUNt



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Espectrum.Pclass.Count.CountCls
	:members:
	:undoc-members:
	:noindex:
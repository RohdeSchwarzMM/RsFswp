Identify
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Layout.Identify.IdentifyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k40PhaseNoise.layout.identify.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K40_PhaseNoise_Layout_Identify_Window.rst
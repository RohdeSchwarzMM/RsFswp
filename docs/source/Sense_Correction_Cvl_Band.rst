Band
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:CVL:BAND

.. code-block:: python

	SENSe:CORRection:CVL:BAND



.. autoclass:: RsFswp.Implementations.Sense.Correction.Cvl.Band.BandCls
	:members:
	:undoc-members:
	:noindex:
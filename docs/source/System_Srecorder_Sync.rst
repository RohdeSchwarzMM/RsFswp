Sync
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:SRECorder:SYNC

.. code-block:: python

	SYSTem:SRECorder:SYNC



.. autoclass:: RsFswp.Implementations.System.Srecorder.Sync.SyncCls
	:members:
	:undoc-members:
	:noindex:
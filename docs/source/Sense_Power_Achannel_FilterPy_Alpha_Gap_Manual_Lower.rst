Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:FILTer:ALPHa:GAP<GapChannel>:MANual:LOWer

.. code-block:: python

	SENSe:POWer:ACHannel:FILTer:ALPHa:GAP<GapChannel>:MANual:LOWer



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.FilterPy.Alpha.Gap.Manual.Lower.LowerCls
	:members:
	:undoc-members:
	:noindex:
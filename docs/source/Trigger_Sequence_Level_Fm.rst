Fm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:LEVel:FM

.. code-block:: python

	TRIGger:SEQuence:LEVel:FM



.. autoclass:: RsFswp.Implementations.Trigger.Sequence.Level.Fm.FmCls
	:members:
	:undoc-members:
	:noindex:
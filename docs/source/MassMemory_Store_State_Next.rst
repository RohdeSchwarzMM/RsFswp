Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:STORe:STATe:NEXT

.. code-block:: python

	MMEMory:STORe:STATe:NEXT



.. autoclass:: RsFswp.Implementations.MassMemory.Store.State.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
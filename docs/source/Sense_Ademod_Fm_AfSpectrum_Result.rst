Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:FM:AFSPectrum:RESult

.. code-block:: python

	SENSe:ADEMod:FM:AFSPectrum:RESult



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Fm.AfSpectrum.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
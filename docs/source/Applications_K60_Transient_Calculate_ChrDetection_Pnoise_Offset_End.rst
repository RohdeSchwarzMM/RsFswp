End
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:CHRDetection:PNOise:OFFSet:END

.. code-block:: python

	CALCulate<Window>:CHRDetection:PNOise:OFFSet:END



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.ChrDetection.Pnoise.Offset.End.EndCls
	:members:
	:undoc-members:
	:noindex:
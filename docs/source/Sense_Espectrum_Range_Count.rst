Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:COUNt

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:COUNt



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Range.Count.CountCls
	:members:
	:undoc-members:
	:noindex:
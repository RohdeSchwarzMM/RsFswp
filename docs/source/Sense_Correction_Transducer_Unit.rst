Unit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:TRANsducer:UNIT

.. code-block:: python

	SENSe:CORRection:TRANsducer:UNIT



.. autoclass:: RsFswp.Implementations.Sense.Correction.Transducer.Unit.UnitCls
	:members:
	:undoc-members:
	:noindex:
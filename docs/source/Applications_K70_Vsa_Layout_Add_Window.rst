Window
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:ADD:WINDow

.. code-block:: python

	LAYout:ADD:WINDow



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Layout.Add.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:
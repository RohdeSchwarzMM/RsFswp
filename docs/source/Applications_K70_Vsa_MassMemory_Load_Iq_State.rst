State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:LOAD<Window>:IQ:STATe

.. code-block:: python

	MMEMory:LOAD<Window>:IQ:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.MassMemory.Load.Iq.State.StateCls
	:members:
	:undoc-members:
	:noindex:
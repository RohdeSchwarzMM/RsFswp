Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:FILE:REPetition:COUNt

.. code-block:: python

	TRACe:IQ:FILE:REPetition:COUNt



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Trace.Iq.File.Repetition.Count.CountCls
	:members:
	:undoc-members:
	:noindex:
Function
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:LIMit<LimitIx>:RELative:STARt:FUNCtion

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:LIMit<LimitIx>:RELative:STARt:FUNCtion



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Range.Limit.Relative.Start.Function.FunctionCls
	:members:
	:undoc-members:
	:noindex:
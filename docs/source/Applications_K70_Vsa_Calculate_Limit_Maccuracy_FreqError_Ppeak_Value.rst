Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:FERRor:PPEak:VALue

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:FERRor:PPEak:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.FreqError.Ppeak.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
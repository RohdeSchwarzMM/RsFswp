AbImpedance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:ABIMpedance

.. code-block:: python

	INSTrument:COUPle:ABIMpedance



.. autoclass:: RsFswp.Implementations.Instrument.Couple.AbImpedance.AbImpedanceCls
	:members:
	:undoc-members:
	:noindex:
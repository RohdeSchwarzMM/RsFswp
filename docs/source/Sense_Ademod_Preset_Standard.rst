Standard
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:PRESet:STANdard

.. code-block:: python

	SENSe:ADEMod:PRESet:STANdard



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Preset.Standard.StandardCls
	:members:
	:undoc-members:
	:noindex:
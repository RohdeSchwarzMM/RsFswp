Holdoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:RFPower:HOLDoff

.. code-block:: python

	TRIGger:SEQuence:RFPower:HOLDoff



.. autoclass:: RsFswp.Implementations.Trigger.Sequence.RfPower.Holdoff.HoldoffCls
	:members:
	:undoc-members:
	:noindex:
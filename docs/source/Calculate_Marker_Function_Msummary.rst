Msummary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:MSUMmary

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:MSUMmary



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Msummary.MsummaryCls
	:members:
	:undoc-members:
	:noindex:
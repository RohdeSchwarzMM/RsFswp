Dpath
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:DPATh

.. code-block:: python

	INPut<InputIx>:DPATh



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.InputPy.Dpath.DpathCls
	:members:
	:undoc-members:
	:noindex:
Factory
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:IDENtify:FACTory

.. code-block:: python

	SYSTem:IDENtify:FACTory



.. autoclass:: RsFswp.Implementations.System.Identify.Factory.FactoryCls
	:members:
	:undoc-members:
	:noindex:
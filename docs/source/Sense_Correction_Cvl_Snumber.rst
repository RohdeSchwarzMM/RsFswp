Snumber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:CVL:SNUMber

.. code-block:: python

	SENSe:CORRection:CVL:SNUMber



.. autoclass:: RsFswp.Implementations.Sense.Correction.Cvl.Snumber.SnumberCls
	:members:
	:undoc-members:
	:noindex:
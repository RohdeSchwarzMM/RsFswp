State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:AFPHase:STATe

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:AFPHase:STATe



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Function.AfPhase.State.StateCls
	:members:
	:undoc-members:
	:noindex:
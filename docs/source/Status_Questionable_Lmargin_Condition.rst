Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:LMARgin<Window>:CONDition

.. code-block:: python

	STATus:QUEStionable:LMARgin<Window>:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Lmargin.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
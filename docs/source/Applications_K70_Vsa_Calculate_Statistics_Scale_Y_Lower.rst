Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:STATistics:SCALe:Y:LOWer

.. code-block:: python

	CALCulate<Window>:STATistics:SCALe:Y:LOWer



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Statistics.Scale.Y.Lower.LowerCls
	:members:
	:undoc-members:
	:noindex:
Uncertainty
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:SANalyzer:GAIN:UNCertainty

.. code-block:: python

	CALCulate<Window>:UNCertainty:SANalyzer:GAIN:UNCertainty



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Sanalyzer.Gain.Uncertainty.UncertaintyCls
	:members:
	:undoc-members:
	:noindex:
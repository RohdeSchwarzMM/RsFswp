Sequencer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:SEQuencer

.. code-block:: python

	SYSTem:SEQuencer



.. autoclass:: RsFswp.Implementations.System.Sequencer.SequencerCls
	:members:
	:undoc-members:
	:noindex:
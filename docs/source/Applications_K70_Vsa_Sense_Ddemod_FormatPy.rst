FormatPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:FORMat

.. code-block:: python

	SENSe:DDEMod:FORMat



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex:
Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:COUNt:CURRent

.. code-block:: python

	SENSe:SWEep:COUNt:CURRent



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Sense.Sweep.Count.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:
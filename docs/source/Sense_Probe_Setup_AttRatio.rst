AttRatio
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PROBe<Probe>:SETup:ATTRatio

.. code-block:: python

	SENSe:PROBe<Probe>:SETup:ATTRatio



.. autoclass:: RsFswp.Implementations.Sense.Probe.Setup.AttRatio.AttRatioCls
	:members:
	:undoc-members:
	:noindex:
Size
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SIZE

.. code-block:: python

	DISPlay:WINDow<Window>:SIZE



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Display.Window.Size.SizeCls
	:members:
	:undoc-members:
	:noindex:
Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:THEMe:CATalog

.. code-block:: python

	DISPlay:THEMe:CATalog



.. autoclass:: RsFswp.Implementations.Display.Theme.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:
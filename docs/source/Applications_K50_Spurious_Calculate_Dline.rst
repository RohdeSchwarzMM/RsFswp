Dline<DisplayLine>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.applications.k50Spurious.calculate.dline.repcap_displayLine_get()
	driver.applications.k50Spurious.calculate.dline.repcap_displayLine_set(repcap.DisplayLine.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DLINe<DisplayLine>

.. code-block:: python

	CALCulate<Window>:DLINe<DisplayLine>



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Dline.DlineCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.calculate.dline.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Calculate_Dline_State.rst
Alpha
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:FILTer:RRC:ALPHa

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:FILTer:RRC:ALPHa



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.FilterPy.Rrc.Alpha.AlphaCls
	:members:
	:undoc-members:
	:noindex:
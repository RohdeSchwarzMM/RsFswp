Holdoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:HOLDoff

.. code-block:: python

	SENSe:SWEep:EGATe:HOLDoff



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Sweep.Egate.Holdoff.HoldoffCls
	:members:
	:undoc-members:
	:noindex:
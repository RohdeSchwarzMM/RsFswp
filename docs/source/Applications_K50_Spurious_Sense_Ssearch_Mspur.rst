Mspur
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SSEarch:MSPur

.. code-block:: python

	SENSe:SSEarch:MSPur



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Ssearch.Mspur.MspurCls
	:members:
	:undoc-members:
	:noindex:
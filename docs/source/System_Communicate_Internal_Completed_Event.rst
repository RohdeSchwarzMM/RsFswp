Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:INTernal:COMPleted:EVENt

.. code-block:: python

	SYSTem:COMMunicate:INTernal:COMPleted:EVENt



.. autoclass:: RsFswp.Implementations.System.Communicate.Internal.Completed.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
Cdevice
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:DIQ:CDEVice

.. code-block:: python

	INPut:DIQ:CDEVice



.. autoclass:: RsFswp.Implementations.InputPy.Diq.Cdevice.CdeviceCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:FERRor:PPEak:STATe

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:FERRor:PPEak:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.FreqError.Ppeak.State.StateCls
	:members:
	:undoc-members:
	:noindex:
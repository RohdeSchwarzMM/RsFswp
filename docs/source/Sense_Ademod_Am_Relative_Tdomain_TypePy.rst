TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:AM:RELative:TDOMain:TYPE

.. code-block:: python

	SENSe:ADEMod:AM:RELative:TDOMain:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Am.Relative.Tdomain.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
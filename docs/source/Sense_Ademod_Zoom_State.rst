State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:ZOOM:STATe

.. code-block:: python

	SENSe:ADEMod:ZOOM:STATe



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Zoom.State.StateCls
	:members:
	:undoc-members:
	:noindex:
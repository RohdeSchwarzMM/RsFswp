State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ACPower:STATe

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ACPower:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Limit.AcPower.State.StateCls
	:members:
	:undoc-members:
	:noindex:
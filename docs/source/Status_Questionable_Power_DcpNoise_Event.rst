Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:POWer:DCPNoise:EVENt

.. code-block:: python

	STATus:QUEStionable:POWer:DCPNoise:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Power.DcpNoise.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
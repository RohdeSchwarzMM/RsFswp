Save
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:SAVE

.. code-block:: python

	SENSe:CORRection:SAVE



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Save.SaveCls
	:members:
	:undoc-members:
	:noindex:
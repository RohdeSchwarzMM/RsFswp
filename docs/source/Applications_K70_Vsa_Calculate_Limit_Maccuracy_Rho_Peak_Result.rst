Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:RHO:PEAK:RESult

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:RHO:PEAK:RESult



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Rho.Peak.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
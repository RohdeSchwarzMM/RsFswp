Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:POWer<Source>:LEVel:MODE

.. code-block:: python

	SOURce:VOLTage:POWer<Source>:LEVel:MODE



.. autoclass:: RsFswp.Implementations.Source.Voltage.Power.Level.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
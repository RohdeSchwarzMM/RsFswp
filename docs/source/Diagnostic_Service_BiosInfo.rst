BiosInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:BIOSinfo

.. code-block:: python

	DIAGnostic:SERVice:BIOSinfo



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.BiosInfo.BiosInfoCls
	:members:
	:undoc-members:
	:noindex:
Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:SPECtrum:SPAN:MAXimum

.. code-block:: python

	SENSe:ADEMod:SPECtrum:SPAN:MAXimum



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Spectrum.Span.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:
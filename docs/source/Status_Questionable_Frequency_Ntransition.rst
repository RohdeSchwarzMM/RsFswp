Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:FREQuency:NTRansition

.. code-block:: python

	STATus:QUEStionable:FREQuency:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Frequency.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
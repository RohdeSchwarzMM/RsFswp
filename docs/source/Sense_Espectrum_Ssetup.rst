Ssetup
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:SSETup

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:SSETup



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Ssetup.SsetupCls
	:members:
	:undoc-members:
	:noindex:
Nsource
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:NSOurce

.. code-block:: python

	DIAGnostic:SERVice:NSOurce



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.Nsource.NsourceCls
	:members:
	:undoc-members:
	:noindex:
Service
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:SERVice

.. code-block:: python

	DIAGnostic:HUMS:SERVice



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.Service.ServiceCls
	:members:
	:undoc-members:
	:noindex:
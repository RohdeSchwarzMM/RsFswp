Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ACPower:ALTernate<Channel>:RESult:RELative

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ACPower:ALTernate<Channel>:RESult:RELative



.. autoclass:: RsFswp.Implementations.Calculate.Limit.AcPower.Alternate.Result.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex:
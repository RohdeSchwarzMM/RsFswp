Size
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:FPEaks:LIST:SIZE

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:FPEaks:LIST:SIZE



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Fpeaks.ListPy.Size.SizeCls
	:members:
	:undoc-members:
	:noindex:
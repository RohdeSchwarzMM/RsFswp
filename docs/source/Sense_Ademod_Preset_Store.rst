Store
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:PRESet:STORe

.. code-block:: python

	SENSe:ADEMod:PRESet:STORe



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Preset.Store.StoreCls
	:members:
	:undoc-members:
	:noindex:
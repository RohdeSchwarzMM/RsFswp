Header
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMat:DEXPort:HEADer

.. code-block:: python

	FORMat:DEXPort:HEADer



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.FormatPy.Dexport.Header.HeaderCls
	:members:
	:undoc-members:
	:noindex:
Modulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:FRAMe:EDIT:PREVious:MODulation

.. code-block:: python

	SENSe:DDEMod:PATTern:FRAMe:EDIT:PREVious:MODulation



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.Frame.Edit.Previous.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:
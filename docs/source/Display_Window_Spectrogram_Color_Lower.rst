Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SPECtrogram:COLor:LOWer

.. code-block:: python

	DISPlay:WINDow<Window>:SPECtrogram:COLor:LOWer



.. autoclass:: RsFswp.Implementations.Display.Window.Spectrogram.Color.Lower.LowerCls
	:members:
	:undoc-members:
	:noindex:
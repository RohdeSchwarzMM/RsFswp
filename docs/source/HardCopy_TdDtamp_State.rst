State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TDSTamp:STATe

.. code-block:: python

	HCOPy:TDSTamp:STATe



.. autoclass:: RsFswp.Implementations.HardCopy.TdDtamp.State.StateCls
	:members:
	:undoc-members:
	:noindex:
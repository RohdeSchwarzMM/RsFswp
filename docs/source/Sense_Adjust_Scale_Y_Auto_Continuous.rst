Continuous
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:SCALe:Y:AUTO:CONTinuous

.. code-block:: python

	SENSe:ADJust:SCALe:Y:AUTO:CONTinuous



.. autoclass:: RsFswp.Implementations.Sense.Adjust.Scale.Y.Auto.Continuous.ContinuousCls
	:members:
	:undoc-members:
	:noindex:
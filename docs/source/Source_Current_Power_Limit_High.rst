High
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:POWer<Source>:LIMit:HIGH

.. code-block:: python

	SOURce:CURRent:POWer<Source>:LIMit:HIGH



.. autoclass:: RsFswp.Implementations.Source.Current.Power.Limit.High.HighCls
	:members:
	:undoc-members:
	:noindex:
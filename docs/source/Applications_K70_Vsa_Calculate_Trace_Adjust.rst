Adjust
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Trace.Adjust.AdjustCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.calculate.trace.adjust.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_Calculate_Trace_Adjust_Alignment.rst
	Applications_K70_Vsa_Calculate_Trace_Adjust_Value.rst
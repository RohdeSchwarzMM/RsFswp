State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:LOAD:STATe 1,

.. code-block:: python

	MMEMory:LOAD:STATe 1,



.. autoclass:: RsFswp.Implementations.MassMemory.Load.State.StateCls
	:members:
	:undoc-members:
	:noindex:
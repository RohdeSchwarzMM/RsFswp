Apply
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:MSR:APPLy

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:MSR:APPLy



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Msr.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex:
Gauss
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:STATistics:CCDF:GAUSs

.. code-block:: python

	DISPlay:WINDow<Window>:STATistics:CCDF:GAUSs



.. autoclass:: RsFswp.Implementations.Display.Window.Statistics.CumulativeDistribFnc.Gauss.GaussCls
	:members:
	:undoc-members:
	:noindex:
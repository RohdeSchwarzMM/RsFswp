Peak
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:MAXimum:PEAK

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:MAXimum:PEAK



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.DeltaMarker.Maximum.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:STATe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Marker.State.StateCls
	:members:
	:undoc-members:
	:noindex:
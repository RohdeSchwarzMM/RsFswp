Numerator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EXTernal:FREQuency:FACTor:NUMerator

.. code-block:: python

	SOURce:EXTernal:FREQuency:FACTor:NUMerator



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Source.External.Frequency.Factor.Numerator.NumeratorCls
	:members:
	:undoc-members:
	:noindex:
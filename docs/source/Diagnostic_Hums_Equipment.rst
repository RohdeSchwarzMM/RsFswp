Equipment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:EQUipment

.. code-block:: python

	DIAGnostic:HUMS:EQUipment



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.Equipment.EquipmentCls
	:members:
	:undoc-members:
	:noindex:
Peak
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:SPECtrogram:XY:MAXimum:PEAK

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:SPECtrogram:XY:MAXimum:PEAK



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Spectrogram.Xy.Maximum.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:DIQ:STATe

.. code-block:: python

	OUTPut:DIQ:STATe



.. autoclass:: RsFswp.Implementations.Output.Diq.State.StateCls
	:members:
	:undoc-members:
	:noindex:
PeakSearch
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.PeakSearch.PeakSearchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.calculate.peakSearch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Calculate_PeakSearch_Pshow.rst
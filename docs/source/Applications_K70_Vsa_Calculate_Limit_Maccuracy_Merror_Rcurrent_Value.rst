Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:MERRor:RCURrent:VALue

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:MERRor:RCURrent:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Merror.Rcurrent.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
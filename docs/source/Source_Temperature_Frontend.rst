Frontend
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:TEMPerature:FRONtend

.. code-block:: python

	SOURce:TEMPerature:FRONtend



.. autoclass:: RsFswp.Implementations.Source.Temperature.Frontend.FrontendCls
	:members:
	:undoc-members:
	:noindex:
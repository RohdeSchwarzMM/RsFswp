X
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMAT REAL,32;TRACe<Window>:DATA:X

.. code-block:: python

	FORMAT REAL,32;TRACe<Window>:DATA:X



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Trace.Data.X.XCls
	:members:
	:undoc-members:
	:noindex:
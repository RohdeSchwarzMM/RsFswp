ConMeas
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:BLOCk:CONMeas

.. code-block:: python

	INITiate:BLOCk:CONMeas



.. autoclass:: RsFswp.Implementations.Initiate.Block.ConMeas.ConMeasCls
	:members:
	:undoc-members:
	:noindex:
Delete
----------------------------------------





.. autoclass:: RsFswp.Implementations.MassMemory.Delete.DeleteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.massMemory.delete.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MassMemory_Delete_Immediate.rst
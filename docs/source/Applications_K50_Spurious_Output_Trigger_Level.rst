Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:TRIGger<TriggerPort>:LEVel

.. code-block:: python

	OUTPut:TRIGger<TriggerPort>:LEVel



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Output.Trigger.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:
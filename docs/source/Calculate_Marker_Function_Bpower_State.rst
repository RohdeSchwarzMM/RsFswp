State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:BPOWer:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:BPOWer:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Bpower.State.StateCls
	:members:
	:undoc-members:
	:noindex:
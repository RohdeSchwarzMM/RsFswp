State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:FILTer:YIG:STATe

.. code-block:: python

	INPut<InputIx>:FILTer:YIG:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.InputPy.FilterPy.Yig.State.StateCls
	:members:
	:undoc-members:
	:noindex:
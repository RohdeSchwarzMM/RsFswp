Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger<TriggerPort>:SEQuence:HOLDoff:TIME

.. code-block:: python

	TRIGger<TriggerPort>:SEQuence:HOLDoff:TIME



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Trigger.Sequence.Holdoff.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:
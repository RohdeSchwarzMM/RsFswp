Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:LOSS:OUTPut:MODE

.. code-block:: python

	SENSe:CORRection:LOSS:OUTPut:MODE



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Loss.Output.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
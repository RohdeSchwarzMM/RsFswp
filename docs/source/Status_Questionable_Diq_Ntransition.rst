Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:DIQ:NTRansition

.. code-block:: python

	STATus:QUEStionable:DIQ:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Diq.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
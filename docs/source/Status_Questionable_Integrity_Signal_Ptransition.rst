Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INTegrity:SIGNal:PTRansition

.. code-block:: python

	STATus:QUEStionable:INTegrity:SIGNal:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Integrity.Signal.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
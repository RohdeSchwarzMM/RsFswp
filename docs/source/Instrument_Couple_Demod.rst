Demod
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:DEMod

.. code-block:: python

	INSTrument:COUPle:DEMod



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Demod.DemodCls
	:members:
	:undoc-members:
	:noindex:
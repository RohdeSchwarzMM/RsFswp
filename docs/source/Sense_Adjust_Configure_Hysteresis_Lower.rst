Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:CONFigure:HYSTeresis:LOWer

.. code-block:: python

	SENSe:ADJust:CONFigure:HYSTeresis:LOWer



.. autoclass:: RsFswp.Implementations.Sense.Adjust.Configure.Hysteresis.Lower.LowerCls
	:members:
	:undoc-members:
	:noindex:
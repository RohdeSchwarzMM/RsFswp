Window
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:IDENtify:WINDow

.. code-block:: python

	LAYout:IDENtify:WINDow



.. autoclass:: RsFswp.Implementations.Applications.K7_AnalogDemod.Layout.Identify.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:
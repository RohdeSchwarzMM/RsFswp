Nstate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:APSK:NSTate

.. code-block:: python

	SENSe:DDEMod:PATTern:APSK:NSTate



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.Apsk.Nstate.NstateCls
	:members:
	:undoc-members:
	:noindex:
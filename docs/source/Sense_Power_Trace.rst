Trace
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:TRACe

.. code-block:: python

	SENSe:POWer:TRACe



.. autoclass:: RsFswp.Implementations.Sense.Power.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:
Link
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:LINK

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:LINK



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Link.LinkCls
	:members:
	:undoc-members:
	:noindex:
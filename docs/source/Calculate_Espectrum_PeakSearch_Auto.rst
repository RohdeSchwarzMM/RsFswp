Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:ESPectrum:PSEarch:AUTO

.. code-block:: python

	CALCulate<Window>:ESPectrum:PSEarch:AUTO



.. autoclass:: RsFswp.Implementations.Calculate.Espectrum.PeakSearch.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
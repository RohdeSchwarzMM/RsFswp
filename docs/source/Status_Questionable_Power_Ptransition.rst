Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:POWer:PTRansition

.. code-block:: python

	STATus:QUEStionable:POWer:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Power.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
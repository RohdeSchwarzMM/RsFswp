Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:CORRection:ENABle

.. code-block:: python

	STATus:QUEStionable:CORRection:ENABle



.. autoclass:: RsFswp.Implementations.Status.Questionable.Correction.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
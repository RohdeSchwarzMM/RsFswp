Storage
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:STORage

.. code-block:: python

	DIAGnostic:HUMS:STORage



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.Storage.StorageCls
	:members:
	:undoc-members:
	:noindex:
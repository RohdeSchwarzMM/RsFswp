Pulse
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Generator.Pulse.PulseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.generator.pulse.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Generator_Pulse_Period.rst
	Source_Generator_Pulse_Trigger.rst
	Source_Generator_Pulse_Width.rst
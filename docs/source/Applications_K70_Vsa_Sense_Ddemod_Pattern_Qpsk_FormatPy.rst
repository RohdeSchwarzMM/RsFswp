FormatPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:QPSK:FORMat

.. code-block:: python

	SENSe:DDEMod:PATTern:QPSK:FORMat



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.Qpsk.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex:
Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:CFERror:PEAK:VALue

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:CFERror:PEAK:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.CfError.Peak.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
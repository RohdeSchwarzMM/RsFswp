State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:STATe

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:STATe



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.State.StateCls
	:members:
	:undoc-members:
	:noindex:
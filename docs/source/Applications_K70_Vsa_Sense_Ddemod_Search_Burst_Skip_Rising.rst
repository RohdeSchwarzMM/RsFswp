Rising
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SEARch:BURSt:SKIP:RISing

.. code-block:: python

	SENSe:DDEMod:SEARch:BURSt:SKIP:RISing



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Search.Burst.Skip.Rising.RisingCls
	:members:
	:undoc-members:
	:noindex:
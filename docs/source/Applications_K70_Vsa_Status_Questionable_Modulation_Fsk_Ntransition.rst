Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:MODulation<Window>:FSK:NTRansition

.. code-block:: python

	STATus:QUEStionable:MODulation<Window>:FSK:NTRansition



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Status.Questionable.Modulation.Fsk.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
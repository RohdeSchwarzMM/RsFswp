Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EXTernal<ExternalGen>:FREQuency

.. code-block:: python

	SOURce:EXTernal<ExternalGen>:FREQuency



.. autoclass:: RsFswp.Implementations.Source.External.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.external.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_External_Frequency_Coupling.rst
	Source_External_Frequency_Factor.rst
	Source_External_Frequency_Offset.rst
	Source_External_Frequency_Sweep.rst
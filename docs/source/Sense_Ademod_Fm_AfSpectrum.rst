AfSpectrum
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.Ademod.Fm.AfSpectrum.AfSpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ademod.fm.afSpectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Ademod_Fm_AfSpectrum_Result.rst
	Sense_Ademod_Fm_AfSpectrum_TypePy.rst
Sinfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:SINFo

.. code-block:: python

	DIAGnostic:SERVice:SINFo



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.Sinfo.SinfoCls
	:members:
	:undoc-members:
	:noindex:
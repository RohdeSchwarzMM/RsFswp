Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:PNOise:NTRansition

.. code-block:: python

	STATus:QUEStionable:PNOise:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Pnoise.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
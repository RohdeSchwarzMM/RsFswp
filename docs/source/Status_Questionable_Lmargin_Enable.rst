Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:LMARgin<Window>:ENABle

.. code-block:: python

	STATus:QUEStionable:LMARgin<Window>:ENABle



.. autoclass:: RsFswp.Implementations.Status.Questionable.Lmargin.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
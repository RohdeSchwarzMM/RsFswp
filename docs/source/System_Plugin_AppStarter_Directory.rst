Directory
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PLUGin:APPStarter:DIRectory

.. code-block:: python

	SYSTem:PLUGin:APPStarter:DIRectory



.. autoclass:: RsFswp.Implementations.System.Plugin.AppStarter.Directory.DirectoryCls
	:members:
	:undoc-members:
	:noindex:
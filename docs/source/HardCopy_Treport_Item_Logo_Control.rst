Control
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:ITEM:LOGO:CONTrol

.. code-block:: python

	HCOPy:TREPort:ITEM:LOGO:CONTrol



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Item.Logo.Control.ControlCls
	:members:
	:undoc-members:
	:noindex:
RefMeas
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:REFMeas

.. code-block:: python

	INITiate:REFMeas



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Initiate.RefMeas.RefMeasCls
	:members:
	:undoc-members:
	:noindex:
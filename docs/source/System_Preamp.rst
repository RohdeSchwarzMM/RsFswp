Preamp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PREamp

.. code-block:: python

	SYSTem:PREamp



.. autoclass:: RsFswp.Implementations.System.Preamp.PreampCls
	:members:
	:undoc-members:
	:noindex:
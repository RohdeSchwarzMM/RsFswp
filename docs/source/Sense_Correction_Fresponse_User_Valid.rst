Valid
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:VALid

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:VALid



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Valid.ValidCls
	:members:
	:undoc-members:
	:noindex:
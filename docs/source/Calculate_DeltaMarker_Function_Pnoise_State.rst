State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:PNOise:STATe

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:PNOise:STATe



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Function.Pnoise.State.StateCls
	:members:
	:undoc-members:
	:noindex:
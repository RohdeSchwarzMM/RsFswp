Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:CALibration:PTRansition

.. code-block:: python

	STATus:QUEStionable:CALibration:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Calibration.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
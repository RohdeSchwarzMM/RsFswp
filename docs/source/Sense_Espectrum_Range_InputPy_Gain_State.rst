State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:INPut:GAIN:STATe

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:INPut:GAIN:STATe



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Range.InputPy.Gain.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Comment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:TRACe<Trace>:COMMent

.. code-block:: python

	SENSe:SWEep:EGATe:TRACe<Trace>:COMMent



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Egate.Trace.Comment.CommentCls
	:members:
	:undoc-members:
	:noindex:
Hysteresis
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:IFPower:HYSTeresis

.. code-block:: python

	TRIGger:SEQuence:IFPower:HYSTeresis



.. autoclass:: RsFswp.Implementations.Trigger.Sequence.IfPower.Hysteresis.HysteresisCls
	:members:
	:undoc-members:
	:noindex:
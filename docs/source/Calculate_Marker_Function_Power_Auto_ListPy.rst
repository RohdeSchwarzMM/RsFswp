ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:POWer<SubBlock>:AUTO:LIST

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:POWer<SubBlock>:AUTO:LIST



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Power.Auto.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:
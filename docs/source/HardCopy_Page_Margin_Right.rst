Right
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:PAGE:MARGin:RIGHt

.. code-block:: python

	HCOPy:PAGE:MARGin:RIGHt



.. autoclass:: RsFswp.Implementations.HardCopy.Page.Margin.Right.RightCls
	:members:
	:undoc-members:
	:noindex:
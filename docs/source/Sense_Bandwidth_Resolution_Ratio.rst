Ratio
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BWIDth:RESolution:RATio

.. code-block:: python

	SENSe:BWIDth:RESolution:RATio



.. autoclass:: RsFswp.Implementations.Sense.Bandwidth.Resolution.Ratio.RatioCls
	:members:
	:undoc-members:
	:noindex:
Direction
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:TRIGger<TriggerPort>:DIRection

.. code-block:: python

	OUTPut<OutputConnector>:TRIGger<TriggerPort>:DIRection



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Output.Trigger.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex:
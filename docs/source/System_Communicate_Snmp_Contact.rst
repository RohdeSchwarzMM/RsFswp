Contact
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:SNMP:CONTact

.. code-block:: python

	SYSTem:COMMunicate:SNMP:CONTact



.. autoclass:: RsFswp.Implementations.System.Communicate.Snmp.Contact.ContactCls
	:members:
	:undoc-members:
	:noindex:
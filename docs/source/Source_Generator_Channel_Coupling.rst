Coupling
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:GENerator:CHANnel:COUPling

.. code-block:: python

	SOURce:GENerator:CHANnel:COUPling



.. autoclass:: RsFswp.Implementations.Source.Generator.Channel.Coupling.CouplingCls
	:members:
	:undoc-members:
	:noindex:
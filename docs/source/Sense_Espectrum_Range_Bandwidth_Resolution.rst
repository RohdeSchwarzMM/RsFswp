Resolution
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:BANDwidth:RESolution

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:BANDwidth:RESolution



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Range.Bandwidth.Resolution.ResolutionCls
	:members:
	:undoc-members:
	:noindex:
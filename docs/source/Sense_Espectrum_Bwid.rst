Bwid
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:BWID

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:BWID



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Bwid.BwidCls
	:members:
	:undoc-members:
	:noindex:
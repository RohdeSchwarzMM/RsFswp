FilterPy
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.ListPy.Range.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.listPy.range.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_ListPy_Range_FilterPy_TypePy.rst
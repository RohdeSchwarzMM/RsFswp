Alpha
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:FILTer:ALPHa

.. code-block:: python

	SENSe:DDEMod:FILTer:ALPHa



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.FilterPy.Alpha.AlphaCls
	:members:
	:undoc-members:
	:noindex:
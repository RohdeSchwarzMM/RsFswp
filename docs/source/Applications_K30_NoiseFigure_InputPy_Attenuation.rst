Attenuation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:ATTenuation

.. code-block:: python

	INPut<InputIx>:ATTenuation



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.InputPy.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:GAIN:STATe

.. code-block:: python

	INPut:GAIN:STATe



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.InputPy.Gain.State.StateCls
	:members:
	:undoc-members:
	:noindex:
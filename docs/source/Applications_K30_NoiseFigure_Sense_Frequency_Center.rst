Center
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:CENTer

.. code-block:: python

	SENSe:FREQuency:CENTer



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Frequency.Center.CenterCls
	:members:
	:undoc-members:
	:noindex:
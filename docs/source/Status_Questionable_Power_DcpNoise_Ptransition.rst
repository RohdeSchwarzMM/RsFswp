Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:POWer:DCPNoise:PTRansition

.. code-block:: python

	STATus:QUEStionable:POWer:DCPNoise:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Power.DcpNoise.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
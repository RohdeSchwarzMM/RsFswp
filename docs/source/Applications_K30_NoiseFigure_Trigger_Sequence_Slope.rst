Slope
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger<TriggerPort>:SEQuence:SLOPe

.. code-block:: python

	TRIGger<TriggerPort>:SEQuence:SLOPe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Trigger.Sequence.Slope.SlopeCls
	:members:
	:undoc-members:
	:noindex:
SymbolRate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:SRATe

.. code-block:: python

	TRACe:IQ:SRATe



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex:
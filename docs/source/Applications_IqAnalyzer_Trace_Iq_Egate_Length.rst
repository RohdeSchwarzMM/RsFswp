Length
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:EGATe:LENGth

.. code-block:: python

	TRACe:IQ:EGATe:LENGth



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.Egate.Length.LengthCls
	:members:
	:undoc-members:
	:noindex:
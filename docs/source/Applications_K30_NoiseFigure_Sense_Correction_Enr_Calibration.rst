Calibration
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Enr.Calibration.CalibrationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k30NoiseFigure.sense.correction.enr.calibration.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K30_NoiseFigure_Sense_Correction_Enr_Calibration_Mode.rst
	Applications_K30_NoiseFigure_Sense_Correction_Enr_Calibration_Sns.rst
	Applications_K30_NoiseFigure_Sense_Correction_Enr_Calibration_Spot.rst
	Applications_K30_NoiseFigure_Sense_Correction_Enr_Calibration_Table.rst
	Applications_K30_NoiseFigure_Sense_Correction_Enr_Calibration_TypePy.rst
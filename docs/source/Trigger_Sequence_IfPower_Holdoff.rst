Holdoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:IFPower:HOLDoff

.. code-block:: python

	TRIGger:SEQuence:IFPower:HOLDoff



.. autoclass:: RsFswp.Implementations.Trigger.Sequence.IfPower.Holdoff.HoldoffCls
	:members:
	:undoc-members:
	:noindex:
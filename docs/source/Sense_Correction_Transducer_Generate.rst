Generate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:TRANsducer:GENerate

.. code-block:: python

	SENSe:CORRection:TRANsducer:GENerate



.. autoclass:: RsFswp.Implementations.Sense.Correction.Transducer.Generate.GenerateCls
	:members:
	:undoc-members:
	:noindex:
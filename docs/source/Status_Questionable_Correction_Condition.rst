Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:CORRection:CONDition

.. code-block:: python

	STATus:QUEStionable:CORRection:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Correction.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
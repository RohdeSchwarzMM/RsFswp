Mref
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:MREF

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:MREF



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.DeltaMarker.Mref.MrefCls
	:members:
	:undoc-members:
	:noindex:
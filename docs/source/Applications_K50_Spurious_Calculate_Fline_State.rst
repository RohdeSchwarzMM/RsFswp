State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:FLINe<FreqLine>:STATe

.. code-block:: python

	CALCulate<Window>:FLINe<FreqLine>:STATe



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Fline.State.StateCls
	:members:
	:undoc-members:
	:noindex:
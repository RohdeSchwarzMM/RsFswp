Fft
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BWIDth:RESolution:FFT

.. code-block:: python

	SENSe:BWIDth:RESolution:FFT



.. autoclass:: RsFswp.Implementations.Sense.Bandwidth.Resolution.Fft.FftCls
	:members:
	:undoc-members:
	:noindex:
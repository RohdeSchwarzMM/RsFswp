Final
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:SELect:ITEM:FINal

.. code-block:: python

	MMEMory:SELect:ITEM:FINal



.. autoclass:: RsFswp.Implementations.MassMemory.Select.Item.Final.FinalCls
	:members:
	:undoc-members:
	:noindex:
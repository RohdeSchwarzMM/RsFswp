Aunit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:AUNit

.. code-block:: python

	INSTrument:COUPle:AUNit



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Aunit.AunitCls
	:members:
	:undoc-members:
	:noindex:
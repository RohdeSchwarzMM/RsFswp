Peak
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:MINimum:PEAK

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:MINimum:PEAK



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.Marker.Minimum.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:
Level
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Voltage.Auxiliary.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.voltage.auxiliary.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Voltage_Auxiliary_Level_Amplitude.rst
	Source_Voltage_Auxiliary_Level_Limit.rst
	Source_Voltage_Auxiliary_Level_State.rst
Deviation
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Fsk.Deviation.DeviationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.calculate.fsk.deviation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_Calculate_Fsk_Deviation_Compensation.rst
	Applications_K70_Vsa_Calculate_Fsk_Deviation_Reference.rst
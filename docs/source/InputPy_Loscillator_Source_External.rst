External
----------------------------------------





.. autoclass:: RsFswp.Implementations.InputPy.Loscillator.Source.External.ExternalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputPy.loscillator.source.external.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputPy_Loscillator_Source_External_Level.rst
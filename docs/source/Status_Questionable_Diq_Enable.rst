Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:DIQ:ENABle

.. code-block:: python

	STATus:QUEStionable:DIQ:ENABle



.. autoclass:: RsFswp.Implementations.Status.Questionable.Diq.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
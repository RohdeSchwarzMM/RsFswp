Factory
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:REVision:FACTory

.. code-block:: python

	SYSTem:REVision:FACTory



.. autoclass:: RsFswp.Implementations.System.Revision.Factory.FactoryCls
	:members:
	:undoc-members:
	:noindex:
Vswr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:MATCh:PREamp:VSWR

.. code-block:: python

	CALCulate<Window>:UNCertainty:MATCh:PREamp:VSWR



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Match.Preamp.Vswr.VswrCls
	:members:
	:undoc-members:
	:noindex:
Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TRANsducer:CONDition

.. code-block:: python

	STATus:QUEStionable:TRANsducer:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Transducer.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
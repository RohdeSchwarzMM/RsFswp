Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:SPCHeck:RESult

.. code-block:: python

	DIAGnostic:SERVice:SPCHeck:RESult



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.SpCheck.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
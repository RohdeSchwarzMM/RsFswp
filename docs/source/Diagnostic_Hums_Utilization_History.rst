History
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:UTILization:HISTory
	single: DIAGnostic:HUMS:UTILization:HISTory:DELete:ALL

.. code-block:: python

	DIAGnostic:HUMS:UTILization:HISTory
	DIAGnostic:HUMS:UTILization:HISTory:DELete:ALL



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.Utilization.History.HistoryCls
	:members:
	:undoc-members:
	:noindex:
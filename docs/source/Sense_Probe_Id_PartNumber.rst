PartNumber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PROBe<Probe>:ID:PARTnumber

.. code-block:: python

	SENSe:PROBe<Probe>:ID:PARTnumber



.. autoclass:: RsFswp.Implementations.Sense.Probe.Id.PartNumber.PartNumberCls
	:members:
	:undoc-members:
	:noindex:
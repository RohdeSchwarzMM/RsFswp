Aoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:AOFF

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:AOFF



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.DeltaMarker.Aoff.AoffCls
	:members:
	:undoc-members:
	:noindex:
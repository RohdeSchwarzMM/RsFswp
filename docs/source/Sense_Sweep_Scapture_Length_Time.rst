Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:SCAPture:LENGth:TIME

.. code-block:: python

	SENSe:SWEep:SCAPture:LENGth:TIME



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Scapture.Length.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:
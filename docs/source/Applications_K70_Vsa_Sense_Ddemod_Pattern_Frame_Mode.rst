Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:FRAMe:MODE

.. code-block:: python

	SENSe:DDEMod:PATTern:FRAMe:MODE



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.Frame.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
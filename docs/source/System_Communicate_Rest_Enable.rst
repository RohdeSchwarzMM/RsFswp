Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:REST:ENABle

.. code-block:: python

	SYSTem:COMMunicate:REST:ENABle



.. autoclass:: RsFswp.Implementations.System.Communicate.Rest.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
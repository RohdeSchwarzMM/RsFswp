AdcPrefilter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:ADCPrefilter

.. code-block:: python

	SENSe:ADEMod:ADCPrefilter



.. autoclass:: RsFswp.Implementations.Sense.Ademod.AdcPrefilter.AdcPrefilterCls
	:members:
	:undoc-members:
	:noindex:
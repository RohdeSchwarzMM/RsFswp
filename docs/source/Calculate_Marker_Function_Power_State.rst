State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:POWer<SubBlock>:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:POWer<SubBlock>:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Power.State.StateCls
	:members:
	:undoc-members:
	:noindex:
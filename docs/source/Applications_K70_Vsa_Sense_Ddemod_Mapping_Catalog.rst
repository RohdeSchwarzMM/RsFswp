Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:MAPPing:CATalog

.. code-block:: python

	SENSe:DDEMod:MAPPing:CATalog



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Mapping.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:
Direction
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:TRIGger<TriggerPort>:DIRection

.. code-block:: python

	OUTPut:TRIGger<TriggerPort>:DIRection



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Output.Trigger.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex:
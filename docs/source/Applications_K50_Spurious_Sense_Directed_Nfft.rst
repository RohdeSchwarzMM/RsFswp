Nfft
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DIRected:NFFT

.. code-block:: python

	SENSe:DIRected:NFFT



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Directed.Nfft.NfftCls
	:members:
	:undoc-members:
	:noindex:
Sns
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:MATCh:SOURce:SNS

.. code-block:: python

	CALCulate<Window>:UNCertainty:MATCh:SOURce:SNS



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Match.Source.Sns.SnsCls
	:members:
	:undoc-members:
	:noindex:
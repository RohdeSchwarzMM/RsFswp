Phase
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:FLISt<FileList>:DATA:PHASe

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:FLISt<FileList>:DATA:PHASe



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Flist.Data.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex:
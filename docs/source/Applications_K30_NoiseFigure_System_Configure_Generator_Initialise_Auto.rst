Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:CONFigure:GENerator:INITialise:AUTO

.. code-block:: python

	SYSTem:CONFigure:GENerator:INITialise:AUTO



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.System.Configure.Generator.Initialise.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
Load
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:EQUalizer:LOAD

.. code-block:: python

	SENSe:DDEMod:EQUalizer:LOAD



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Equalizer.Load.LoadCls
	:members:
	:undoc-members:
	:noindex:
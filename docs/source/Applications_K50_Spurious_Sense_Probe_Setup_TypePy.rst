TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PROBe<Probe>:SETup:TYPE

.. code-block:: python

	SENSe:PROBe<Probe>:SETup:TYPE



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Probe.Setup.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
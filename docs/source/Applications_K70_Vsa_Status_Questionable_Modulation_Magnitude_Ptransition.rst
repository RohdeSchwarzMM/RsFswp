Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:MODulation<Window>:MAGNitude:PTRansition

.. code-block:: python

	STATus:QUEStionable:MODulation<Window>:MAGNitude:PTRansition



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Status.Questionable.Modulation.Magnitude.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
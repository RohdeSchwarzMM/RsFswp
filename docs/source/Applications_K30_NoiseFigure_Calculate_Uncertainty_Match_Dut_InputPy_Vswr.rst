Vswr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:MATCh:DUT:IN:VSWR

.. code-block:: python

	CALCulate<Window>:UNCertainty:MATCh:DUT:IN:VSWR



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Match.Dut.InputPy.Vswr.VswrCls
	:members:
	:undoc-members:
	:noindex:
Vfirmware
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:RDEVice:OSCilloscope:VFIRmware

.. code-block:: python

	SYSTem:COMMunicate:RDEVice:OSCilloscope:VFIRmware



.. autoclass:: RsFswp.Implementations.System.Communicate.Rdevice.Oscilloscope.Vfirmware.VfirmwareCls
	:members:
	:undoc-members:
	:noindex:
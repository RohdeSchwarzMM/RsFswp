Length
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:LENGth

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:LENGth



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Display.Window.Trace.Length.LengthCls
	:members:
	:undoc-members:
	:noindex:
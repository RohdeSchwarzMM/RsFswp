Loffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:LOFFset

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:LOFFset



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.ListPy.Range.Loffset.LoffsetCls
	:members:
	:undoc-members:
	:noindex:
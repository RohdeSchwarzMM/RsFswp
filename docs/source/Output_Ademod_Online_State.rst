State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:ADEMod:ONLine:STATe

.. code-block:: python

	OUTPut<OutputConnector>:ADEMod:ONLine:STATe



.. autoclass:: RsFswp.Implementations.Output.Ademod.Online.State.StateCls
	:members:
	:undoc-members:
	:noindex:
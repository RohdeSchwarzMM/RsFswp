Link
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:LINK

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:LINK



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Marker.Link.LinkCls
	:members:
	:undoc-members:
	:noindex:
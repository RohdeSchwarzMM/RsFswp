State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:GAIN:STATe

.. code-block:: python

	INPut<InputIx>:GAIN:STATe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.InputPy.Gain.State.StateCls
	:members:
	:undoc-members:
	:noindex:
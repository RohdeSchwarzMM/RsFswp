HwSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:SELect:ITEM:HWSettings

.. code-block:: python

	MMEMory:SELect:ITEM:HWSettings



.. autoclass:: RsFswp.Implementations.MassMemory.Select.Item.HwSettings.HwSettingsCls
	:members:
	:undoc-members:
	:noindex:
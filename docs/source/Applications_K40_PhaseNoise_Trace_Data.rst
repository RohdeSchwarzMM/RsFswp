Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMAT REAL,32;TRACe<Window>:DATA

.. code-block:: python

	FORMAT REAL,32;TRACe<Window>:DATA



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Trace.Data.DataCls
	:members:
	:undoc-members:
	:noindex:
Bandwidth
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:BWIDth

.. code-block:: python

	SENSe:POWer:BWIDth



.. autoclass:: RsFswp.Implementations.Sense.Power.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex:
ConMeas
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:CONMeas

.. code-block:: python

	INITiate:CONMeas



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Initiate.ConMeas.ConMeasCls
	:members:
	:undoc-members:
	:noindex:
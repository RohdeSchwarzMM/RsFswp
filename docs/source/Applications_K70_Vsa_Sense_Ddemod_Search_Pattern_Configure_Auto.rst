Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SEARch:PATTern:CONFigure:AUTO

.. code-block:: python

	SENSe:DDEMod:SEARch:PATTern:CONFigure:AUTO



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Search.Pattern.Configure.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
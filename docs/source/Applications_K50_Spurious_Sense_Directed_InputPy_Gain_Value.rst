Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DIRected:INPut:GAIN:VALue

.. code-block:: python

	SENSe:DIRected:INPut:GAIN:VALue



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Directed.InputPy.Gain.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
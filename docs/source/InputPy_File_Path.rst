Path
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:FILE:PATH

.. code-block:: python

	INPut:FILE:PATH



.. autoclass:: RsFswp.Implementations.InputPy.File.Path.PathCls
	:members:
	:undoc-members:
	:noindex:
Vswr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:MATCh:SOURce:CALibration:VSWR

.. code-block:: python

	CALCulate<Window>:UNCertainty:MATCh:SOURce:CALibration:VSWR



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Match.Source.Calibration.Vswr.VswrCls
	:members:
	:undoc-members:
	:noindex:
Power
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNIT:POWer

.. code-block:: python

	CALCulate<Window>:UNIT:POWer



.. autoclass:: RsFswp.Implementations.Calculate.Unit.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:
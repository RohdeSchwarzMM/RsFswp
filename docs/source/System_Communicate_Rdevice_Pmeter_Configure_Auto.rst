Auto
----------------------------------------





.. autoclass:: RsFswp.Implementations.System.Communicate.Rdevice.Pmeter.Configure.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.communicate.rdevice.pmeter.configure.auto.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_Rdevice_Pmeter_Configure_Auto_State.rst
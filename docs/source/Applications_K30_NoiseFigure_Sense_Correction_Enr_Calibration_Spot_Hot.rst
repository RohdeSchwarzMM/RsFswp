Hot
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:ENR:CALibration:SPOT:HOT

.. code-block:: python

	SENSe:CORRection:ENR:CALibration:SPOT:HOT



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Enr.Calibration.Spot.Hot.HotCls
	:members:
	:undoc-members:
	:noindex:
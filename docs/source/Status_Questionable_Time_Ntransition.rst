Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TIME:NTRansition

.. code-block:: python

	STATus:QUEStionable:TIME:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Time.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
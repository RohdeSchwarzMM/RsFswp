Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:SPACing:GAP<GapChannel>:MANual:UPPer

.. code-block:: python

	SENSe:POWer:ACHannel:SPACing:GAP<GapChannel>:MANual:UPPer



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Spacing.Gap.Manual.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex:
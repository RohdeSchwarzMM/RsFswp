Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:FPEaks:IMMediate

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:FPEaks:IMMediate



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Fpeaks.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
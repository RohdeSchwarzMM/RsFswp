Otype
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:TRIGger<TriggerPort>:OTYPe

.. code-block:: python

	OUTPut:TRIGger<TriggerPort>:OTYPe



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Output.Trigger.Otype.OtypeCls
	:members:
	:undoc-members:
	:noindex:
SrError
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:NORMalize:SRERror

.. code-block:: python

	SENSe:DDEMod:NORMalize:SRERror



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Normalize.SrError.SrErrorCls
	:members:
	:undoc-members:
	:noindex:
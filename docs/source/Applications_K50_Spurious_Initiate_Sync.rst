Sync
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:SYNC

.. code-block:: python

	INITiate:SYNC



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Initiate.Sync.SyncCls
	:members:
	:undoc-members:
	:noindex:
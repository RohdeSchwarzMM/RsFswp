Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:PNOise:EVENt

.. code-block:: python

	STATus:QUEStionable:PNOise:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Pnoise.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
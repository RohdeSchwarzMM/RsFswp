Width
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:GENerator:PULSe:WIDTh

.. code-block:: python

	SOURce:GENerator:PULSe:WIDTh



.. autoclass:: RsFswp.Implementations.Source.Generator.Pulse.Width.WidthCls
	:members:
	:undoc-members:
	:noindex:
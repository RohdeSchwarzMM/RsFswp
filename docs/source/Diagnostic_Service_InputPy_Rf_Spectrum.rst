Spectrum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:INPut:RF:SPECtrum

.. code-block:: python

	DIAGnostic:SERVice:INPut:RF:SPECtrum



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.InputPy.Rf.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:
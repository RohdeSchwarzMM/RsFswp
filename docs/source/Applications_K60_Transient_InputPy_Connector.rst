Connector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:CONNector

.. code-block:: python

	INPut:CONNector



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.InputPy.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex:
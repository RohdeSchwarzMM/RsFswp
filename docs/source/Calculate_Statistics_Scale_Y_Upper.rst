Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:STATistics:SCALe:Y:UPPer

.. code-block:: python

	CALCulate<Window>:STATistics:SCALe:Y:UPPer



.. autoclass:: RsFswp.Implementations.Calculate.Statistics.Scale.Y.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex:
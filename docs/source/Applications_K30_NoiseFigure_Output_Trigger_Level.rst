Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:TRIGger<TriggerPort>:LEVel

.. code-block:: python

	OUTPut<OutputConnector>:TRIGger<TriggerPort>:LEVel



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Output.Trigger.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:
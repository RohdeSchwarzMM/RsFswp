State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FILTer<FilterPy>:CCIR:UNWeighted:STATe

.. code-block:: python

	SENSe:FILTer<FilterPy>:CCIR:UNWeighted:STATe



.. autoclass:: RsFswp.Implementations.Sense.FilterPy.Ccir.Unweighted.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Transducer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:TRANsducer

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:TRANsducer



.. autoclass:: RsFswp.Implementations.Sense.ListPy.Range.Transducer.TransducerCls
	:members:
	:undoc-members:
	:noindex:
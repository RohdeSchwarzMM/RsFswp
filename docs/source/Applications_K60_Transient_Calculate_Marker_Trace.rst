Trace
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:TRACe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:TRACe



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.Marker.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:
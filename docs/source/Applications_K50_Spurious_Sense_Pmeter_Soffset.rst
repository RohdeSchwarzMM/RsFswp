Soffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:SOFFset

.. code-block:: python

	SENSe:PMETer<PowerMeter>:SOFFset



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Pmeter.Soffset.SoffsetCls
	:members:
	:undoc-members:
	:noindex:
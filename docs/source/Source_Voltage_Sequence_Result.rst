Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:SEQuence:RESult

.. code-block:: python

	SOURce:VOLTage:SEQuence:RESult



.. autoclass:: RsFswp.Implementations.Source.Voltage.Sequence.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
Loffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DIRected:LOFFset

.. code-block:: python

	SENSe:DIRected:LOFFset



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Directed.Loffset.LoffsetCls
	:members:
	:undoc-members:
	:noindex:
Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TIME:PTRansition

.. code-block:: python

	STATus:QUEStionable:TIME:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Time.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:AVERage:STATe

.. code-block:: python

	TRACe:IQ:AVERage:STATe



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.Average.State.StateCls
	:members:
	:undoc-members:
	:noindex:
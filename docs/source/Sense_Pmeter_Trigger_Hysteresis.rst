Hysteresis
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:TRIGger:HYSTeresis

.. code-block:: python

	SENSe:PMETer<PowerMeter>:TRIGger:HYSTeresis



.. autoclass:: RsFswp.Implementations.Sense.Pmeter.Trigger.Hysteresis.HysteresisCls
	:members:
	:undoc-members:
	:noindex:
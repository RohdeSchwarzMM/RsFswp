Impedance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:IQ:IMPedance

.. code-block:: python

	SENSe:IQ:IMPedance



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Sense.Iq.Impedance.ImpedanceCls
	:members:
	:undoc-members:
	:noindex:
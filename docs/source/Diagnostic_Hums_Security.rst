Security
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:SECurity

.. code-block:: python

	DIAGnostic:HUMS:SECurity



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.Security.SecurityCls
	:members:
	:undoc-members:
	:noindex:
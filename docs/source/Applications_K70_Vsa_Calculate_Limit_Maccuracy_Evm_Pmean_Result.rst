Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:EVM:PMEan:RESult

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:EVM:PMEan:RESult



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Evm.Pmean.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
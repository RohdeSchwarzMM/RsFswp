Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:PCALibration:EVENt

.. code-block:: python

	STATus:OPERation:PCALibration:EVENt



.. autoclass:: RsFswp.Implementations.Status.Operation.Pcalibration.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
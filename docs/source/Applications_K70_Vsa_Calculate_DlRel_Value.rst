Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DLRel:VALue

.. code-block:: python

	CALCulate<Window>:DLRel:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.DlRel.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
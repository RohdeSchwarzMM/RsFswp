Name
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:NAME

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:NAME



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Name.NameCls
	:members:
	:undoc-members:
	:noindex:
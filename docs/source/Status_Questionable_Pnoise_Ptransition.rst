Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:PNOise:PTRansition

.. code-block:: python

	STATus:QUEStionable:PNOise:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Pnoise.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
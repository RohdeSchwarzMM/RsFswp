Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:RMS:AVERage:RESult

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:RMS:AVERage:RESult



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Summary.Rms.Average.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
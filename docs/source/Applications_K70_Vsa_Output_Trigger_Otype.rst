Otype
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:TRIGger<TriggerPort>:OTYPe

.. code-block:: python

	OUTPut<OutputConnector>:TRIGger<TriggerPort>:OTYPe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Output.Trigger.Otype.OtypeCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EXTernal<ExternalGen>:FREQuency:SWEep:STATe

.. code-block:: python

	SOURce:EXTernal<ExternalGen>:FREQuency:SWEep:STATe



.. autoclass:: RsFswp.Implementations.Source.External.Frequency.Sweep.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Params
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PLUGin:APPStarter:PARams

.. code-block:: python

	SYSTem:PLUGin:APPStarter:PARams



.. autoclass:: RsFswp.Implementations.System.Plugin.AppStarter.Params.ParamsCls
	:members:
	:undoc-members:
	:noindex:
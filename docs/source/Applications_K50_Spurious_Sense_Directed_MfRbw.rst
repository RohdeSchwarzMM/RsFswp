MfRbw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DIRected:MFRBw

.. code-block:: python

	SENSe:DIRected:MFRBw



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Directed.MfRbw.MfRbwCls
	:members:
	:undoc-members:
	:noindex:
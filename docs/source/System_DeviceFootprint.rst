DeviceFootprint
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:DFPRint

.. code-block:: python

	SYSTem:DFPRint



.. autoclass:: RsFswp.Implementations.System.DeviceFootprint.DeviceFootprintCls
	:members:
	:undoc-members:
	:noindex:
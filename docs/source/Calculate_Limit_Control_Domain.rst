Domain
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:CONTrol:DOMain

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:CONTrol:DOMain



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Control.Domain.DomainCls
	:members:
	:undoc-members:
	:noindex:
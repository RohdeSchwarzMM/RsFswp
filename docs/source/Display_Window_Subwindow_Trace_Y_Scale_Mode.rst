Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:Y:SCALe:MODE

.. code-block:: python

	DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:Y:SCALe:MODE



.. autoclass:: RsFswp.Implementations.Display.Window.Subwindow.Trace.Y.Scale.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
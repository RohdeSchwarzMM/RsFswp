Comment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:TRANsducer:COMMent

.. code-block:: python

	SENSe:CORRection:TRANsducer:COMMent



.. autoclass:: RsFswp.Implementations.Sense.Correction.Transducer.Comment.CommentCls
	:members:
	:undoc-members:
	:noindex:
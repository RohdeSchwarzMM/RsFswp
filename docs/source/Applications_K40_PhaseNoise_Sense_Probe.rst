Probe<Probe>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.applications.k40PhaseNoise.sense.probe.repcap_probe_get()
	driver.applications.k40PhaseNoise.sense.probe.repcap_probe_set(repcap.Probe.Nr1)





.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Sense.Probe.ProbeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k40PhaseNoise.sense.probe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K40_PhaseNoise_Sense_Probe_Id.rst
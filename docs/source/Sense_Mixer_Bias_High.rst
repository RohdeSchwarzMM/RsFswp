High
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:BIAS:HIGH

.. code-block:: python

	SENSe:MIXer:BIAS:HIGH



.. autoclass:: RsFswp.Implementations.Sense.Mixer.Bias.High.HighCls
	:members:
	:undoc-members:
	:noindex:
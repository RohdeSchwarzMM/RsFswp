Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:CALibration:NTRansition

.. code-block:: python

	STATus:QUEStionable:CALibration:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Calibration.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
Stop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:STOP

.. code-block:: python

	SENSe:FREQuency:STOP



.. autoclass:: RsFswp.Implementations.Sense.Frequency.Stop.StopCls
	:members:
	:undoc-members:
	:noindex:
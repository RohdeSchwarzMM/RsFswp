State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:RDEVice:OSCilloscope:STATe

.. code-block:: python

	SYSTem:COMMunicate:RDEVice:OSCilloscope:STATe



.. autoclass:: RsFswp.Implementations.System.Communicate.Rdevice.Oscilloscope.State.StateCls
	:members:
	:undoc-members:
	:noindex:
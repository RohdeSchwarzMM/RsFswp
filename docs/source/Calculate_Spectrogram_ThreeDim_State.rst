State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:SPECtrogram:THReedim:STATe

.. code-block:: python

	CALCulate<Window>:SPECtrogram:THReedim:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Spectrogram.ThreeDim.State.StateCls
	:members:
	:undoc-members:
	:noindex:
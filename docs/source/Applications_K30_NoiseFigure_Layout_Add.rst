Add
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Layout.Add.AddCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k30NoiseFigure.layout.add.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K30_NoiseFigure_Layout_Add_Window.rst
Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:CONTrol:DATA

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:CONTrol:DATA



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Limit.Control.Data.DataCls
	:members:
	:undoc-members:
	:noindex:
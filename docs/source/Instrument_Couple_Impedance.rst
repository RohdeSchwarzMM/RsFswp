Impedance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:IMPedance

.. code-block:: python

	INSTrument:COUPle:IMPedance



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Impedance.ImpedanceCls
	:members:
	:undoc-members:
	:noindex:
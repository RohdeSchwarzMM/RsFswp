Adroop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:ADRoop

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:ADRoop



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Marker.Function.Ddemod.Statistic.Adroop.AdroopCls
	:members:
	:undoc-members:
	:noindex:
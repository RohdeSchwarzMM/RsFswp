Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:SYNC:EVENt

.. code-block:: python

	STATus:QUEStionable:SYNC:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Sync.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TRANsducer:PTRansition

.. code-block:: python

	STATus:QUEStionable:TRANsducer:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Transducer.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
Terminator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:TERMinator

.. code-block:: python

	INPut<InputIx>:TERMinator



.. autoclass:: RsFswp.Implementations.InputPy.Terminator.TerminatorCls
	:members:
	:undoc-members:
	:noindex:
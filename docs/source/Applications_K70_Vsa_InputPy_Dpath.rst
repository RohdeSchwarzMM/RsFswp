Dpath
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:DPATh

.. code-block:: python

	INPut<InputIx>:DPATh



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.InputPy.Dpath.DpathCls
	:members:
	:undoc-members:
	:noindex:
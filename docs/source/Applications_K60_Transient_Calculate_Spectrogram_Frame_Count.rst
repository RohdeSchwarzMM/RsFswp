Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:SPECtrogram:FRAMe:COUNt

.. code-block:: python

	CALCulate<Window>:SPECtrogram:FRAMe:COUNt



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.Spectrogram.Frame.Count.CountCls
	:members:
	:undoc-members:
	:noindex:
Scovered
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:SCOVered

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:SCOVered



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Scovered.ScoveredCls
	:members:
	:undoc-members:
	:noindex:
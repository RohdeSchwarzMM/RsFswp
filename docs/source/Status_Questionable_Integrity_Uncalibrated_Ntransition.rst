Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INTegrity:UNCalibrated:NTRansition

.. code-block:: python

	STATus:QUEStionable:INTegrity:UNCalibrated:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Integrity.Uncalibrated.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CONFigure:MODE:SYSTem:LO:FREQuency

.. code-block:: python

	SENSe:CONFigure:MODE:SYSTem:LO:FREQuency



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Configure.Mode.System.Lo.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:
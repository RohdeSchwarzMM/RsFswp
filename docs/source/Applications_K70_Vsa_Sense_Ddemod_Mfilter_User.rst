User
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:MFILter:USER

.. code-block:: python

	SENSe:DDEMod:MFILter:USER



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Mfilter.User.UserCls
	:members:
	:undoc-members:
	:noindex:
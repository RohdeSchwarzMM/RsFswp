Csetup
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:SELect:ITEM:CSETup

.. code-block:: python

	MMEMory:SELect:ITEM:CSETup



.. autoclass:: RsFswp.Implementations.MassMemory.Select.Item.Csetup.CsetupCls
	:members:
	:undoc-members:
	:noindex:
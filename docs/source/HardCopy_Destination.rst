Destination
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:DESTination

.. code-block:: python

	HCOPy:DESTination



.. autoclass:: RsFswp.Implementations.HardCopy.Destination.DestinationCls
	:members:
	:undoc-members:
	:noindex:
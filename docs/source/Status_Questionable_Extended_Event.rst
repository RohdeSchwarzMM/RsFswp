Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:EXTended:EVENt

.. code-block:: python

	STATus:QUEStionable:EXTended:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Extended.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
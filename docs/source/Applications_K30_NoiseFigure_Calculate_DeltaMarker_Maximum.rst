Maximum
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.DeltaMarker.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k30NoiseFigure.calculate.deltaMarker.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K30_NoiseFigure_Calculate_DeltaMarker_Maximum_Next.rst
	Applications_K30_NoiseFigure_Calculate_DeltaMarker_Maximum_Peak.rst
Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MATH:MODE

.. code-block:: python

	CALCulate<Window>:MATH:MODE



.. autoclass:: RsFswp.Implementations.Calculate.Math.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WINDow<Window>:DETector<Trace>:FUNCtion:AUTO

.. code-block:: python

	SENSe:WINDow<Window>:DETector<Trace>:FUNCtion:AUTO



.. autoclass:: RsFswp.Implementations.Sense.Window.Detector.Function.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
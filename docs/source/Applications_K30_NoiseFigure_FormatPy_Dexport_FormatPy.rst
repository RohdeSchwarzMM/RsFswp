FormatPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMat:DEXPort:FORMat

.. code-block:: python

	FORMat:DEXPort:FORMat



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.FormatPy.Dexport.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex:
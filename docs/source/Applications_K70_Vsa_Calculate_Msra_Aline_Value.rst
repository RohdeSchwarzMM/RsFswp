Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MSRA:ALINe:VALue

.. code-block:: python

	CALCulate<Window>:MSRA:ALINe:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Msra.Aline.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
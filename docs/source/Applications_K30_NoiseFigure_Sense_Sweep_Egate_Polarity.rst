Polarity
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:POLarity

.. code-block:: python

	SENSe:SWEep:EGATe:POLarity



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Sweep.Egate.Polarity.PolarityCls
	:members:
	:undoc-members:
	:noindex:
Probe<Probe>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.output.probe.repcap_probe_get()
	driver.output.probe.repcap_probe_set(repcap.Probe.Nr1)





.. autoclass:: RsFswp.Implementations.Output.Probe.ProbeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.output.probe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Output_Probe_Power.rst
Ident
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:FORMat:IDENt

.. code-block:: python

	SYSTem:FORMat:IDENt



.. autoclass:: RsFswp.Implementations.System.FormatPy.Ident.IdentCls
	:members:
	:undoc-members:
	:noindex:
Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:ELIN:VALue

.. code-block:: python

	CALCulate<Window>:ELIN:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Elin.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
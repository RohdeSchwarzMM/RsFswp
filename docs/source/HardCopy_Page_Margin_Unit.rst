Unit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:PAGE:MARGin:UNIT

.. code-block:: python

	HCOPy:PAGE:MARGin:UNIT



.. autoclass:: RsFswp.Implementations.HardCopy.Page.Margin.Unit.UnitCls
	:members:
	:undoc-members:
	:noindex:
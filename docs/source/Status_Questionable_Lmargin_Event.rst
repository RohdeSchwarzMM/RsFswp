Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:LMARgin<Window>:EVENt

.. code-block:: python

	STATus:QUEStionable:LMARgin<Window>:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Lmargin.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
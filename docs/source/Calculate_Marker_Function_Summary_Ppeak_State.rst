State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:PPEak:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:PPEak:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Summary.Ppeak.State.StateCls
	:members:
	:undoc-members:
	:noindex:
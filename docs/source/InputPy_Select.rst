Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:SELect

.. code-block:: python

	INPut<InputIx>:SELect



.. autoclass:: RsFswp.Implementations.InputPy.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
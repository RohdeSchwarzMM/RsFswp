Expression
----------------------------------------





.. autoclass:: RsFswp.Implementations.Calculate.Math.Expression.ExpressionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.math.expression.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Math_Expression_Define.rst
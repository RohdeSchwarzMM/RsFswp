Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:ANNotation:FREQuency

.. code-block:: python

	DISPlay:ANNotation:FREQuency



.. autoclass:: RsFswp.Implementations.Display.Annotation.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:
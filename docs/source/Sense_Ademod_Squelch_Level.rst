Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:SQUelch:LEVel

.. code-block:: python

	SENSe:ADEMod:SQUelch:LEVel



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Squelch.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:
Resolution
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:SPECtrum:BWIDth:RESolution

.. code-block:: python

	SENSe:ADEMod:SPECtrum:BWIDth:RESolution



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Spectrum.Bandwidth.Resolution.ResolutionCls
	:members:
	:undoc-members:
	:noindex:
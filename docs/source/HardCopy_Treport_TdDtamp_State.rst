State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:TDSTamp:STATe

.. code-block:: python

	HCOPy:TREPort:TDSTamp:STATe



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.TdDtamp.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Source
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:SOURce

.. code-block:: python

	SENSe:SWEep:EGATe:SOURce



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Sweep.Egate.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:
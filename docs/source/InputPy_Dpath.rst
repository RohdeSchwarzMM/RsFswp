Dpath
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:DPATh

.. code-block:: python

	INPut:DPATh



.. autoclass:: RsFswp.Implementations.InputPy.Dpath.DpathCls
	:members:
	:undoc-members:
	:noindex:
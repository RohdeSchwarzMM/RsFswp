Power
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:PROBe<Probe>:POWer

.. code-block:: python

	OUTPut<OutputConnector>:PROBe<Probe>:POWer



.. autoclass:: RsFswp.Implementations.Output.Probe.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:
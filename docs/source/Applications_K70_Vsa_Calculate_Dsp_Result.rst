Result
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Dsp.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.calculate.dsp.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_Calculate_Dsp_Result_Capture.rst
	Applications_K70_Vsa_Calculate_Dsp_Result_Rrange.rst
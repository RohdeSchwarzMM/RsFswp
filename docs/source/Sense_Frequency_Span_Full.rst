Full
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:SPAN:FULL

.. code-block:: python

	SENSe:FREQuency:SPAN:FULL



.. autoclass:: RsFswp.Implementations.Sense.Frequency.Span.Full.FullCls
	:members:
	:undoc-members:
	:noindex:
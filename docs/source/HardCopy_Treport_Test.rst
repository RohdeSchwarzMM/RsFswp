Test
----------------------------------------





.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Test.TestCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hardCopy.treport.test.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	HardCopy_Treport_Test_Remove.rst
	HardCopy_Treport_Test_Select.rst
Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:IMMediate

.. code-block:: python

	INITiate:IMMediate



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Initiate.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
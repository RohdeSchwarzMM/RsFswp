Slope
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:TRIGger:SLOPe

.. code-block:: python

	SENSe:PMETer<PowerMeter>:TRIGger:SLOPe



.. autoclass:: RsFswp.Implementations.Sense.Pmeter.Trigger.Slope.SlopeCls
	:members:
	:undoc-members:
	:noindex:
Distance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:INPut:MC:DISTance

.. code-block:: python

	DIAGnostic:SERVice:INPut:MC:DISTance



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.InputPy.Mc.Distance.DistanceCls
	:members:
	:undoc-members:
	:noindex:
Nstate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SEARch:SYNC:NSTate

.. code-block:: python

	SENSe:DDEMod:SEARch:SYNC:NSTate



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Search.Sync.Nstate.NstateCls
	:members:
	:undoc-members:
	:noindex:
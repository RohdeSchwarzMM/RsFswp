Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TEMPerature:EVENt

.. code-block:: python

	STATus:QUEStionable:TEMPerature:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Temperature.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
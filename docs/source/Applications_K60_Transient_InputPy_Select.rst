Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:SELect

.. code-block:: python

	INPut:SELect



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.InputPy.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
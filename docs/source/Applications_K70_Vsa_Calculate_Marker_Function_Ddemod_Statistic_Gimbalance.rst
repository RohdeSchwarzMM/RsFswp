Gimbalance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:GIMBalance

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:GIMBalance



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Marker.Function.Ddemod.Statistic.Gimbalance.GimbalanceCls
	:members:
	:undoc-members:
	:noindex:
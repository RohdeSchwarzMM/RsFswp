Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:DATA:FREQuency

.. code-block:: python

	CALCulate<Window>:UNCertainty:DATA:FREQuency



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Data.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:
Transfer
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Transfer.TransferCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.sense.transfer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Sense_Transfer_Segment.rst
	Applications_K50_Spurious_Sense_Transfer_Spur.rst
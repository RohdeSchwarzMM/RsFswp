Measured
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Fetch.Pnoise.Measured.MeasuredCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k40PhaseNoise.fetch.pnoise.measured.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K40_PhaseNoise_Fetch_Pnoise_Measured_Frequency.rst
	Applications_K40_PhaseNoise_Fetch_Pnoise_Measured_Level.rst
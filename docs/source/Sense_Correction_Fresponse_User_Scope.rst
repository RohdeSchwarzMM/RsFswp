Scope
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:SCOPe

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:SCOPe



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Scope.ScopeCls
	:members:
	:undoc-members:
	:noindex:
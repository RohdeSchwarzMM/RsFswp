Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:UPPer:DATA

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:UPPer:DATA



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Limit.Upper.Data.DataCls
	:members:
	:undoc-members:
	:noindex:
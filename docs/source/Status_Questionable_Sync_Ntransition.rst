Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:SYNC:NTRansition

.. code-block:: python

	STATus:QUEStionable:SYNC:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Sync.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
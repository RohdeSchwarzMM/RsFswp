Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:CONDition

.. code-block:: python

	STATus:QUEStionable:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
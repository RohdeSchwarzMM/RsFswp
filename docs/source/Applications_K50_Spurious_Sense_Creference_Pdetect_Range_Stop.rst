Stop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:PDETect:RANGe:STOP

.. code-block:: python

	SENSe:CREFerence:PDETect:RANGe:STOP



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Pdetect.Range.Stop.StopCls
	:members:
	:undoc-members:
	:noindex:
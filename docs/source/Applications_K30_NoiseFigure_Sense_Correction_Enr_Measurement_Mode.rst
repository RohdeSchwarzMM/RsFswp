Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:ENR:MEASurement:MODE

.. code-block:: python

	SENSe:CORRection:ENR:MEASurement:MODE



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Enr.Measurement.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
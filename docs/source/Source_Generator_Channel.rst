Channel
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Generator.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.generator.channel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Generator_Channel_Coupling.rst
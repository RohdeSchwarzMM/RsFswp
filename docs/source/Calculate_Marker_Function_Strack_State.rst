State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:STRack:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:STRack:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Strack.State.StateCls
	:members:
	:undoc-members:
	:noindex:
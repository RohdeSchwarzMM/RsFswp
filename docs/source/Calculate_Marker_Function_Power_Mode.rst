Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:POWer<SubBlock>:MODE

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:POWer<SubBlock>:MODE



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Power.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
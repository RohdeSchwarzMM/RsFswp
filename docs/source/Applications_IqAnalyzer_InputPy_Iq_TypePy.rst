TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:IQ:TYPE

.. code-block:: python

	INPut<InputIx>:IQ:TYPE



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.InputPy.Iq.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
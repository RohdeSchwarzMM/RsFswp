Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:SPECtrogram:FRAMe:SELect

.. code-block:: python

	CALCulate<Window>:SPECtrogram:FRAMe:SELect



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.Spectrogram.Frame.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
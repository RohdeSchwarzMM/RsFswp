Offset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:OFFSet

.. code-block:: python

	SENSe:FREQuency:OFFSet



.. autoclass:: RsFswp.Implementations.Sense.Frequency.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:
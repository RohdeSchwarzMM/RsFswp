FormatPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:FORMat

.. code-block:: python

	DIAGnostic:HUMS:FORMat



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex:
Source
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:SOURce

.. code-block:: python

	TRIGger:SEQuence:SOURce



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Trigger.Sequence.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:
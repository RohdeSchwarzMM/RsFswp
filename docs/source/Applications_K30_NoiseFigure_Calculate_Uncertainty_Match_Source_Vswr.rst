Vswr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:MATCh:SOURce:VSWR

.. code-block:: python

	CALCulate<Window>:UNCertainty:MATCh:SOURce:VSWR



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Match.Source.Vswr.VswrCls
	:members:
	:undoc-members:
	:noindex:
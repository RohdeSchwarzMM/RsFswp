Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:LOSCillator:SOURce:EXTernal:LEVel

.. code-block:: python

	INPut<InputIx>:LOSCillator:SOURce:EXTernal:LEVel



.. autoclass:: RsFswp.Implementations.InputPy.Loscillator.Source.External.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:
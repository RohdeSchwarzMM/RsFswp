Range
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:STATistics:SCALe:X:RANGe

.. code-block:: python

	CALCulate<Window>:STATistics:SCALe:X:RANGe



.. autoclass:: RsFswp.Implementations.Calculate.Statistics.Scale.X.Range.RangeCls
	:members:
	:undoc-members:
	:noindex:
Current
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Current_Auxiliary.rst
	Source_Current_Control.rst
	Source_Current_Power.rst
	Source_Current_Sequence.rst
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SEARch:SYNC:STATe

.. code-block:: python

	SENSe:DDEMod:SEARch:SYNC:STATe



.. autoclass:: RsFswp.Implementations.Sense.Ddemod.Search.Sync.State.StateCls
	:members:
	:undoc-members:
	:noindex:
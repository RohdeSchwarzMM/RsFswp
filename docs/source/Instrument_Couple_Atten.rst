Atten
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:ATTen

.. code-block:: python

	INSTrument:COUPle:ATTen



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Atten.AttenCls
	:members:
	:undoc-members:
	:noindex:
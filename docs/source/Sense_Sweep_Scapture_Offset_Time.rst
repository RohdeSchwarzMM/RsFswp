Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:SCAPture:OFFSet:TIME

.. code-block:: python

	SENSe:SWEep:SCAPture:OFFSet:TIME



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Scapture.Offset.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:SECurity:STATe

.. code-block:: python

	SYSTem:SECurity:STATe



.. autoclass:: RsFswp.Implementations.System.Security.State.StateCls
	:members:
	:undoc-members:
	:noindex:
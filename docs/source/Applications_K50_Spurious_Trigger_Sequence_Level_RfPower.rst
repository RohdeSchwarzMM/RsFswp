RfPower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:LEVel:RFPower

.. code-block:: python

	TRIGger:SEQuence:LEVel:RFPower



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Trigger.Sequence.Level.RfPower.RfPowerCls
	:members:
	:undoc-members:
	:noindex:
FormatPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:FORMat

.. code-block:: python

	SENSe:DDEMod:PATTern:FORMat



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex:
Phase
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMAT REAL,32;SENSe:CORRection:FRESponse:USER:SPECtrum:DATA:PHASe

.. code-block:: python

	FORMAT REAL,32;SENSe:CORRection:FRESponse:USER:SPECtrum:DATA:PHASe



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Spectrum.Data.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex:
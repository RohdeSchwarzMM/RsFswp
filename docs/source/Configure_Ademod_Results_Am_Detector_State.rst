State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:ADEMod:RESults:AM:DETector<Trace>:STATe

.. code-block:: python

	CONFigure:ADEMod:RESults:AM:DETector<Trace>:STATe



.. autoclass:: RsFswp.Implementations.Configure.Ademod.Results.Am.Detector.State.StateCls
	:members:
	:undoc-members:
	:noindex:
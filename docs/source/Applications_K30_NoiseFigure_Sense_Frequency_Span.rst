Span
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:SPAN

.. code-block:: python

	SENSe:FREQuency:SPAN



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Frequency.Span.SpanCls
	:members:
	:undoc-members:
	:noindex:
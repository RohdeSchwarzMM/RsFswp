Dseparator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMat:DEXPort:DSEParator

.. code-block:: python

	FORMat:DEXPort:DSEParator



.. autoclass:: RsFswp.Implementations.FormatPy.Dexport.Dseparator.DseparatorCls
	:members:
	:undoc-members:
	:noindex:
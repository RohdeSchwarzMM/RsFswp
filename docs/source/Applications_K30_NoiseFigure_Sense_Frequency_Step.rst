Step
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:STEP

.. code-block:: python

	SENSe:FREQuency:STEP



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Frequency.Step.StepCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:STATe

.. code-block:: python

	SENSe:CORRection:STATe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:FREQuency:EVENt

.. code-block:: python

	STATus:QUEStionable:FREQuency:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Frequency.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
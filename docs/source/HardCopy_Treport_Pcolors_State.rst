State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:PCOLors:STATe

.. code-block:: python

	HCOPy:TREPort:PCOLors:STATe



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Pcolors.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Right
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:MAXimum:RIGHt

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:MAXimum:RIGHt



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Marker.Maximum.Right.RightCls
	:members:
	:undoc-members:
	:noindex:
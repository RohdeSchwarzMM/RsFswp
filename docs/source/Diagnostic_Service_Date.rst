Date
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:DATE

.. code-block:: python

	DIAGnostic:SERVice:DATE



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.Date.DateCls
	:members:
	:undoc-members:
	:noindex:
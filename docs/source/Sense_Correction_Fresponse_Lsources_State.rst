State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:LSOurces:STATe

.. code-block:: python

	SENSe:CORRection:FRESponse:LSOurces:STATe



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.Lsources.State.StateCls
	:members:
	:undoc-members:
	:noindex:
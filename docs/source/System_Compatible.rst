Compatible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMPatible

.. code-block:: python

	SYSTem:COMPatible



.. autoclass:: RsFswp.Implementations.System.Compatible.CompatibleCls
	:members:
	:undoc-members:
	:noindex:
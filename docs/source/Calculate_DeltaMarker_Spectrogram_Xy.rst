Xy
----------------------------------------





.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Spectrogram.Xy.XyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.deltaMarker.spectrogram.xy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_DeltaMarker_Spectrogram_Xy_Maximum.rst
	Calculate_DeltaMarker_Spectrogram_Xy_Minimum.rst
Cdevice
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:IQHS:CDEVice

.. code-block:: python

	OUTPut:IQHS:CDEVice



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Output.Iqhs.Cdevice.CdeviceCls
	:members:
	:undoc-members:
	:noindex:
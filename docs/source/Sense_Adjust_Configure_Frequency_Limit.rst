Limit
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.Adjust.Configure.Frequency.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.adjust.configure.frequency.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Adjust_Configure_Frequency_Limit_High.rst
	Sense_Adjust_Configure_Frequency_Limit_Low.rst
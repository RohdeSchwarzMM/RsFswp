Active
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ACTive

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ACTive



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Limit.Active.ActiveCls
	:members:
	:undoc-members:
	:noindex:
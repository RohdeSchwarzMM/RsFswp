Unit
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Y.Unit.UnitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.calculate.y.unit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_Calculate_Y_Unit_Time.rst
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:RHO:CURRent:STATe

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:RHO:CURRent:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Rho.Current.State.StateCls
	:members:
	:undoc-members:
	:noindex:
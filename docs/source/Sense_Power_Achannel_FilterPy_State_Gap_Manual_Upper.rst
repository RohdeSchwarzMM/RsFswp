Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:FILTer:STATe:GAP<GapChannel>:MANual:UPPer

.. code-block:: python

	SENSe:POWer:ACHannel:FILTer:STATe:GAP<GapChannel>:MANual:UPPer



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.FilterPy.State.Gap.Manual.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex:
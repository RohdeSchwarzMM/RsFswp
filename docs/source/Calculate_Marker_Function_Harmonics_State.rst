State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:HARMonics:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:HARMonics:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Harmonics.State.StateCls
	:members:
	:undoc-members:
	:noindex:
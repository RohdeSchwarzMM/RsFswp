Window
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:ADD:WINDow

.. code-block:: python

	LAYout:ADD:WINDow



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Layout.Add.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:
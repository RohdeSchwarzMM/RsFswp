Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:DUE:TIME

.. code-block:: python

	CALibration:DUE:TIME



.. autoclass:: RsFswp.Implementations.Calibration.Due.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:CONTrol<Source>:LEVel:STATe

.. code-block:: python

	SOURce:VOLTage:CONTrol<Source>:LEVel:STATe



.. autoclass:: RsFswp.Implementations.Source.Voltage.Control.Level.State.StateCls
	:members:
	:undoc-members:
	:noindex:
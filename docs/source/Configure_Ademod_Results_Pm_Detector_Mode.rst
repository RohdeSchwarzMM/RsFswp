Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:ADEMod:RESults:PM:DETector<Trace>:MODE

.. code-block:: python

	CONFigure:ADEMod:RESults:PM:DETector<Trace>:MODE



.. autoclass:: RsFswp.Implementations.Configure.Ademod.Results.Pm.Detector.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
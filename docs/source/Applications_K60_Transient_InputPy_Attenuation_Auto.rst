Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:ATTenuation:AUTO

.. code-block:: python

	INPut:ATTenuation:AUTO



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.InputPy.Attenuation.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
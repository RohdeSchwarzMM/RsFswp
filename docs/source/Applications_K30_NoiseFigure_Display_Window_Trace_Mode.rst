Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:MODE

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:MODE



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Display.Window.Trace.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
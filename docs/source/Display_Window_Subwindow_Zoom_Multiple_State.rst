State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SUBWindow<SubWindow>:ZOOM:MULTiple<ZoomWindow>:STATe

.. code-block:: python

	DISPlay:WINDow<Window>:SUBWindow<SubWindow>:ZOOM:MULTiple<ZoomWindow>:STATe



.. autoclass:: RsFswp.Implementations.Display.Window.Subwindow.Zoom.Multiple.State.StateCls
	:members:
	:undoc-members:
	:noindex:
ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:OPTion:TRIal:LIST

.. code-block:: python

	SYSTem:OPTion:TRIal:LIST



.. autoclass:: RsFswp.Implementations.System.Option.Trial.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:
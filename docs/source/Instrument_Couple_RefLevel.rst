RefLevel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:RLEVel

.. code-block:: python

	INSTrument:COUPle:RLEVel



.. autoclass:: RsFswp.Implementations.Instrument.Couple.RefLevel.RefLevelCls
	:members:
	:undoc-members:
	:noindex:
Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:SYNC:CONDition

.. code-block:: python

	STATus:QUEStionable:SYNC:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Sync.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
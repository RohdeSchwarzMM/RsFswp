Frequency
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.Adjust.Configure.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.adjust.configure.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Adjust_Configure_Frequency_Limit.rst
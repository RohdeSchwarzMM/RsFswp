Update
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:DISPlay:UPDate

.. code-block:: python

	SYSTem:DISPlay:UPDate



.. autoclass:: RsFswp.Implementations.System.Display.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex:
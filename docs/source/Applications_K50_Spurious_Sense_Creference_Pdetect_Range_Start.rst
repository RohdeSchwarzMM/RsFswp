Start
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:PDETect:RANGe:STARt

.. code-block:: python

	SENSe:CREFerence:PDETect:RANGe:STARt



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Pdetect.Range.Start.StartCls
	:members:
	:undoc-members:
	:noindex:
Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:EVENt

.. code-block:: python

	STATus:QUEStionable:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
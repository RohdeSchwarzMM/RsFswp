Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:INPut<InputIx>:USER:SLISt<TouchStone>:CATalog

.. code-block:: python

	SENSe:CORRection:FRESponse:INPut<InputIx>:USER:SLISt<TouchStone>:CATalog



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.InputPy.User.Slist.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:
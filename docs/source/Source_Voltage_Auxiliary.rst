Auxiliary
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Voltage.Auxiliary.AuxiliaryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.voltage.auxiliary.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Voltage_Auxiliary_Level.rst
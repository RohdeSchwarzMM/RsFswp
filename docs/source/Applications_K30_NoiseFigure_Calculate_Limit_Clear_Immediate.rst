Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:CLEar:IMMediate

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:CLEar:IMMediate



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Limit.Clear.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
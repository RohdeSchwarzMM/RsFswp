RfbWidth
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:SBLock<SubBlock>:RFBWidth

.. code-block:: python

	SENSe:POWer:ACHannel:SBLock<SubBlock>:RFBWidth



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Sblock.RfbWidth.RfbWidthCls
	:members:
	:undoc-members:
	:noindex:
Sequence
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Power.Sequence.SequenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.power.sequence.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Power_Sequence_Result.rst
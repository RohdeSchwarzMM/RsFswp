State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:PAGE:COUNt:STATe

.. code-block:: python

	HCOPy:PAGE:COUNt:STATe



.. autoclass:: RsFswp.Implementations.HardCopy.Page.Count.State.StateCls
	:members:
	:undoc-members:
	:noindex:
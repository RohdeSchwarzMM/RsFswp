State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:TRANsducer:INPut<InputIx>:STATe

.. code-block:: python

	SENSe:CORRection:TRANsducer:INPut<InputIx>:STATe



.. autoclass:: RsFswp.Implementations.Sense.Correction.Transducer.InputPy.State.StateCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:GUARd:STATe

.. code-block:: python

	SENSe:CREFerence:GUARd:STATe



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Guard.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:CONFigure:GENerator:SWITch:AUTO

.. code-block:: python

	SYSTem:CONFigure:GENerator:SWITch:AUTO



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.System.Configure.Generator.Switch.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
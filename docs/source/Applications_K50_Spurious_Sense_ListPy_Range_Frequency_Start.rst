Start
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:FREQuency:STARt

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:FREQuency:STARt



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.ListPy.Range.Frequency.Start.StartCls
	:members:
	:undoc-members:
	:noindex:
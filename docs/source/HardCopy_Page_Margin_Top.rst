Top
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:PAGE:MARGin:TOP

.. code-block:: python

	HCOPy:PAGE:MARGin:TOP



.. autoclass:: RsFswp.Implementations.HardCopy.Page.Margin.Top.TopCls
	:members:
	:undoc-members:
	:noindex:
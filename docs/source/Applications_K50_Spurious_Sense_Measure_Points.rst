Points
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MEASure:POINts

.. code-block:: python

	SENSe:MEASure:POINts



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Measure.Points.PointsCls
	:members:
	:undoc-members:
	:noindex:
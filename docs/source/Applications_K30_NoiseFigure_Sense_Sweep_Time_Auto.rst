Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:TIME:AUTO

.. code-block:: python

	SENSe:SWEep:TIME:AUTO



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Sweep.Time.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
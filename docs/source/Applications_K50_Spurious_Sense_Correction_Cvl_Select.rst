Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:CVL:SELect

.. code-block:: python

	SENSe:CORRection:CVL:SELect



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Correction.Cvl.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
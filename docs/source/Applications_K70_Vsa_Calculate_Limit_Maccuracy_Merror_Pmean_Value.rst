Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:MERRor:PMEan:VALue

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:MERRor:PMEan:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Merror.Pmean.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
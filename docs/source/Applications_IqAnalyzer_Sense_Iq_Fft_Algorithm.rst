Algorithm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:IQ:FFT:ALGorithm

.. code-block:: python

	SENSe:IQ:FFT:ALGorithm



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Sense.Iq.Fft.Algorithm.AlgorithmCls
	:members:
	:undoc-members:
	:noindex:
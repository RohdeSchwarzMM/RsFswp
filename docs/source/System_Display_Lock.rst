Lock
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:DISPlay:LOCK

.. code-block:: python

	SYSTem:DISPlay:LOCK



.. autoclass:: RsFswp.Implementations.System.Display.Lock.LockCls
	:members:
	:undoc-members:
	:noindex:
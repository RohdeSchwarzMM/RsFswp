Left
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:MINimum:LEFT

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:MINimum:LEFT



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Marker.Minimum.Left.LeftCls
	:members:
	:undoc-members:
	:noindex:
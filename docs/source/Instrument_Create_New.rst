New
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:CREate:NEW

.. code-block:: python

	INSTrument:CREate:NEW



.. autoclass:: RsFswp.Implementations.Instrument.Create.New.NewCls
	:members:
	:undoc-members:
	:noindex:
Stop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:PNOise<Trace>:SWEep:STOP

.. code-block:: python

	FETCh:PNOise<Trace>:SWEep:STOP



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Fetch.Pnoise.Sweep.Stop.StopCls
	:members:
	:undoc-members:
	:noindex:
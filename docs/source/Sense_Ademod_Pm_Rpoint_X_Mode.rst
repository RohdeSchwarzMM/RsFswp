Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:PM:RPOint:X:MODE

.. code-block:: python

	SENSe:ADEMod:PM:RPOint:X:MODE



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Pm.Rpoint.X.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
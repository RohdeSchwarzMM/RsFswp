Right
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:MINimum:RIGHt

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:MINimum:RIGHt



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.DeltaMarker.Minimum.Right.RightCls
	:members:
	:undoc-members:
	:noindex:
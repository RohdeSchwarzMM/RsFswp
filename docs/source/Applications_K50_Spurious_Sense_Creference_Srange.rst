Srange
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:SRANge

.. code-block:: python

	SENSe:CREFerence:SRANge



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Srange.SrangeCls
	:members:
	:undoc-members:
	:noindex:
Above
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:SPECtrogram:Y:MAXimum:ABOVe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:SPECtrogram:Y:MAXimum:ABOVe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Spectrogram.Y.Maximum.Above.AboveCls
	:members:
	:undoc-members:
	:noindex:
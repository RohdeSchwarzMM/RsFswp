Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:SPECtrogram:CLEar:IMMediate

.. code-block:: python

	CALCulate<Window>:SPECtrogram:CLEar:IMMediate



.. autoclass:: RsFswp.Implementations.Calculate.Spectrogram.Clear.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:IQ:TYPE

.. code-block:: python

	INPut:IQ:TYPE



.. autoclass:: RsFswp.Implementations.InputPy.Iq.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
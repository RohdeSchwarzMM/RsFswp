Nof
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:EGATe:NOF

.. code-block:: python

	TRACe:IQ:EGATe:NOF



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.Egate.Nof.NofCls
	:members:
	:undoc-members:
	:noindex:
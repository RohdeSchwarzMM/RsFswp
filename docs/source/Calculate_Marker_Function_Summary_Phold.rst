Phold
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:PHOLd

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:PHOLd



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Summary.Phold.PholdCls
	:members:
	:undoc-members:
	:noindex:
Xdistrib
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMat:DEXPort:XDIStrib

.. code-block:: python

	FORMat:DEXPort:XDIStrib



.. autoclass:: RsFswp.Implementations.FormatPy.Dexport.Xdistrib.XdistribCls
	:members:
	:undoc-members:
	:noindex:
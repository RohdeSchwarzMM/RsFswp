Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:CORRection:EVENt

.. code-block:: python

	STATus:QUEStionable:CORRection:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Correction.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
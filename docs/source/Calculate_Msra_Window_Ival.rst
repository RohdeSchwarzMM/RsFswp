Ival
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MSRA:WINDow:IVAL

.. code-block:: python

	CALCulate<Window>:MSRA:WINDow:IVAL



.. autoclass:: RsFswp.Implementations.Calculate.Msra.Window.Ival.IvalCls
	:members:
	:undoc-members:
	:noindex:
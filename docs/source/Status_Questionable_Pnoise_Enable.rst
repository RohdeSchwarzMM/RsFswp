Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:PNOise:ENABle

.. code-block:: python

	STATus:QUEStionable:PNOise:ENABle



.. autoclass:: RsFswp.Implementations.Status.Questionable.Pnoise.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
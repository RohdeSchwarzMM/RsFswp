Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:GENerator:FREQuency

.. code-block:: python

	SOURce:GENerator:FREQuency



.. autoclass:: RsFswp.Implementations.Source.Generator.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.generator.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Generator_Frequency_Step.rst
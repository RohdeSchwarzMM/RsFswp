Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:TLRel:VALue

.. code-block:: python

	CALCulate<Window>:TLRel:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.TlRel.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
Pdivision
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:Y:SCALe:PDIVision

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:Y:SCALe:PDIVision



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Display.Window.Trace.Y.Scale.Pdivision.PdivisionCls
	:members:
	:undoc-members:
	:noindex:
Offset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MSRA:CAPTure:OFFSet

.. code-block:: python

	SENSe:MSRA:CAPTure:OFFSet



.. autoclass:: RsFswp.Implementations.Sense.Msra.Capture.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:
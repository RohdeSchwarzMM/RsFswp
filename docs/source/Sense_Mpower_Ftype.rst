Ftype
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MPOWer:FTYPe

.. code-block:: python

	SENSe:MPOWer:FTYPe



.. autoclass:: RsFswp.Implementations.Sense.Mpower.Ftype.FtypeCls
	:members:
	:undoc-members:
	:noindex:
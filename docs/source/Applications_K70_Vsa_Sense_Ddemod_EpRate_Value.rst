Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:EPRate:VALue

.. code-block:: python

	SENSe:DDEMod:EPRate:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.EpRate.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
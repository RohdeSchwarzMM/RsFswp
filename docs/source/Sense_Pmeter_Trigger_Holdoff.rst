Holdoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:TRIGger:HOLDoff

.. code-block:: python

	SENSe:PMETer<PowerMeter>:TRIGger:HOLDoff



.. autoclass:: RsFswp.Implementations.Sense.Pmeter.Trigger.Holdoff.HoldoffCls
	:members:
	:undoc-members:
	:noindex:
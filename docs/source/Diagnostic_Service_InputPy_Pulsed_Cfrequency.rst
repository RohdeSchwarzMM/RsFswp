Cfrequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:INPut:PULSed:CFRequency

.. code-block:: python

	DIAGnostic:SERVice:INPut:PULSed:CFRequency



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.InputPy.Pulsed.Cfrequency.CfrequencyCls
	:members:
	:undoc-members:
	:noindex:
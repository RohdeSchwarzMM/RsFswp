DeleteDirectory
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:RDIRectory

.. code-block:: python

	MMEMory:RDIRectory



.. autoclass:: RsFswp.Implementations.MassMemory.DeleteDirectory.DeleteDirectoryCls
	:members:
	:undoc-members:
	:noindex:
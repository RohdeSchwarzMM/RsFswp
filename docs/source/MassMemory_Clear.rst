Clear
----------------------------------------





.. autoclass:: RsFswp.Implementations.MassMemory.Clear.ClearCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.massMemory.clear.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MassMemory_Clear_State.rst
Gain
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:CONFigure:DUT:GAIN

.. code-block:: python

	SYSTem:CONFigure:DUT:GAIN



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.System.Configure.Dut.Gain.GainCls
	:members:
	:undoc-members:
	:noindex:
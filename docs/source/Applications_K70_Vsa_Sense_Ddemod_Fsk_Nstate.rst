Nstate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:FSK:NSTate

.. code-block:: python

	SENSe:DDEMod:FSK:NSTate



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Fsk.Nstate.NstateCls
	:members:
	:undoc-members:
	:noindex:
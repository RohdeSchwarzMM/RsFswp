Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:NORMalize:CHANnel

.. code-block:: python

	SENSe:DDEMod:NORMalize:CHANnel



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Normalize.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex:
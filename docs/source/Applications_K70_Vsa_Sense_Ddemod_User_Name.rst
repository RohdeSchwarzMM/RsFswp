Name
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:USER:NAME

.. code-block:: python

	SENSe:DDEMod:USER:NAME



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.User.Name.NameCls
	:members:
	:undoc-members:
	:noindex:
Shutdown
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:SHUTdown

.. code-block:: python

	SYSTem:SHUTdown



.. autoclass:: RsFswp.Implementations.System.Shutdown.ShutdownCls
	:members:
	:undoc-members:
	:noindex:
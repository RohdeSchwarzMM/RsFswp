Stop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:LIMit<LimitIx>:ABSolute:STOP

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:LIMit<LimitIx>:ABSolute:STOP



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Range.Limit.Absolute.Stop.StopCls
	:members:
	:undoc-members:
	:noindex:
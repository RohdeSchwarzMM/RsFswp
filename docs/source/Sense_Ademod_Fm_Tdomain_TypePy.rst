TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:FM:TDOMain:TYPE

.. code-block:: python

	SENSe:ADEMod:FM:TDOMain:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Fm.Tdomain.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
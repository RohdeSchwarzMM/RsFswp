TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BWIDth:VIDeo:TYPE

.. code-block:: python

	SENSe:BWIDth:VIDeo:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Bandwidth.Video.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:AM:ABSolute:AFSPectrum:TYPE

.. code-block:: python

	SENSe:ADEMod:AM:ABSolute:AFSPectrum:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Am.Absolute.AfSpectrum.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
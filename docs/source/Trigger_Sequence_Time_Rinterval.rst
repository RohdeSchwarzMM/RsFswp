Rinterval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:TIME:RINTerval

.. code-block:: python

	TRIGger:SEQuence:TIME:RINTerval



.. autoclass:: RsFswp.Implementations.Trigger.Sequence.Time.Rinterval.RintervalCls
	:members:
	:undoc-members:
	:noindex:
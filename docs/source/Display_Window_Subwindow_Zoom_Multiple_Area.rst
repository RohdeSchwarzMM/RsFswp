Area
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SUBWindow<SubWindow>:ZOOM:MULTiple<ZoomWindow>:AREA

.. code-block:: python

	DISPlay:WINDow<Window>:SUBWindow<SubWindow>:ZOOM:MULTiple<ZoomWindow>:AREA



.. autoclass:: RsFswp.Implementations.Display.Window.Subwindow.Zoom.Multiple.Area.AreaCls
	:members:
	:undoc-members:
	:noindex:
Resolution
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:IQ:BWIDth:RESolution

.. code-block:: python

	SENSe:IQ:BWIDth:RESolution



.. autoclass:: RsFswp.Implementations.Sense.Iq.Bandwidth.Resolution.ResolutionCls
	:members:
	:undoc-members:
	:noindex:
Fstate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:FSTate

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:FSTate



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Fstate.FstateCls
	:members:
	:undoc-members:
	:noindex:
Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:PNOise<Trace>:MEASured:LEVel

.. code-block:: python

	FETCh:PNOise<Trace>:MEASured:LEVel



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Fetch.Pnoise.Measured.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:
Set
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:POWer:SET

.. code-block:: python

	SENSe:LIST:POWer:SET



.. autoclass:: RsFswp.Implementations.Sense.ListPy.Power.Set.SetCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ACPower:ALTernate<Channel>:ABSolute:STATe

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ACPower:ALTernate<Channel>:ABSolute:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Limit.AcPower.Alternate.Absolute.State.StateCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:SPECtrogram:TSTamp:STATe

.. code-block:: python

	CALCulate<Window>:SPECtrogram:TSTamp:STATe



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.Spectrogram.Tstamp.State.StateCls
	:members:
	:undoc-members:
	:noindex:
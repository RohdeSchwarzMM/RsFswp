Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SPECtrogram:COLor:UPPer

.. code-block:: python

	DISPlay:WINDow<Window>:SPECtrogram:COLor:UPPer



.. autoclass:: RsFswp.Implementations.Display.Window.Spectrogram.Color.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex:
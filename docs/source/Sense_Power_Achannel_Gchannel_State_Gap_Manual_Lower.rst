Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:GCHannel:STATe:GAP<GapChannel>:MANual:LOWer

.. code-block:: python

	SENSe:POWer:ACHannel:GCHannel:STATe:GAP<GapChannel>:MANual:LOWer



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Gchannel.State.Gap.Manual.Lower.LowerCls
	:members:
	:undoc-members:
	:noindex:
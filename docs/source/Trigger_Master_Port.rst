Port
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:MASTer:PORT

.. code-block:: python

	TRIGger:MASTer:PORT



.. autoclass:: RsFswp.Implementations.Trigger.Master.Port.PortCls
	:members:
	:undoc-members:
	:noindex:
Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:MODulation<Window>:CFRequency:CONDition

.. code-block:: python

	STATus:QUEStionable:MODulation<Window>:CFRequency:CONDition



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Status.Questionable.Modulation.Cfrequency.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
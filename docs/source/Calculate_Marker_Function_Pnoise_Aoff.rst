Aoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:PNOise:AOFF

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:PNOise:AOFF



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Pnoise.Aoff.AoffCls
	:members:
	:undoc-members:
	:noindex:
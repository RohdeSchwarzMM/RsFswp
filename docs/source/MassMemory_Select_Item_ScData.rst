ScData
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:SELect:ITEM:SCData

.. code-block:: python

	MMEMory:SELect:ITEM:SCData



.. autoclass:: RsFswp.Implementations.MassMemory.Select.Item.ScData.ScDataCls
	:members:
	:undoc-members:
	:noindex:
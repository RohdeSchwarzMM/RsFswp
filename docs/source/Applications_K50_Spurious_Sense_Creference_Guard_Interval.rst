Interval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:GUARd:INTerval

.. code-block:: python

	SENSe:CREFerence:GUARd:INTerval



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Guard.Interval.IntervalCls
	:members:
	:undoc-members:
	:noindex:
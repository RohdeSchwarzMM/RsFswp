Bios
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:BIOS

.. code-block:: python

	DIAGnostic:HUMS:BIOS



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.Bios.BiosCls
	:members:
	:undoc-members:
	:noindex:
Single
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CONFigure:FREQuency:SINGle

.. code-block:: python

	SENSe:CONFigure:FREQuency:SINGle



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Configure.Frequency.Single.SingleCls
	:members:
	:undoc-members:
	:noindex:
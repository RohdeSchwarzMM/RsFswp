Angle
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNIT:ANGLe

.. code-block:: python

	CALCulate<Window>:UNIT:ANGLe



.. autoclass:: RsFswp.Implementations.Calculate.Unit.Angle.AngleCls
	:members:
	:undoc-members:
	:noindex:
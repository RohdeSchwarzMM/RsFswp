Duration
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:CONFigure:DURation

.. code-block:: python

	SENSe:ADJust:CONFigure:DURation



.. autoclass:: RsFswp.Implementations.Sense.Adjust.Configure.Duration.DurationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.adjust.configure.duration.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Adjust_Configure_Duration_Mode.rst
Thd
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: UNIT:THD

.. code-block:: python

	UNIT:THD



.. autoclass:: RsFswp.Implementations.Unit.Thd.ThdCls
	:members:
	:undoc-members:
	:noindex:
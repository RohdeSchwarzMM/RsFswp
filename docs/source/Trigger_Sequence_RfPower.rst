RfPower
----------------------------------------





.. autoclass:: RsFswp.Implementations.Trigger.Sequence.RfPower.RfPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.sequence.rfPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Sequence_RfPower_Holdoff.rst
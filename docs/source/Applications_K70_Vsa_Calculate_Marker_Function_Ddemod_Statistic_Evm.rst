Evm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:EVM

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:EVM



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Marker.Function.Ddemod.Statistic.Evm.EvmCls
	:members:
	:undoc-members:
	:noindex:
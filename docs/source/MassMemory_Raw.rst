Raw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:RAW

.. code-block:: python

	MMEMory:RAW



.. autoclass:: RsFswp.Implementations.MassMemory.Raw.RawCls
	:members:
	:undoc-members:
	:noindex:
Span
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:PDETect:RANGe:SPAN

.. code-block:: python

	SENSe:CREFerence:PDETect:RANGe:SPAN



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Pdetect.Range.Span.SpanCls
	:members:
	:undoc-members:
	:noindex:
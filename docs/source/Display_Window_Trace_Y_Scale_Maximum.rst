Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:Y:SCALe:MAXimum

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:Y:SCALe:MAXimum



.. autoclass:: RsFswp.Implementations.Display.Window.Trace.Y.Scale.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:
Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ACPower:ACHannel:RESult:RELative

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ACPower:ACHannel:RESult:RELative



.. autoclass:: RsFswp.Implementations.Calculate.Limit.AcPower.Achannel.Result.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex:
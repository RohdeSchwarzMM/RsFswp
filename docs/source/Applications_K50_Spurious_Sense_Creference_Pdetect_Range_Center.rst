Center
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:PDETect:RANGe:CENTer

.. code-block:: python

	SENSe:CREFerence:PDETect:RANGe:CENTer



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Pdetect.Range.Center.CenterCls
	:members:
	:undoc-members:
	:noindex:
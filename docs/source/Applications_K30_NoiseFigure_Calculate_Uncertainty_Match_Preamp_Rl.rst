Rl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:MATCh:PREamp:RL

.. code-block:: python

	CALCulate<Window>:UNCertainty:MATCh:PREamp:RL



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Match.Preamp.Rl.RlCls
	:members:
	:undoc-members:
	:noindex:
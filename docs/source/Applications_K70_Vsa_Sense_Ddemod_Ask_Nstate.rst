Nstate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:ASK:NSTate

.. code-block:: python

	SENSe:DDEMod:ASK:NSTate



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Ask.Nstate.NstateCls
	:members:
	:undoc-members:
	:noindex:
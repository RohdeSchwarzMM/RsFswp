State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EXTernal<ExternalGen>:FREQuency:COUPling:STATe

.. code-block:: python

	SOURce:EXTernal<ExternalGen>:FREQuency:COUPling:STATe



.. autoclass:: RsFswp.Implementations.Source.External.Frequency.Coupling.State.StateCls
	:members:
	:undoc-members:
	:noindex:
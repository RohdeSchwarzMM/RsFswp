Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:MEAN:RESult

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:MEAN:RESult



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Summary.Mean.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
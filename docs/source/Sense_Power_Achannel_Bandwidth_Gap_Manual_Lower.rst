Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:BWIDth:GAP<GapChannel>:MANual:LOWer

.. code-block:: python

	SENSe:POWer:ACHannel:BWIDth:GAP<GapChannel>:MANual:LOWer



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Bandwidth.Gap.Manual.Lower.LowerCls
	:members:
	:undoc-members:
	:noindex:
Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:POWer:DCPNoise:NTRansition

.. code-block:: python

	STATus:QUEStionable:POWer:DCPNoise:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Power.DcpNoise.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
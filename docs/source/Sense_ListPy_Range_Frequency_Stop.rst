Stop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:FREQuency:STOP

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:FREQuency:STOP



.. autoclass:: RsFswp.Implementations.Sense.ListPy.Range.Frequency.Stop.StopCls
	:members:
	:undoc-members:
	:noindex:
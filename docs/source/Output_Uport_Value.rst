Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:UPORt:VALue

.. code-block:: python

	OUTPut<OutputConnector>:UPORt:VALue



.. autoclass:: RsFswp.Implementations.Output.Uport.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
Transfer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FPLan:TRANsfer

.. code-block:: python

	SENSe:FPLan:TRANsfer



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Fplan.Transfer.TransferCls
	:members:
	:undoc-members:
	:noindex:
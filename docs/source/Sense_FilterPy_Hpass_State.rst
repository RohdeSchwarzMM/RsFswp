State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FILTer<FilterPy>:HPASs:STATe

.. code-block:: python

	SENSe:FILTer<FilterPy>:HPASs:STATe



.. autoclass:: RsFswp.Implementations.Sense.FilterPy.Hpass.State.StateCls
	:members:
	:undoc-members:
	:noindex:
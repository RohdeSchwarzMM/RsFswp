Theme
----------------------------------------





.. autoclass:: RsFswp.Implementations.Display.Theme.ThemeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.theme.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_Theme_Catalog.rst
	Display_Theme_Select.rst
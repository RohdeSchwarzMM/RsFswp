State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:INPut<InputIx>:USER:FLISt<FileList>:PHASe:STATe

.. code-block:: python

	SENSe:CORRection:FRESponse:INPut<InputIx>:USER:FLISt<FileList>:PHASe:STATe



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.InputPy.User.Flist.Phase.State.StateCls
	:members:
	:undoc-members:
	:noindex:
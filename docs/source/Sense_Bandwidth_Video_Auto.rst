Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BWIDth:VIDeo:AUTO

.. code-block:: python

	SENSe:BWIDth:VIDeo:AUTO



.. autoclass:: RsFswp.Implementations.Sense.Bandwidth.Video.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
VersionInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:VERSinfo

.. code-block:: python

	DIAGnostic:SERVice:VERSinfo



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.VersionInfo.VersionInfoCls
	:members:
	:undoc-members:
	:noindex:
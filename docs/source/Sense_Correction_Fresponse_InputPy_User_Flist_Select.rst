Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:INPut<InputIx>:USER:FLISt<FileList>:SELect

.. code-block:: python

	SENSe:CORRection:FRESponse:INPut<InputIx>:USER:FLISt<FileList>:SELect



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.InputPy.User.Flist.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
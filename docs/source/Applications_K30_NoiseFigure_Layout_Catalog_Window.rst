Window
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:CATalog:WINDow

.. code-block:: python

	LAYout:CATalog:WINDow



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Layout.Catalog.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:
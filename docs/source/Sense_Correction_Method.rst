Method
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:METHod

.. code-block:: python

	SENSe:CORRection:METHod



.. autoclass:: RsFswp.Implementations.Sense.Correction.Method.MethodCls
	:members:
	:undoc-members:
	:noindex:
Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:MSR:GSM:CARRier

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:MSR:GSM:CARRier



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Msr.Gsm.Carrier.CarrierCls
	:members:
	:undoc-members:
	:noindex:
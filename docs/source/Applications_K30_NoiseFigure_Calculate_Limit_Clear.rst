Clear
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Limit.Clear.ClearCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k30NoiseFigure.calculate.limit.clear.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K30_NoiseFigure_Calculate_Limit_Clear_Immediate.rst
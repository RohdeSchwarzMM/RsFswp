Pulse
----------------------------------------





.. autoclass:: RsFswp.Implementations.Output.Trigger.Pulse.PulseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.output.trigger.pulse.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Output_Trigger_Pulse_Immediate.rst
	Output_Trigger_Pulse_Length.rst
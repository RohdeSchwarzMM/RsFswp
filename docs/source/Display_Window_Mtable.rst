Mtable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:MTABle

.. code-block:: python

	DISPlay:WINDow<Window>:MTABle



.. autoclass:: RsFswp.Implementations.Display.Window.Mtable.MtableCls
	:members:
	:undoc-members:
	:noindex:
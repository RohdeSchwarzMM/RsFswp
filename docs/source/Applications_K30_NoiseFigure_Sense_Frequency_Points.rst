Points
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:POINts

.. code-block:: python

	SENSe:FREQuency:POINts



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Frequency.Points.PointsCls
	:members:
	:undoc-members:
	:noindex:
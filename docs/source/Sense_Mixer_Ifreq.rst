Ifreq
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:IF

.. code-block:: python

	SENSe:MIXer:IF



.. autoclass:: RsFswp.Implementations.Sense.Mixer.Ifreq.IfreqCls
	:members:
	:undoc-members:
	:noindex:
Atab
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:ATAB

.. code-block:: python

	DISPlay:ATAB



.. autoclass:: RsFswp.Implementations.Display.Atab.AtabCls
	:members:
	:undoc-members:
	:noindex:
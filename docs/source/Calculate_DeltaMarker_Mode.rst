Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:MODE

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:MODE



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
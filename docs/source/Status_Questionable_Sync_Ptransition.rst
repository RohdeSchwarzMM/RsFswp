Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:SYNC:PTRansition

.. code-block:: python

	STATus:QUEStionable:SYNC:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Sync.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
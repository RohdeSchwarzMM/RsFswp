Trigger
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Generator.Pulse.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.generator.pulse.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Generator_Pulse_Trigger_Output.rst
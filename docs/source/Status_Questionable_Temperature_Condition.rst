Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TEMPerature:CONDition

.. code-block:: python

	STATus:QUEStionable:TEMPerature:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Temperature.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
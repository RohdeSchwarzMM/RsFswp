Gap
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:EGATe:GAP

.. code-block:: python

	TRACe:IQ:EGATe:GAP



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.Egate.Gap.GapCls
	:members:
	:undoc-members:
	:noindex:
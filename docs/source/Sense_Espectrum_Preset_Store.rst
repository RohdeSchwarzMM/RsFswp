Store
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:PRESet:STORe

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:PRESet:STORe



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Preset.Store.StoreCls
	:members:
	:undoc-members:
	:noindex:
Stop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:FREQuency:STOP

.. code-block:: python

	SENSe:MIXer:FREQuency:STOP



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Mixer.Frequency.Stop.StopCls
	:members:
	:undoc-members:
	:noindex:
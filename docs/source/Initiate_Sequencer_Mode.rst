Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:SEQuencer:MODE

.. code-block:: python

	INITiate:SEQuencer:MODE



.. autoclass:: RsFswp.Implementations.Initiate.Sequencer.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
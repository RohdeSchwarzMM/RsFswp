Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:PERRor:PPEak:VALue

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:PERRor:PPEak:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Perror.Ppeak.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
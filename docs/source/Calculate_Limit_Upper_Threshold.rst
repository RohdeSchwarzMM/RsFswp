Threshold
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:UPPer:THReshold

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:UPPer:THReshold



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Upper.Threshold.ThresholdCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:TRIGger:STATe

.. code-block:: python

	SENSe:PMETer<PowerMeter>:TRIGger:STATe



.. autoclass:: RsFswp.Implementations.Sense.Pmeter.Trigger.State.StateCls
	:members:
	:undoc-members:
	:noindex:
SymbolRate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SRATe

.. code-block:: python

	SENSe:DDEMod:SRATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex:
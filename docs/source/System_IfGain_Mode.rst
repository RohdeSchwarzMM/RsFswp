Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:IFGain:MODE

.. code-block:: python

	SYSTem:IFGain:MODE



.. autoclass:: RsFswp.Implementations.System.IfGain.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
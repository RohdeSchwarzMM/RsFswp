Resolution
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:COUNt:RESolution

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:COUNt:RESolution



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Count.Resolution.ResolutionCls
	:members:
	:undoc-members:
	:noindex:
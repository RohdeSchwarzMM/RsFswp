Text
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SEARch:SYNC:TEXT

.. code-block:: python

	SENSe:DDEMod:SEARch:SYNC:TEXT



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Search.Sync.Text.TextCls
	:members:
	:undoc-members:
	:noindex:
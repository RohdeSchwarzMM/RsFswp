Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:SWEep:TIME:AUTO

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:SWEep:TIME:AUTO



.. autoclass:: RsFswp.Implementations.Sense.ListPy.Range.Sweep.Time.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
Stop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:AF:STOP

.. code-block:: python

	SENSe:ADEMod:AF:STOP



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Af.Stop.StopCls
	:members:
	:undoc-members:
	:noindex:
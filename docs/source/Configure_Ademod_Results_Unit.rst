Unit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:ADEMod:RESults:UNIT

.. code-block:: python

	CONFigure:ADEMod:RESults:UNIT



.. autoclass:: RsFswp.Implementations.Configure.Ademod.Results.Unit.UnitCls
	:members:
	:undoc-members:
	:noindex:
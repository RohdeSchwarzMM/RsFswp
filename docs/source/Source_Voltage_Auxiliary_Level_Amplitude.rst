Amplitude
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:AUX:LEVel:AMPLitude

.. code-block:: python

	SOURce:VOLTage:AUX:LEVel:AMPLitude



.. autoclass:: RsFswp.Implementations.Source.Voltage.Auxiliary.Level.Amplitude.AmplitudeCls
	:members:
	:undoc-members:
	:noindex:
High
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:CONTrol<Source>:LIMit:HIGH

.. code-block:: python

	SOURce:CURRent:CONTrol<Source>:LIMit:HIGH



.. autoclass:: RsFswp.Implementations.Source.Current.Control.Limit.High.HighCls
	:members:
	:undoc-members:
	:noindex:
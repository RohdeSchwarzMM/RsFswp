Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:NDBDown:RESult

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:NDBDown:RESult



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.NdbDown.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:MTIMe:AVERage:STATe

.. code-block:: python

	SENSe:PMETer<PowerMeter>:MTIMe:AVERage:STATe



.. autoclass:: RsFswp.Implementations.Sense.Pmeter.Mtime.Average.State.StateCls
	:members:
	:undoc-members:
	:noindex:
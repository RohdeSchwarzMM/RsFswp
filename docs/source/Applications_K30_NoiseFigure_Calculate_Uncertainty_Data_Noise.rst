Noise
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:DATA:NOISe

.. code-block:: python

	CALCulate<Window>:UNCertainty:DATA:NOISe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Data.Noise.NoiseCls
	:members:
	:undoc-members:
	:noindex:
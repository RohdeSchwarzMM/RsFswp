RefLevel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:RLEVel

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:RLEVel



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Range.RefLevel.RefLevelCls
	:members:
	:undoc-members:
	:noindex:
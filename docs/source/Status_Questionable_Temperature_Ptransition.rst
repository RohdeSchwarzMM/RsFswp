Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TEMPerature:PTRansition

.. code-block:: python

	STATus:QUEStionable:TEMPerature:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Temperature.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
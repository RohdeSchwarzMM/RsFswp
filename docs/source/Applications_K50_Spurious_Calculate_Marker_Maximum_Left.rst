Left
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:MAXimum:LEFT

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:MAXimum:LEFT



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Marker.Maximum.Left.LeftCls
	:members:
	:undoc-members:
	:noindex:
Start
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:FREQuency:STARt

.. code-block:: python

	SENSe:MIXer:FREQuency:STARt



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Mixer.Frequency.Start.StartCls
	:members:
	:undoc-members:
	:noindex:
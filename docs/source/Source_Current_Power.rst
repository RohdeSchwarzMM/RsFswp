Power<Source>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.source.current.power.repcap_source_get()
	driver.source.current.power.repcap_source_set(repcap.Source.Nr1)





.. autoclass:: RsFswp.Implementations.Source.Current.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.current.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Current_Power_Limit.rst
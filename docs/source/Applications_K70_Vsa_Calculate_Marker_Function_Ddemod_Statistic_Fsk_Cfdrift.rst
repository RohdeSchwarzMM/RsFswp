Cfdrift
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:FSK:CFDRift

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:FSK:CFDRift



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Marker.Function.Ddemod.Statistic.Fsk.Cfdrift.CfdriftCls
	:members:
	:undoc-members:
	:noindex:
Link
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:RDEVice:GENerator:LINK

.. code-block:: python

	SYSTem:COMMunicate:RDEVice:GENerator:LINK



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.System.Communicate.Rdevice.Generator.Link.LinkCls
	:members:
	:undoc-members:
	:noindex:
Offset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:UPPer:OFFSet

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:UPPer:OFFSet



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Upper.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:
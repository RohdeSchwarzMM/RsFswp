State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:STATe

.. code-block:: python

	TRACe:IQ:STATe



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.State.StateCls
	:members:
	:undoc-members:
	:noindex:
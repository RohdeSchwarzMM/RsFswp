Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:LOSS:OUTPut:TABLe:SELect

.. code-block:: python

	SENSe:CORRection:LOSS:OUTPut:TABLe:SELect



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Loss.Output.Table.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
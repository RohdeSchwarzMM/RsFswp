Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:DIQ:CONDition

.. code-block:: python

	STATus:QUEStionable:DIQ:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Diq.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
Rrange
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:RRANge

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:RRANge



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Rrange.RrangeCls
	:members:
	:undoc-members:
	:noindex:
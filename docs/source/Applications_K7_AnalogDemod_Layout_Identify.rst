Identify
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K7_AnalogDemod.Layout.Identify.IdentifyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k7AnalogDemod.layout.identify.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K7_AnalogDemod_Layout_Identify_Window.rst
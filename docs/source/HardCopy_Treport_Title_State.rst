State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:TITLe:STATe

.. code-block:: python

	HCOPy:TREPort:TITLe:STATe



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Title.State.StateCls
	:members:
	:undoc-members:
	:noindex:
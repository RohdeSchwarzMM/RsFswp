IfPower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:LEVel:IFPower

.. code-block:: python

	TRIGger:SEQuence:LEVel:IFPower



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Trigger.Sequence.Level.IfPower.IfPowerCls
	:members:
	:undoc-members:
	:noindex:
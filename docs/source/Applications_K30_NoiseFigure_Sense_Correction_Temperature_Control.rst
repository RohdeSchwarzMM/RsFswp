Control
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:TEMPerature:CONTrol

.. code-block:: python

	SENSe:CORRection:TEMPerature:CONTrol



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Temperature.Control.ControlCls
	:members:
	:undoc-members:
	:noindex:
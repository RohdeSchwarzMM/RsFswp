Annotation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:ANNotation

.. code-block:: python

	SENSe:FREQuency:ANNotation



.. autoclass:: RsFswp.Implementations.Sense.Frequency.Annotation.AnnotationCls
	:members:
	:undoc-members:
	:noindex:
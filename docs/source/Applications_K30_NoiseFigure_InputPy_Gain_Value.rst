Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:GAIN:VALue

.. code-block:: python

	INPut<InputIx>:GAIN:VALue



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.InputPy.Gain.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
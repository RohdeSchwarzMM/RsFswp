State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:PERRor:RPEak:STATe

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:PERRor:RPEak:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Perror.Rpeak.State.StateCls
	:members:
	:undoc-members:
	:noindex:
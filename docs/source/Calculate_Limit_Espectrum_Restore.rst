Restore
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ESPectrum<SubBlock>:RESTore

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ESPectrum<SubBlock>:RESTore



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Espectrum.Restore.RestoreCls
	:members:
	:undoc-members:
	:noindex:
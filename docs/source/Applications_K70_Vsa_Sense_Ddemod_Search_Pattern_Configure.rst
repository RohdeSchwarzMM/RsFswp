Configure
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Search.Pattern.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.sense.ddemod.search.pattern.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_Sense_Ddemod_Search_Pattern_Configure_Auto.rst
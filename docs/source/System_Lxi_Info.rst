Info
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:LXI:INFO

.. code-block:: python

	SYSTem:LXI:INFO



.. autoclass:: RsFswp.Implementations.System.Lxi.Info.InfoCls
	:members:
	:undoc-members:
	:noindex:
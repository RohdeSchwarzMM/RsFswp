FormatPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:FORMat

.. code-block:: python

	DISPlay:FORMat



.. autoclass:: RsFswp.Implementations.Display.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex:
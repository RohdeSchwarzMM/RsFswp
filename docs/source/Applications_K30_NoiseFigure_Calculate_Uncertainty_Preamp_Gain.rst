Gain
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:PREamp:GAIN

.. code-block:: python

	CALCulate<Window>:UNCertainty:PREamp:GAIN



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Preamp.Gain.GainCls
	:members:
	:undoc-members:
	:noindex:
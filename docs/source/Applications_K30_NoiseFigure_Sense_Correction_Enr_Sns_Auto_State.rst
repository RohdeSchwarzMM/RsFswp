State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:ENR:SNS:AUTO:STATe

.. code-block:: python

	SENSe:CORRection:ENR:SNS:AUTO:STATe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Enr.Sns.Auto.State.StateCls
	:members:
	:undoc-members:
	:noindex:
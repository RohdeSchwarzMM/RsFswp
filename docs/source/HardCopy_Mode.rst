Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:MODE

.. code-block:: python

	HCOPy:MODE



.. autoclass:: RsFswp.Implementations.HardCopy.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:RTMS:ALINe:VALue

.. code-block:: python

	CALCulate<Window>:RTMS:ALINe:VALue



.. autoclass:: RsFswp.Implementations.Calculate.Rtms.Aline.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
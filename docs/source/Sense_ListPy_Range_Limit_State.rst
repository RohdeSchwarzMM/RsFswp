State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:LIMit:STATe

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:LIMit:STATe



.. autoclass:: RsFswp.Implementations.Sense.ListPy.Range.Limit.State.StateCls
	:members:
	:undoc-members:
	:noindex:
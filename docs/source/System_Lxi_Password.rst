Password
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:LXI:PASSword

.. code-block:: python

	SYSTem:LXI:PASSword



.. autoclass:: RsFswp.Implementations.System.Lxi.Password.PasswordCls
	:members:
	:undoc-members:
	:noindex:
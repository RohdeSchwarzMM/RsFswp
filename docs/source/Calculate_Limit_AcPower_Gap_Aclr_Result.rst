Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ACPower:GAP<GapChannel>:ACLR:RESult

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ACPower:GAP<GapChannel>:ACLR:RESult



.. autoclass:: RsFswp.Implementations.Calculate.Limit.AcPower.Gap.Aclr.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
Common
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:COMMon

.. code-block:: python

	CALCulate<Window>:UNCertainty:COMMon



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Common.CommonCls
	:members:
	:undoc-members:
	:noindex:
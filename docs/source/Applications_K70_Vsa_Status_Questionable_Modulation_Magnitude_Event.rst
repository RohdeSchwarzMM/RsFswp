Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:MODulation<Window>:MAGNitude:EVENt

.. code-block:: python

	STATus:QUEStionable:MODulation<Window>:MAGNitude:EVENt



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Status.Questionable.Modulation.Magnitude.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
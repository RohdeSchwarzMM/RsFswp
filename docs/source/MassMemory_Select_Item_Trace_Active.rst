Active
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:SELect:ITEM:TRACe:ACTive

.. code-block:: python

	MMEMory:SELect:ITEM:TRACe:ACTive



.. autoclass:: RsFswp.Implementations.MassMemory.Select.Item.Trace.Active.ActiveCls
	:members:
	:undoc-members:
	:noindex:
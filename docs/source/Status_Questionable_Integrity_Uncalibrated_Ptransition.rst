Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INTegrity:UNCalibrated:PTRansition

.. code-block:: python

	STATus:QUEStionable:INTegrity:UNCalibrated:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Integrity.Uncalibrated.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
Execute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PLUGin:APPStarter:EXECute

.. code-block:: python

	SYSTem:PLUGin:APPStarter:EXECute



.. autoclass:: RsFswp.Implementations.System.Plugin.AppStarter.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex:
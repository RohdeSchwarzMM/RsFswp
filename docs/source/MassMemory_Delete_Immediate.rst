Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:DELete:IMMediate

.. code-block:: python

	MMEMory:DELete:IMMediate



.. autoclass:: RsFswp.Implementations.MassMemory.Delete.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
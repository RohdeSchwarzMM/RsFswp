Default
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:ITEM:DEFault

.. code-block:: python

	HCOPy:TREPort:ITEM:DEFault



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Item.Default.DefaultCls
	:members:
	:undoc-members:
	:noindex:
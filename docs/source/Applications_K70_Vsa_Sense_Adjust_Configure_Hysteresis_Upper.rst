Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:CONFigure:HYSTeresis:UPPer

.. code-block:: python

	SENSe:ADJust:CONFigure:HYSTeresis:UPPer



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Adjust.Configure.Hysteresis.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex:
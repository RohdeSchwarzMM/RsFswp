Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:DATA:FREQuency<SPortPair>

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:DATA:FREQuency<SPortPair>



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Slist.Data.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:
Period
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:GENerator:PULSe:PERiod

.. code-block:: python

	SOURce:GENerator:PULSe:PERiod



.. autoclass:: RsFswp.Implementations.Source.Generator.Pulse.Period.PeriodCls
	:members:
	:undoc-members:
	:noindex:
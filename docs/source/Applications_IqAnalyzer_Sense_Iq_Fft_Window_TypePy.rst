TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:IQ:FFT:WINDow:TYPE

.. code-block:: python

	SENSe:IQ:FFT:WINDow:TYPE



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Sense.Iq.Fft.Window.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
Fline<FreqLine>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.applications.k50Spurious.calculate.fline.repcap_freqLine_get()
	driver.applications.k50Spurious.calculate.fline.repcap_freqLine_set(repcap.FreqLine.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:FLINe<FreqLine>

.. code-block:: python

	CALCulate<Window>:FLINe<FreqLine>



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Fline.FlineCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.calculate.fline.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Calculate_Fline_State.rst
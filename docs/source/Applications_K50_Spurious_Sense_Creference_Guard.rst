Guard
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Guard.GuardCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.sense.creference.guard.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Sense_Creference_Guard_Interval.rst
	Applications_K50_Spurious_Sense_Creference_Guard_State.rst
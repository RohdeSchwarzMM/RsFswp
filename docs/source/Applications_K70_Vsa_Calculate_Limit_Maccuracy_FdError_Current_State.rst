State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:FDERror:CURRent:STATe

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:FDERror:CURRent:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.FdError.Current.State.StateCls
	:members:
	:undoc-members:
	:noindex:
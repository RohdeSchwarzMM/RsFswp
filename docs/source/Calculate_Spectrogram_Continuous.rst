Continuous
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:SPECtrogram:CONTinuous

.. code-block:: python

	CALCulate<Window>:SPECtrogram:CONTinuous



.. autoclass:: RsFswp.Implementations.Calculate.Spectrogram.Continuous.ContinuousCls
	:members:
	:undoc-members:
	:noindex:
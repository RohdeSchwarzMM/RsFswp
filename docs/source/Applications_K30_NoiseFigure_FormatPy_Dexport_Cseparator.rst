Cseparator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMat:DEXPort:CSEParator

.. code-block:: python

	FORMat:DEXPort:CSEParator



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.FormatPy.Dexport.Cseparator.CseparatorCls
	:members:
	:undoc-members:
	:noindex:
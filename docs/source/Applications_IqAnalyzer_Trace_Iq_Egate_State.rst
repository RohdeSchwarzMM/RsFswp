State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:EGATe:STATe

.. code-block:: python

	TRACe:IQ:EGATe:STATe



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.Egate.State.StateCls
	:members:
	:undoc-members:
	:noindex:
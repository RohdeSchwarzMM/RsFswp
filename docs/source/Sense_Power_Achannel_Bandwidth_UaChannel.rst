UaChannel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:BWIDth:UACHannel

.. code-block:: python

	SENSe:POWer:ACHannel:BWIDth:UACHannel



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Bandwidth.UaChannel.UaChannelCls
	:members:
	:undoc-members:
	:noindex:
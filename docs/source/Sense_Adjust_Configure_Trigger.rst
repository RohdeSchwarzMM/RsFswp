Trigger
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:CONFigure:TRIGger

.. code-block:: python

	SENSe:ADJust:CONFigure:TRIGger



.. autoclass:: RsFswp.Implementations.Sense.Adjust.Configure.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:
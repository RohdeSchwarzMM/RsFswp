Spacing
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:Y:SPACing

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:Y:SPACing



.. autoclass:: RsFswp.Implementations.Display.Window.Trace.Y.Spacing.SpacingCls
	:members:
	:undoc-members:
	:noindex:
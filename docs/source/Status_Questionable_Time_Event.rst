Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TIME:EVENt

.. code-block:: python

	STATus:QUEStionable:TIME:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Time.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SEARch:TIME

.. code-block:: python

	SENSe:DDEMod:SEARch:TIME



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Search.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:
Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:MINimum:NEXT

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:MINimum:NEXT



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Marker.Minimum.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
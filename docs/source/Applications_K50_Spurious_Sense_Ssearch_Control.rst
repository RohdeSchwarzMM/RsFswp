Control
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SSEarch:CONTrol

.. code-block:: python

	SENSe:SSEarch:CONTrol



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Ssearch.Control.ControlCls
	:members:
	:undoc-members:
	:noindex:
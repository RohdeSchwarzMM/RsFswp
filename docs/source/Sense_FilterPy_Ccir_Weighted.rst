Weighted
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.FilterPy.Ccir.Weighted.WeightedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.filterPy.ccir.weighted.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_FilterPy_Ccir_Weighted_State.rst
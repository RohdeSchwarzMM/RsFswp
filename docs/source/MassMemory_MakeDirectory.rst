MakeDirectory
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:MDIRectory

.. code-block:: python

	MMEMory:MDIRectory



.. autoclass:: RsFswp.Implementations.MassMemory.MakeDirectory.MakeDirectoryCls
	:members:
	:undoc-members:
	:noindex:
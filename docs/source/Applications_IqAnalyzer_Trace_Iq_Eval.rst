Eval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:EVAL

.. code-block:: python

	TRACe:IQ:EVAL



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.Eval.EvalCls
	:members:
	:undoc-members:
	:noindex:
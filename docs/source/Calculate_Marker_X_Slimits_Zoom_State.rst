State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:X:SLIMits:ZOOM:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:X:SLIMits:ZOOM:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.X.Slimits.Zoom.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Pshow
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:PSEarch:PSHow

.. code-block:: python

	CALCulate<Window>:PSEarch:PSHow



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.PeakSearch.Pshow.PshowCls
	:members:
	:undoc-members:
	:noindex:
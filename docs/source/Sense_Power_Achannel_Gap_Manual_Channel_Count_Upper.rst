Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:GAP<GapChannel>:MANual:CHANnel:COUNt:UPPer

.. code-block:: python

	SENSe:POWer:ACHannel:GAP<GapChannel>:MANual:CHANnel:COUNt:UPPer



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Gap.Manual.Channel.Count.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:MDEPth:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:MDEPth:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Mdepth.State.StateCls
	:members:
	:undoc-members:
	:noindex:
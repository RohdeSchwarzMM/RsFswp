TriggerInvoke
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: *TRG

.. code-block:: python

	*TRG



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.TriggerInvoke.TriggerInvokeCls
	:members:
	:undoc-members:
	:noindex:
String
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:REVision:STRing

.. code-block:: python

	SYSTem:REVision:STRing



.. autoclass:: RsFswp.Implementations.System.Revision.String.StringCls
	:members:
	:undoc-members:
	:noindex:
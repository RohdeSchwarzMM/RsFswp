Perror
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.calculate.limit.maccuracy.perror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_Calculate_Limit_Maccuracy_Perror_Pcurrent.rst
	Applications_K70_Vsa_Calculate_Limit_Maccuracy_Perror_Pmean.rst
	Applications_K70_Vsa_Calculate_Limit_Maccuracy_Perror_Ppeak.rst
	Applications_K70_Vsa_Calculate_Limit_Maccuracy_Perror_Rcurrent.rst
	Applications_K70_Vsa_Calculate_Limit_Maccuracy_Perror_Rmean.rst
	Applications_K70_Vsa_Calculate_Limit_Maccuracy_Perror_Rpeak.rst
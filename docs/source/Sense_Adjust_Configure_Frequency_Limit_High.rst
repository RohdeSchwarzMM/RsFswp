High
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:CONFigure:FREQuency:LIMit:HIGH

.. code-block:: python

	SENSe:ADJust:CONFigure:FREQuency:LIMit:HIGH



.. autoclass:: RsFswp.Implementations.Sense.Adjust.Configure.Frequency.Limit.High.HighCls
	:members:
	:undoc-members:
	:noindex:
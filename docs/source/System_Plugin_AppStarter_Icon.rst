Icon
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PLUGin:APPStarter:ICON

.. code-block:: python

	SYSTem:PLUGin:APPStarter:ICON



.. autoclass:: RsFswp.Implementations.System.Plugin.AppStarter.Icon.IconCls
	:members:
	:undoc-members:
	:noindex:
Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:EVM:PPEak:VALue

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:EVM:PPEak:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Evm.Ppeak.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
Scale
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:PAGE:WINDow:SCALe

.. code-block:: python

	HCOPy:PAGE:WINDow:SCALe



.. autoclass:: RsFswp.Implementations.HardCopy.Page.Window.Scale.ScaleCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:PMETer<PowerMeter>:STATe

.. code-block:: python

	DISPlay:WINDow<Window>:PMETer<PowerMeter>:STATe



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Display.Window.Pmeter.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Trange
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ROSCillator:TRANge

.. code-block:: python

	SENSe:ROSCillator:TRANge



.. autoclass:: RsFswp.Implementations.Sense.Roscillator.Trange.TrangeCls
	:members:
	:undoc-members:
	:noindex:
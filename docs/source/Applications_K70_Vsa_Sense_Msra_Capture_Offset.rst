Offset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MSRA:CAPTure:OFFSet

.. code-block:: python

	SENSe:MSRA:CAPTure:OFFSet



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Msra.Capture.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:
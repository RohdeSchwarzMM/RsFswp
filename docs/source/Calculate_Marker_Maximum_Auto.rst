Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:MAXimum:AUTO

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:MAXimum:AUTO



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Maximum.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
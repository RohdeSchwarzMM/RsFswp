Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMAT REAL,32;TRACe<Window>:DATA

.. code-block:: python

	FORMAT REAL,32;TRACe<Window>:DATA



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Trace.Data.DataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k60Transient.trace.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K60_Transient_Trace_Data_X.rst
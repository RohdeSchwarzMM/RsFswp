Begin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:HOPDetection:PNOise:OFFSet:BEGin

.. code-block:: python

	CALCulate<Window>:HOPDetection:PNOise:OFFSet:BEGin



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.HopDetection.Pnoise.Offset.Begin.BeginCls
	:members:
	:undoc-members:
	:noindex:
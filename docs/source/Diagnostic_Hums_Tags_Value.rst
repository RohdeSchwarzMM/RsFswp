Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:TAGS:VALue

.. code-block:: python

	DIAGnostic:HUMS:TAGS:VALue



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.Tags.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
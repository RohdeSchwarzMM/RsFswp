Random
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:PNOise<Trace>:SPURs:RANDom

.. code-block:: python

	FETCh:PNOise<Trace>:SPURs:RANDom



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Fetch.Pnoise.Spurs.Random.RandomCls
	:members:
	:undoc-members:
	:noindex:
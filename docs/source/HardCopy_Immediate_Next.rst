Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:IMMediate:NEXT

.. code-block:: python

	HCOPy:IMMediate:NEXT



.. autoclass:: RsFswp.Implementations.HardCopy.Immediate.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
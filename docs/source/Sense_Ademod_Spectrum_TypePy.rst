TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:SPECtrum:TYPE

.. code-block:: python

	SENSe:ADEMod:SPECtrum:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Spectrum.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
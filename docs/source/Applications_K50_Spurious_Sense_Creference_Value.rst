Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:VALue

.. code-block:: python

	SENSe:CREFerence:VALue



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
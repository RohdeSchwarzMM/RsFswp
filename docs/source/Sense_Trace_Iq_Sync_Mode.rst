Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:TRACe:IQ:SYNC:MODE

.. code-block:: python

	SENSe:TRACe:IQ:SYNC:MODE



.. autoclass:: RsFswp.Implementations.Sense.Trace.Iq.Sync.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
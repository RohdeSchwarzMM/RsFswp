Margin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:ESPectrum:PSEarch:MARGin

.. code-block:: python

	CALCulate<Window>:ESPectrum:PSEarch:MARGin



.. autoclass:: RsFswp.Implementations.Calculate.Espectrum.PeakSearch.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:
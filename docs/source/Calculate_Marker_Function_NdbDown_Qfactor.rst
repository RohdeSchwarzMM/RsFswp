Qfactor
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:NDBDown:QFACtor

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:NDBDown:QFACtor



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.NdbDown.Qfactor.QfactorCls
	:members:
	:undoc-members:
	:noindex:
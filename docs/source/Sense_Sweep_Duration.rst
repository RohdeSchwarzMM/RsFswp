Duration
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:DURation

.. code-block:: python

	SENSe:SWEep:DURation



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Duration.DurationCls
	:members:
	:undoc-members:
	:noindex:
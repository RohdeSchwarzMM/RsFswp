ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MPOWer:RESult:LIST

.. code-block:: python

	SENSe:MPOWer:RESult:LIST



.. autoclass:: RsFswp.Implementations.Sense.Mpower.Result.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:
Gain
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:DATA:GAIN

.. code-block:: python

	CALCulate<Window>:UNCertainty:DATA:GAIN



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Data.Gain.GainCls
	:members:
	:undoc-members:
	:noindex:
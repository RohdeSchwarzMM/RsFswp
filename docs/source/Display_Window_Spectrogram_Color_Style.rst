Style
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SPECtrogram:COLor:STYLe

.. code-block:: python

	DISPlay:WINDow<Window>:SPECtrogram:COLor:STYLe



.. autoclass:: RsFswp.Implementations.Display.Window.Spectrogram.Color.Style.StyleCls
	:members:
	:undoc-members:
	:noindex:
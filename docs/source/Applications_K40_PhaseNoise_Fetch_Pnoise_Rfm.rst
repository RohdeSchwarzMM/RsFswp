Rfm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:PNOise<Trace>:RFM

.. code-block:: python

	FETCh:PNOise<Trace>:RFM



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Fetch.Pnoise.Rfm.RfmCls
	:members:
	:undoc-members:
	:noindex:
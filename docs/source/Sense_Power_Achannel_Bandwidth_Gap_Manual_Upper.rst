Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:BWIDth:GAP<GapChannel>:MANual:UPPer

.. code-block:: python

	SENSe:POWer:ACHannel:BWIDth:GAP<GapChannel>:MANual:UPPer



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Bandwidth.Gap.Manual.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex:
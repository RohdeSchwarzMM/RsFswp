LastResult
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:SFUNction:LASTresult

.. code-block:: python

	DIAGnostic:SERVice:SFUNction:LASTresult



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.Sfunction.LastResult.LastResultCls
	:members:
	:undoc-members:
	:noindex:
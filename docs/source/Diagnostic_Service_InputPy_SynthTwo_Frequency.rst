Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:INPut:SYNThtwo:FREQuency

.. code-block:: python

	DIAGnostic:SERVice:INPut:SYNThtwo:FREQuency



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.InputPy.SynthTwo.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:
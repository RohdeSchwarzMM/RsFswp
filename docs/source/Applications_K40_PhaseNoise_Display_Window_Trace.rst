Trace<Trace>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Tr1 .. Tr16
	rc = driver.applications.k40PhaseNoise.display.window.trace.repcap_trace_get()
	driver.applications.k40PhaseNoise.display.window.trace.repcap_trace_set(repcap.Trace.Tr1)





.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Display.Window.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k40PhaseNoise.display.window.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K40_PhaseNoise_Display_Window_Trace_Select.rst
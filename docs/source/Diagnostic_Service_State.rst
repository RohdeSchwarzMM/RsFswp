State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:STATe

.. code-block:: python

	DIAGnostic:SERVice:STATe



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.State.StateCls
	:members:
	:undoc-members:
	:noindex:
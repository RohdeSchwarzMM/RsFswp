Pcount
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:CONTinuous:PCOunt

.. code-block:: python

	SENSe:SWEep:EGATe:CONTinuous:PCOunt



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Sweep.Egate.Continuous.Pcount.PcountCls
	:members:
	:undoc-members:
	:noindex:
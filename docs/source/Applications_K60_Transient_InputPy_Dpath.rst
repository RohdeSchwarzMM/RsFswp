Dpath
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:DPATh

.. code-block:: python

	INPut:DPATh



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.InputPy.Dpath.DpathCls
	:members:
	:undoc-members:
	:noindex:
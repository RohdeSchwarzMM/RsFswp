HwInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:HWINfo

.. code-block:: python

	DIAGnostic:SERVice:HWINfo



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.HwInfo.HwInfoCls
	:members:
	:undoc-members:
	:noindex:
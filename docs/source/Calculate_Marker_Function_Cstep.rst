Cstep
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:CSTep

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:CSTep



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Cstep.CstepCls
	:members:
	:undoc-members:
	:noindex:
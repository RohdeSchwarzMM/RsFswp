State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:RFOVerrange:STATe

.. code-block:: python

	SENSe:MIXer:RFOVerrange:STATe



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Mixer.RfOverrange.State.StateCls
	:members:
	:undoc-members:
	:noindex:
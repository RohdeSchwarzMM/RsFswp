Compatible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PRESet:COMPatible

.. code-block:: python

	SYSTem:PRESet:COMPatible



.. autoclass:: RsFswp.Implementations.System.Preset.Compatible.CompatibleCls
	:members:
	:undoc-members:
	:noindex:
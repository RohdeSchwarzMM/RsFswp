Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:INPut:GAIN:VALue

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:INPut:GAIN:VALue



.. autoclass:: RsFswp.Implementations.Sense.ListPy.Range.InputPy.Gain.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PROBe<Probe>:SETup:STATe

.. code-block:: python

	SENSe:PROBe<Probe>:SETup:STATe



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Probe.Setup.State.StateCls
	:members:
	:undoc-members:
	:noindex:
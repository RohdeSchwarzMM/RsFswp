Port
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SENDer:PORT

.. code-block:: python

	TRIGger:SENDer:PORT



.. autoclass:: RsFswp.Implementations.Trigger.Sender.Port.PortCls
	:members:
	:undoc-members:
	:noindex:
Limit
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Current.Control.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.current.control.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Current_Control_Limit_High.rst
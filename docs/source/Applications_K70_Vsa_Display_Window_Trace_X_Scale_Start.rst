Start
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:X:SCALe:STARt

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:X:SCALe:STARt



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Display.Window.Trace.X.Scale.Start.StartCls
	:members:
	:undoc-members:
	:noindex:
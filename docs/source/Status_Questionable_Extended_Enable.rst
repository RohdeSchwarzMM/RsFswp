Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:EXTended:ENABle

.. code-block:: python

	STATus:QUEStionable:EXTended:ENABle



.. autoclass:: RsFswp.Implementations.Status.Questionable.Extended.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
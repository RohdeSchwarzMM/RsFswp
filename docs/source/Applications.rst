Applications
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.ApplicationsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_IqAnalyzer.rst
	Applications_K30_NoiseFigure.rst
	Applications_K40_PhaseNoise.rst
	Applications_K50_Spurious.rst
	Applications_K60_Transient.rst
	Applications_K7_AnalogDemod.rst
	Applications_K70_Vsa.rst
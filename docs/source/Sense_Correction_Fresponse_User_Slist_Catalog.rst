Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:CATalog

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:CATalog



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Slist.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:
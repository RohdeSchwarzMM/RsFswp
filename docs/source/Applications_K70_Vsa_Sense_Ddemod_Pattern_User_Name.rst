Name
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:USER:NAME

.. code-block:: python

	SENSe:DDEMod:PATTern:USER:NAME



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.User.Name.NameCls
	:members:
	:undoc-members:
	:noindex:
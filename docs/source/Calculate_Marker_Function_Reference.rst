Reference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:REFerence

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:REFerence



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex:
Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:SPECtrogram:Y:MAXimum:NEXT

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:SPECtrogram:Y:MAXimum:NEXT



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.Marker.Spectrogram.Y.Maximum.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
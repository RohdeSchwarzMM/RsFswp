Transducer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:TRANsducer

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:TRANsducer



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Range.Transducer.TransducerCls
	:members:
	:undoc-members:
	:noindex:
Wselect
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WSELect

.. code-block:: python

	DISPlay:WSELect



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Display.Wselect.WselectCls
	:members:
	:undoc-members:
	:noindex:
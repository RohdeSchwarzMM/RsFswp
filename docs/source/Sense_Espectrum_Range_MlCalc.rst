MlCalc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:MLCalc

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:RANGe<RangePy>:MLCalc



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Range.MlCalc.MlCalcCls
	:members:
	:undoc-members:
	:noindex:
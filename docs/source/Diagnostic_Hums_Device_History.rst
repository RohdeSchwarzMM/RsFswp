History
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:DEVice:HISTory
	single: DIAGnostic:HUMS:DEVice:HISTory:DELete:ALL

.. code-block:: python

	DIAGnostic:HUMS:DEVice:HISTory
	DIAGnostic:HUMS:DEVice:HISTory:DELete:ALL



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.Device.History.HistoryCls
	:members:
	:undoc-members:
	:noindex:
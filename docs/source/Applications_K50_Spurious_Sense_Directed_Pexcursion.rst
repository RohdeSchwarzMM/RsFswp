Pexcursion
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DIRected:PEXCursion

.. code-block:: python

	SENSe:DIRected:PEXCursion



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Directed.Pexcursion.PexcursionCls
	:members:
	:undoc-members:
	:noindex:
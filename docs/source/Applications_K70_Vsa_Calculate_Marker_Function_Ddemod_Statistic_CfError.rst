CfError
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:CFERror

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:DDEMod:STATistic:CFERror



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Marker.Function.Ddemod.Statistic.CfError.CfErrorCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:STATe

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:STATe



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Display.Window.Trace.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Duplicate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:CREate:DUPLicate

.. code-block:: python

	INSTrument:CREate:DUPLicate



.. autoclass:: RsFswp.Implementations.Instrument.Create.Duplicate.DuplicateCls
	:members:
	:undoc-members:
	:noindex:
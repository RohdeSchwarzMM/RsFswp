Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:FLISt<FileList>:DATA:FREQuency

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:FLISt<FileList>:DATA:FREQuency



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Flist.Data.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:
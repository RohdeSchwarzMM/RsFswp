Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:MODulation<Window>:PHASe:CONDition

.. code-block:: python

	STATus:QUEStionable:MODulation<Window>:PHASe:CONDition



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Status.Questionable.Modulation.Phase.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
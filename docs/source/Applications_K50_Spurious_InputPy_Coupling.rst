Coupling
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:COUPling

.. code-block:: python

	INPut:COUPling



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.InputPy.Coupling.CouplingCls
	:members:
	:undoc-members:
	:noindex:
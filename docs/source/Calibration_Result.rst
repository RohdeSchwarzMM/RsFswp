Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:RESult

.. code-block:: python

	CALibration:RESult



.. autoclass:: RsFswp.Implementations.Calibration.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
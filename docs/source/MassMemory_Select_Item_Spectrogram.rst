Spectrogram
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:SELect:ITEM:SPECtrogram

.. code-block:: python

	MMEMory:SELect:ITEM:SPECtrogram



.. autoclass:: RsFswp.Implementations.MassMemory.Select.Item.Spectrogram.SpectrogramCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DLABs:STATe

.. code-block:: python

	CALCulate<Window>:DLABs:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Dlabs.State.StateCls
	:members:
	:undoc-members:
	:noindex:
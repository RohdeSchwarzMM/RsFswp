Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:MODE

.. code-block:: python

	INSTrument:MODE



.. autoclass:: RsFswp.Implementations.Instrument.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
PassThrough
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ROSCillator:PASSthrough

.. code-block:: python

	SENSe:ROSCillator:PASSthrough



.. autoclass:: RsFswp.Implementations.Sense.Roscillator.PassThrough.PassThroughCls
	:members:
	:undoc-members:
	:noindex:
Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:MODulation<Window>:CFRequency:ENABle

.. code-block:: python

	STATus:QUEStionable:MODulation<Window>:CFRequency:ENABle



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Status.Questionable.Modulation.Cfrequency.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
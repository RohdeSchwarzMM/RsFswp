Standard
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:STANdard:SAVE
	single: SENSe:DDEMod:STANdard:DELete

.. code-block:: python

	SENSe:DDEMod:STANdard:SAVE
	SENSe:DDEMod:STANdard:DELete



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Standard.StandardCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.sense.ddemod.standard.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_Sense_Ddemod_Standard_Comment.rst
	Applications_K70_Vsa_Sense_Ddemod_Standard_Preset.rst
	Applications_K70_Vsa_Sense_Ddemod_Standard_Sync.rst
High
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:LOSS:HIGH

.. code-block:: python

	SENSe:MIXer:LOSS:HIGH



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Sense.Mixer.Loss.High.HighCls
	:members:
	:undoc-members:
	:noindex:
Refresh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:REFResh

.. code-block:: python

	INITiate:REFResh



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Initiate.Refresh.RefreshCls
	:members:
	:undoc-members:
	:noindex:
Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:FREQuency:CONDition

.. code-block:: python

	STATus:QUEStionable:FREQuency:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Frequency.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
RfbWidth
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:MSR:RFBWidth

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:MSR:RFBWidth



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Msr.RfbWidth.RfbWidthCls
	:members:
	:undoc-members:
	:noindex:
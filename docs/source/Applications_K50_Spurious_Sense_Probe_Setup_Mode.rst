Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PROBe<Probe>:SETup:MODE

.. code-block:: python

	SENSe:PROBe<Probe>:SETup:MODE



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Probe.Setup.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
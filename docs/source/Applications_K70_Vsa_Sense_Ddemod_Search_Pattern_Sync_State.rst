State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SEARch:PATTern:SYNC:STATe

.. code-block:: python

	SENSe:DDEMod:SEARch:PATTern:SYNC:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Search.Pattern.Sync.State.StateCls
	:members:
	:undoc-members:
	:noindex:
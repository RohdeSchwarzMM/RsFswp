Trigger
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k60Transient.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K60_Transient_Trigger_Sequence.rst
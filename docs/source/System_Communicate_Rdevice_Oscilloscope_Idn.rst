Idn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:RDEVice:OSCilloscope:IDN

.. code-block:: python

	SYSTem:COMMunicate:RDEVice:OSCilloscope:IDN



.. autoclass:: RsFswp.Implementations.System.Communicate.Rdevice.Oscilloscope.Idn.IdnCls
	:members:
	:undoc-members:
	:noindex:
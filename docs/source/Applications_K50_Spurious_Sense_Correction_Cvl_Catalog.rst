Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:CVL:CATalog

.. code-block:: python

	SENSe:CORRection:CVL:CATalog



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Correction.Cvl.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:
Read
----------------------------------------





.. autoclass:: RsFswp.Implementations.Read.ReadCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.read.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Read_Pmeter.rst
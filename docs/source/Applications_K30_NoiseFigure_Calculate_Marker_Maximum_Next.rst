Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:MAXimum:NEXT

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:MAXimum:NEXT



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Marker.Maximum.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
Span
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:BPOWer:SPAN

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:BPOWer:SPAN



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Function.Bpower.Span.SpanCls
	:members:
	:undoc-members:
	:noindex:
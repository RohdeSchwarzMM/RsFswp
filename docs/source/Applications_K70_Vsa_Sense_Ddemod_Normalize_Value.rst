Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:NORMalize:VALue

.. code-block:: python

	SENSe:DDEMod:NORMalize:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Normalize.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
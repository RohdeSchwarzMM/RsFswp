Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:ACPLimit:EVENt

.. code-block:: python

	STATus:QUEStionable:ACPLimit:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.AcpLimit.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
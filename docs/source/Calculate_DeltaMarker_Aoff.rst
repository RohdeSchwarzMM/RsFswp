Aoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker:AOFF

.. code-block:: python

	CALCulate<Window>:DELTamarker:AOFF



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Aoff.AoffCls
	:members:
	:undoc-members:
	:noindex:
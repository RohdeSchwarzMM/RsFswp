Splitter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:SPLitter

.. code-block:: python

	LAYout:SPLitter



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Layout.Splitter.SplitterCls
	:members:
	:undoc-members:
	:noindex:
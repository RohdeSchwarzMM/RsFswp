Generator
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Generator_Channel.rst
	Source_Generator_DutBypass.rst
	Source_Generator_Frequency.rst
	Source_Generator_Level.rst
	Source_Generator_Modulation.rst
	Source_Generator_Pulse.rst
	Source_Generator_State.rst
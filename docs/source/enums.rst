Enums
=========

AccessType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AccessType.RO
	# All values (2x):
	RO | RW

AdcPreFilterMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AdcPreFilterMode.AUTO
	# All values (2x):
	AUTO | WIDE

AdemMeasType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AdemMeasType.MIDDle
	# All values (5x):
	MIDDle | MPEak | NPEak | PPEak | RMS

AdjustAlignment
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AdjustAlignment.CENTer
	# All values (3x):
	CENTer | LEFT | RIGHt

AngleUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AngleUnit.DEG
	# All values (2x):
	DEG | RAD

AnnotationMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AnnotationMode.CSPan
	# All values (2x):
	CSPan | SSTop

AttenuatorMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AttenuatorMode.LDIStortion
	# All values (3x):
	LDIStortion | LNOise | NORMal

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

AutoManualUserMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualUserMode.AUTO
	# All values (3x):
	AUTO | MANual | USER

AutoMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoMode.AUTO
	# All values (3x):
	AUTO | OFF | ON

AutoOrOff
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoOrOff.AUTO
	# All values (2x):
	AUTO | OFF

AverageModeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AverageModeA.LINear
	# All values (3x):
	LINear | LOGarithmic | POWer

AverageModeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AverageModeB.LINear
	# All values (6x):
	LINear | LOGarithmic | MAXimum | POWer | SCALar | VIDeo

Band
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Band.A
	# Last value:
	value = enums.Band.Y
	# All values (14x):
	A | D | E | F | G | J | K | KA
	Q | U | USER | V | W | Y

BandB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BandB.D
	# Last value:
	value = enums.BandB.Y
	# All values (12x):
	D | E | F | G | J | KA | Q | U
	USER | V | W | Y

BbInputSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BbInputSource.AIQ
	# All values (4x):
	AIQ | DIQ | FIQ | RF

BerRateFormat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BerRateFormat.CURRent
	# Last value:
	value = enums.BerRateFormat.TTOTal
	# All values (17x):
	CURRent | DSINdex | MAX | MIN | SECurrent | SEMax | SEMin | SETotal
	TCURrent | TECurrent | TEMax | TEMin | TETotal | TMAX | TMIN | TOTal
	TTOTal

BitOrdering
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BitOrdering.LSB
	# All values (2x):
	LSB | MSB

BurstMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BurstMode.BURS
	# All values (2x):
	BURS | MEAS

CalibrationScope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalibrationScope.ACLear
	# All values (4x):
	ACLear | ALL | OFF | ON

CalibrationState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalibrationState.EXPired
	# All values (3x):
	EXPired | NAN | OK

ChannelType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ChannelType.IqAnalyzer=IQ
	# Last value:
	value = enums.ChannelType.SpectrumAnalyzer=SANALYZER
	# All values (31x):
	IqAnalyzer | K10_Gsm | K106_NbIot | K10x_Lte | K118_Verizon5G | K14x_5GnewRadio | K15_Avionics | K17_MultiCarrierGroupDelay
	K18_AmplifierMeas | K192_193_Docsis31 | K201_OneWeb | K30_Noise | K40_PhaseNoise | K50_FastSpurSearch | K6_PulseAnalysis | K60_TransientAnalysis
	K7_AnalogModulation | K70_VectorSignalAnalyzer | K72_3GppFddBts | K73_3GppFddUe | K76_TdScdmaBts | K77_TdScdmaUe | K82_Cdma2000Bts | K83_Cdma2000Ms
	K84_EvdoBts | K85_EvdoMs | K91_Wlan | K95_80211ad | K97_80211ay | RealTimeSpectrum | SpectrumAnalyzer

CheckResult
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CheckResult.FAILED
	# All values (2x):
	FAILED | PASSED

Color
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Color.BLACk
	# Last value:
	value = enums.Color.YELLow
	# All values (16x):
	BLACk | BLUE | BROWn | CYAN | DGRay | GREen | LBLue | LCYan
	LGRay | LGReen | LMAGenta | LRED | MAGenta | RED | WHITe | YELLow

ColorSchemeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ColorSchemeA.COLD
	# All values (5x):
	COLD | COLor | GRAYscale | HOT | RADar

CompatibilityMode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CompatibilityMode.ATT
	# Last value:
	value = enums.CompatibilityMode.FSWXv1_0
	# All values (11x):
	ATT | DEFault | FSET | FSL | FSMR | FSP | FSQ | FSU
	FSV | FSW | FSWXv1_0

ComponentType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ComponentType.AMPLifier
	# All values (4x):
	AMPLifier | DIVider | MIXer | MULTiplier

ConfigMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConfigMode.DEFault
	# All values (2x):
	DEFault | USER

CorrectionMeasType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CorrectionMeasType.OPEN
	# All values (2x):
	OPEN | THRough

CorrectionMethod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CorrectionMethod.REFLexion
	# All values (2x):
	REFLexion | TRANsmission

CorrectionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CorrectionMode.SPOT
	# All values (2x):
	SPOT | TABLe

Counter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Counter.CAPTure
	# All values (2x):
	CAPTure | STATistics

CouplingTypeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CouplingTypeA.AC
	# All values (2x):
	AC | DC

CouplingTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CouplingTypeB.AC
	# All values (3x):
	AC | DC | DCLimit

DataExportMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataExportMode.RAW
	# All values (2x):
	RAW | TRACe

DataFormat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DataFormat.ASCii
	# Last value:
	value = enums.DataFormat.UINT_cma_64
	# All values (10x):
	ASCii | MATLAB_cma_16 | MATLAB_cma_32 | MATLAB_cma_64 | Real16 | Real32 | Real64 | UINT_cma_16
	UINT_cma_32 | UINT_cma_64

DaysOfWeek
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DaysOfWeek.ALL
	# All values (8x):
	ALL | FRIDay | MONDay | SATurday | SUNDay | THURsday | TUESday | WEDNesday

DdemGroup
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DdemGroup.APSK
	# All values (8x):
	APSK | ASK | FSK | MSK | PSK | QAM | QPSK | UQAM

DdemodFilter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DdemodFilter.A25Fm
	# Last value:
	value = enums.DdemodFilter.RRCosine
	# All values (13x):
	A25Fm | B22 | B25 | B44 | EMES | EREF | GAUSsian | QFM
	QFR | QRM | QRR | RCOSine | RRCosine

DdemResultType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DdemResultType.ADR
	# Last value:
	value = enums.DdemResultType.RHO
	# All values (20x):
	ADR | DEV | DTTS | EVPK | EVPS | EVRM | FEPK | FERR
	FSPK | FSPS | FSRM | IQIM | IQOF | MEPK | MEPS | MERM
	PEPK | PEPS | PERM | RHO

DdemSignalType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DdemSignalType.BURSted
	# All values (2x):
	BURSted | CONTinuous

Detect
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Detect.DETected
	# All values (2x):
	DETected | NDETected

DetectorB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DetectorB.ACSine
	# Last value:
	value = enums.DetectorB.SMP
	# All values (13x):
	ACSine | ACVideo | APEak | AVERage | CAVerage | CRMS | NEGative | NRM
	POSitive | QPEak | RMS | SAMPle | SMP

DetectorC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DetectorC.ACSine
	# All values (8x):
	ACSine | ACVideo | APEak | AVERage | NEGative | POSitive | RMS | SAMPle

DiagnosticSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DiagnosticSignal.AIQ
	# All values (8x):
	AIQ | CALibration | EMI | MCALibration | RF | SYNThtwo | TG | WBCal

DiqUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DiqUnit.AMPere
	# All values (8x):
	AMPere | DBM | DBMV | DBPW | DBUA | DBUV | VOLT | WATT

DisplayFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DisplayFormat.SINGle
	# All values (2x):
	SINGle | SPLit

DisplayPosition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DisplayPosition.BOTTom
	# All values (3x):
	BOTTom | OFF | TOP

Duration
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Duration.LONG
	# All values (3x):
	LONG | NORMal | SHORt

DutType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DutType.AMPLifier
	# All values (4x):
	AMPLifier | DDOWnconv | DOWNconv | UPConv

EgateType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EgateType.EDGE
	# All values (2x):
	EDGE | LEVel

EnrType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EnrType.DIODe
	# All values (3x):
	DIODe | RESistor | SMARt

EspectrumRtype
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EspectrumRtype.CPOWer
	# All values (2x):
	CPOWer | PEAK

EventOnce
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EventOnce.ONCE
	# All values (1x):
	ONCE

EvmCalc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EvmCalc.MACPower
	# All values (4x):
	MACPower | MECPower | SIGNal | SYMBol

Factory
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Factory.ALL
	# All values (3x):
	ALL | PATTern | STANdard

FftFilterMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FftFilterMode.AUTO
	# All values (3x):
	AUTO | NARRow | WIDE

FftWindowType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FftWindowType.BLACkharris
	# All values (8x):
	BLACkharris | FLATtop | GAUSsian | HAMMing | HANNing | KAISerbessel | P5 | RECTangular

FileFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FileFormat.CSV
	# All values (2x):
	CSV | DAT

FileFormatDdem
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FileFormatDdem.FRES
	# All values (2x):
	FRES | VAE

FileSeparator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FileSeparator.COMMa
	# All values (3x):
	COMMa | SEMicolon | TAB

FilterTypeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterTypeA.FLAT
	# All values (2x):
	FLAT | GAUSs

FilterTypeB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FilterTypeB.CFILter
	# Last value:
	value = enums.FilterTypeB.RRC
	# All values (9x):
	CFILter | CISPr | FFT | MIL | NOISe | NORMal | P5 | PULSe
	RRC

FilterTypeC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterTypeC.CFILter
	# All values (7x):
	CFILter | CISPr | MIL | NORMal | P5 | PULSe | RRC

FilterTypeK91
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterTypeK91.CFILter
	# All values (5x):
	CFILter | NORMal | P5 | PULSe | RRC

FineSync
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FineSync.DDATa
	# All values (3x):
	DDATa | KDATa | PATTern

FpeaksSortMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FpeaksSortMode.X
	# All values (2x):
	X | Y

FrameModulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrameModulation.AUTO
	# All values (3x):
	AUTO | DATA | PATTern

FrameModulationB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrameModulationB.DATA
	# All values (2x):
	DATA | PATTern

FramesScope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FramesScope.ALL
	# All values (2x):
	ALL | CHANnel

FrequencyCouplingLinkA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrequencyCouplingLinkA.OFF
	# All values (3x):
	OFF | RBW | SPAN

FrequencyType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrequencyType.IF
	# All values (3x):
	IF | LO | RF

FunctionA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FunctionA.MAX
	# All values (2x):
	MAX | OFF

FunctionB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FunctionB.MAX
	# All values (3x):
	MAX | NONE | SUM

GatedSourceK30
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GatedSourceK30.EXT2
	# All values (3x):
	EXT2 | EXT3 | EXTernal

GeneratorIntf
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorIntf.GPIB
	# All values (2x):
	GPIB | TCPip

GeneratorIntfType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorIntfType.GPIB
	# All values (3x):
	GPIB | PEXPress | TCPip

GeneratorLink
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorLink.GPIB
	# All values (2x):
	GPIB | TTL

GpibTerminator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GpibTerminator.EOI
	# All values (2x):
	EOI | LFEoi

HardcopyContent
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardcopyContent.HCOPy
	# All values (2x):
	HCOPy | WINDows

HardcopyHeader
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardcopyHeader.ALWays
	# All values (5x):
	ALWays | GLOBal | NEVer | ONCE | SECTion

HardcopyLogo
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardcopyLogo.ALWays
	# All values (3x):
	ALWays | GLOBal | NEVer

HardcopyMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardcopyMode.REPort
	# All values (2x):
	REPort | SCReen

HardcopyPageSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardcopyPageSize.A4
	# All values (2x):
	A4 | US

HeadersK50
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.HeadersK50.ALL
	# Last value:
	value = enums.HeadersK50.STOP
	# All values (9x):
	ALL | DELTa | FREQuency | IDENt | POWer | RBW | SID | STARt
	STOP

HumsFileFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HumsFileFormat.JSON
	# All values (2x):
	JSON | XML

IdnFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IdnFormat.FSL
	# All values (3x):
	FSL | LEGacy | NEW

IfGainMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IfGainMode.NORMal
	# All values (2x):
	NORMal | PULSe

IfGainModeDdem
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IfGainModeDdem.AVERaging
	# All values (5x):
	AVERaging | FREeze | NORMal | TRACking | USER

InOutDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InOutDirection.INPut
	# All values (2x):
	INPut | OUTPut

InputConnectorB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputConnectorB.AIQI
	# All values (3x):
	AIQI | RF | RFPRobe

InputConnectorC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputConnectorC.RF
	# All values (2x):
	RF | RFPRobe

InputSelect
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputSelect.INPut1
	# All values (2x):
	INPut1 | INPut2

InputSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputSource.ABBand
	# All values (7x):
	ABBand | AIQ | DIQ | FIQ | OBBand | RF | RFAiq

InputSourceB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputSourceB.FIQ
	# All values (2x):
	FIQ | RF

InstrumentMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InstrumentMode.MSRanalyzer
	# All values (3x):
	MSRanalyzer | RTMStandard | SANalyzer

IqBandwidthMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqBandwidthMode.AUTO
	# All values (3x):
	AUTO | FFT | MANual

IqDataFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqDataFormat.FloatComplex=FLOat32,COMPlex
	# All values (4x):
	FloatComplex | FloatReal | IntegerComplex | IntegerReal

IqDataFormatDdem
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.IqDataFormatDdem.FloatComplex=FLOat32,COMPlex
	# Last value:
	value = enums.IqDataFormatDdem.IntegerReal=INT32,REAL
	# All values (10x):
	FloatComplex | FloatIiQq | FloatIqIq | FloatPolar | FloatReal | IntegerComplex | IntegerIiQq | IntegerIqIq
	IntegerPolar | IntegerReal

IqRangeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqRangeType.CAPTure
	# All values (2x):
	CAPTure | RRANge

IqResultDataFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqResultDataFormat.COMPatible
	# All values (3x):
	COMPatible | IQBLock | IQPair

IqType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqType.Ipart=I
	# All values (3x):
	Ipart | IQpart | Qpart

LeftRightDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeftRightDirection.LEFT
	# All values (2x):
	LEFT | RIGHt

LimitState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LimitState.ABSolute
	# All values (4x):
	ABSolute | AND | OR | RELative

LoadType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoadType.NEW
	# All values (2x):
	NEW | REPLace

LoType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoType.FIXed
	# All values (2x):
	FIXed | VARiable

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

MarkerFunctionA
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MarkerFunctionA.ACPower
	# Last value:
	value = enums.MarkerFunctionA.TPOWer
	# All values (15x):
	ACPower | AOBandwidth | AOBWidth | CN | CN0 | COBandwidth | COBWidth | CPOWer
	GACLr | MACM | MCACpower | OBANdwidth | OBWidth | PPOWer | TPOWer

MarkerFunctionB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MarkerFunctionB.ACPower
	# All values (8x):
	ACPower | AOBWidth | CN | CN0 | CPOWer | MCACpower | OBANdwidth | OBWidth

MarkerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MarkerMode.DENSity
	# All values (3x):
	DENSity | POWer | RPOWer

MarkerRealImagB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MarkerRealImagB.IMAG
	# All values (2x):
	IMAG | REAL

MeasurementStep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementStep.NESTimate
	# All values (4x):
	NESTimate | SDETection | SOVerview | SPOTstep

MeasurementType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementType.DIRected
	# All values (2x):
	DIRected | WIDE

MessageType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MessageType.REMote
	# All values (2x):
	REMote | SMSG

MixerIdentifier
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MixerIdentifier.CLOCk
	# All values (2x):
	CLOCk | LO

MpowerDetector
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MpowerDetector.MEAN
	# All values (2x):
	MEAN | PEAK

MskFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MskFormat.DIFFerential
	# All values (4x):
	DIFFerential | NORMal | TYPe1 | TYPe2

MspurSearchType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MspurSearchType.DMINimum
	# All values (2x):
	DMINimum | PMAXimum

MsSyncMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MsSyncMode.MASTer
	# All values (5x):
	MASTer | NONE | PRIMary | SECondary | SLAVe

NoiseFigureLimit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoiseFigureLimit.ENR
	# All values (7x):
	ENR | GAIN | NOISe | PCOLd | PHOT | TEMPerature | YFACtor

NoiseFigureResult
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NoiseFigureResult.CPCold
	# Last value:
	value = enums.NoiseFigureResult.YFACtor
	# All values (11x):
	CPCold | CPHot | CYFactor | ENR | GAIN | NOISe | NUNCertainty | PCOLd
	PHOT | TEMPerature | YFACtor

NoiseFigureResultCustom
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NoiseFigureResultCustom.CPCold
	# Last value:
	value = enums.NoiseFigureResultCustom.YFACtor
	# All values (10x):
	CPCold | CPHot | CYFactor | ENR | GAIN | NOISe | PCOLd | PHOT
	TEMPerature | YFACtor

OddEven
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OddEven.EODD
	# All values (3x):
	EODD | EVEN | ODD

OffState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OffState.OFF
	# All values (1x):
	OFF

OptimizationCriterion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OptimizationCriterion.EVMMin
	# All values (2x):
	EVMMin | RMSMin

OptionState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OptionState.OCCupy
	# All values (3x):
	OCCupy | OFF | ON

OutputType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OutputType.DEVice
	# All values (4x):
	DEVice | TARMed | TOFF | UDEFined

PadType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PadType.MLPad
	# All values (2x):
	MLPad | SRESistor

PageMarginUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PageMarginUnit.IN
	# All values (2x):
	IN | MM

PageOrientation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PageOrientation.LANDscape
	# All values (2x):
	LANDscape | PORTrait

PathToCalibrate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PathToCalibrate.FULL
	# All values (2x):
	FULL | PARTial

PictureFormat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PictureFormat.BMP
	# Last value:
	value = enums.PictureFormat.WMF
	# All values (11x):
	BMP | DOC | EWMF | GDI | JPEG | JPG | PDF | PNG
	RTF | SVG | WMF

PmeterFreqLink
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PmeterFreqLink.CENTer
	# All values (3x):
	CENTer | MARKer1 | OFF

PmRpointMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PmRpointMode.MANual
	# All values (2x):
	MANual | RIGHt

PowerMeasFunction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerMeasFunction.ACPower
	# All values (7x):
	ACPower | CN | CN0 | CPOWer | MCACpower | OBANdwidth | OBWidth

PowerMeterUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerMeterUnit.DBM
	# All values (3x):
	DBM | W | WATT

PowerSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerSource.VSUPply
	# All values (2x):
	VSUPply | VTUNe

PowerUnitB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PowerUnitB.A
	# Last value:
	value = enums.PowerUnitB.WATT
	# All values (26x):
	A | AMPere | DB | DBM | DBM_hz | DBM_mhz | DBMV | DBMV_mhz
	DBPT | DBPT_mhz | DBPW | DBPW_mhz | DBUA | DBUa_m | DBUa_mhz | DBUa_mmhz
	DBUV | DBUV_m | DBUV_mhz | DBUV_mmhz | PCT | UNITless | V | VOLT
	W | WATT

PowerUnitC
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PowerUnitC.A
	# Last value:
	value = enums.PowerUnitC.WATT
	# All values (28x):
	A | AMPere | DB | DBM | DBM_hz | DBM_mhz | DBMV | DBMV_mhz
	DBPT | DBPT_mhz | DBPW | DBPW_mhz | DBUA | DBUa_m | DBUa_mhz | DBUa_mmhz
	DBUV | DBUV_m | DBUV_mhz | DBUV_mmhz | DEG | HZ | PCT | RAD
	S | UNITless | VOLT | WATT

PowerUnitDdem
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerUnitDdem.DBM
	# All values (3x):
	DBM | DBMV | DBUV

PreampOption
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PreampOption.B23
	# All values (2x):
	B23 | B24

PresetCompatible
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PresetCompatible.MRECeiver
	# All values (8x):
	MRECeiver | MSRA | OFF | PNOise | RECeiver | RTMS | SANalyzer | VNA

Probability
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Probability.P0_01
	# All values (4x):
	P0_01 | P0_1 | P1 | P10

ProbeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ProbeMode.CM
	# All values (4x):
	CM | DM | NM | PM

ProbeSetupMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ProbeSetupMode.NOACtion
	# All values (2x):
	NOACtion | RSINgle

PskFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PskFormat.DIFFerential
	# All values (7x):
	DIFFerential | DPI2 | MNPi2 | N3Pi8 | NORMal | NPI2 | PI8D8psk

PwrLevelMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PwrLevelMode.CURRent
	# All values (2x):
	CURRent | VOLTage

PwrMeasUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PwrMeasUnit.ABS
	# All values (3x):
	ABS | PHZ | PMHZ

QamFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.QamFormat.DIFFerential
	# All values (4x):
	DIFFerential | MNPi4 | NORMal | NPI4

QpskFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.QpskFormat.DIFFerential
	# All values (7x):
	DIFFerential | DPI4 | N3Pi4 | NORMal | NPI4 | OFFSet | SOFFset

RangeClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RangeClass.LOCal
	# All values (3x):
	LOCal | MEDium | WIDE

RangeParam
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RangeParam.ARBW
	# Last value:
	value = enums.RangeParam.TSTR
	# All values (12x):
	ARBW | LOFFset | MFRBw | NFFT | PAValue | PEXCursion | RBW | RFATtenuation
	RLEVel | SNRatio | TSTP | TSTR

RefChannel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RefChannel.LHIGhest
	# All values (3x):
	LHIGhest | MAXimum | MINimum

ReferenceMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReferenceMode.ABSolute
	# All values (2x):
	ABSolute | RELative

ReferenceSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReferenceSource.EXT
	# All values (2x):
	EXT | INT

ReferenceSourceA
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ReferenceSourceA.E10
	# Last value:
	value = enums.ReferenceSourceA.SYNC
	# All values (11x):
	E10 | E100 | E1000 | EAUTo | EXT1 | EXT2 | EXTernal | EXTernal1
	EXTernal2 | INTernal | SYNC

ReferenceSourceB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReferenceSourceB.EAUTo
	# All values (3x):
	EAUTo | EXTernal | INTernal

ReferenceSourceD
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReferenceSourceD.EXT2
	# All values (4x):
	EXT2 | EXT3 | EXTernal | IMMediate

Relay
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Relay.AC_enable
	# Last value:
	value = enums.Relay.SWRF1in
	# All values (84x):
	AC_enable | ACDC | AMPSw_2 | AMPSw_4 | ATT10 | ATT10db | ATT1db | ATT20
	ATT20db | ATT2db | ATT40 | ATT40db | ATT4db_a | ATT4db_b | ATT5 | ATTinput2
	CAL | CAL_enable | EATT | EMIMatt10 | EXT_relais | HP_Bypass | HP_Bypass_2 | HP_Hp100khz
	HP_Hp1khz | HP_Hp1mhz | HP_Hp200khz | HP_Hp20khz | HP_Hp50khz | HP_Hp5mhz | HP_Hp9khz | HP_Sw
	IFSW | INP2matt10r1 | INP2matt10r2 | INPut2 | LFMatt10 | LNA20db_1 | LNA20db_2 | LP_Lp100khz
	LP_Lp14mhz | LP_Lp1mhz | LP_Lp20khz | LP_Lp40mhz | LP_Lp500khz | LP_Lp5mhz | LP_Sw | PREamp
	PREamp30mhz | PRESab_bypr1 | PRESab_bypr2 | PRESab_swir1 | PRESab_swir2 | PRESel | RFAB | SATT10
	SATT20 | SATT40 | SCAL | SIGSourout | SP6T | SPAByp | SPDTinput | SPDTmwcamp
	SWAMp1 | SWAMp1amp2 | SWAMp1toamp4 | SWAMp2 | SWAMp3 | SWAMp3amp4 | SWAMp4 | SWAMp5
	SWAMp6 | SWAMp7 | SWF1f2in | SWF1f2out | SWF1tof4out | SWF3f4out | SWF3in | SWF4in
	SWF5in | SWF6f7in | SWFE | SWRF1in

ResultDevReference
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResultDevReference.CENTer
	# All values (4x):
	CENTer | EDGE | FMSettling | PMSettling

ResultTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResultTypeB.ALL
	# All values (4x):
	ALL | CFACtor | MEAN | PEAK

ResultTypeC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResultTypeC.AVERage
	# All values (2x):
	AVERage | IMMediate

ResultTypeD
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResultTypeD.TOTal
	# All values (1x):
	TOTal

ResultTypeStat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultTypeStat.AVG
	# Last value:
	value = enums.ResultTypeStat.TPEak
	# All values (9x):
	AVG | PAVG | PCTL | PEAK | PPCTl | PSDev | RPEak | SDEV
	TPEak

RoscillatorFreqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscillatorFreqMode.E10
	# All values (3x):
	E10 | E100 | VARiable

RoscillatorRefOut
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscillatorRefOut.EXTernal1
	# All values (7x):
	EXTernal1 | EXTernal2 | O10 | O100 | O1000 | O8000 | OFF

ScaleYaxisUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ScaleYaxisUnit.ABS
	# All values (2x):
	ABS | PCT

ScalingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ScalingMode.LINear
	# All values (2x):
	LINear | LOGarithmic

SearchArea
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SearchArea.MEMory
	# All values (2x):
	MEMory | VISible

SearchRange
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SearchRange.GMAXimum
	# All values (2x):
	GMAXimum | RMAXimum

SelectAll
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelectAll.ALL
	# All values (1x):
	ALL

SelectionRangeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelectionRangeB.ALL
	# All values (2x):
	ALL | CURRent

SelectionScope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelectionScope.ALL
	# All values (2x):
	ALL | SINGle

Separator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Separator.COMMa
	# All values (2x):
	COMMa | POINt

SequencerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SequencerMode.CDEFined
	# All values (4x):
	CDEFined | CONTinous | CONTinuous | SINGle

ServiceBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ServiceBandwidth.BROadband
	# All values (2x):
	BROadband | NARRowband

ServiceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ServiceState.DEViations
	# All values (4x):
	DEViations | NAN | OK | REQired

SidebandPos
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SidebandPos.INVerse
	# All values (2x):
	INVerse | NORMal

SignalLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalLevel.HIGH
	# All values (3x):
	HIGH | LOW | OFF

SignalType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalType.AC
	# All values (3x):
	AC | DC | DCZero

SingleValue
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SingleValue.ALL
	# Last value:
	value = enums.SingleValue.RHO
	# All values (13x):
	ALL | CFER | EVMP | EVMR | FDER | FEP | FERM | IQOF
	MEP | MERM | PEP | PERM | RHO

Size
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Size.LARGe
	# All values (2x):
	LARGe | SMALl

SlopeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlopeType.NEGative
	# All values (2x):
	NEGative | POSitive

SnmpVersion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SnmpVersion.DEFault
	# All values (5x):
	DEFault | OFF | V12 | V123 | V3

Source
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Source.AM
	# Last value:
	value = enums.Source.VIDeo
	# All values (13x):
	AM | FM | FOCus | HVIDeo | IF | IF2 | IQ | OFF
	OUT1 | OUT2 | PM | SSB | VIDeo

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

SpacingY
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpacingY.LDB
	# All values (4x):
	LDB | LINear | LOGarithmic | PERCent

SpanSetting
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpanSetting.FREQuency
	# All values (2x):
	FREQuency | TIME

State
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.State.ALL
	# All values (4x):
	ALL | AUTO | OFF | ON

StatisticMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StatisticMode.INFinite
	# All values (2x):
	INFinite | SONLy

StatisticType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StatisticType.ALL
	# All values (2x):
	ALL | SELected

Stepsize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Stepsize.POINts
	# All values (2x):
	POINts | STANdard

StoreType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StoreType.CHANnel
	# All values (2x):
	CHANnel | INSTrument

SubBlockGaps
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubBlockGaps.AB
	# All values (7x):
	AB | BC | CD | DE | EF | FG | GH

SummaryMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SummaryMode.AVERage
	# All values (2x):
	AVERage | SINGle

SweepModeC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SweepModeC.AUTO
	# All values (3x):
	AUTO | ESPectrum | LIST

SweepOptimize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SweepOptimize.AUTO
	# All values (4x):
	AUTO | DYNamic | SPEed | TRANsient

SweepType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SweepType.AUTO
	# All values (3x):
	AUTO | FFT | SWEep

SymbolSelection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SymbolSelection.ALL
	# All values (3x):
	ALL | DATA | PATTern

Synchronization
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Synchronization.ALL
	# All values (2x):
	ALL | NONE

SyncMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncMode.MEAS
	# All values (2x):
	MEAS | SYNC

SystemStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SystemStatus.ERR
	# All values (3x):
	ERR | OK | WARN

TechnologyStandardA
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TechnologyStandardA.GSM
	# Last value:
	value = enums.TechnologyStandardA.WCDMa
	# All values (28x):
	GSM | LTE_1_40 | LTE_10_00 | LTE_15_00 | LTE_20_00 | LTE_3_00 | LTE_5_00 | NR5G_fr1_10
	NR5G_fr1_100 | NR5G_fr1_15 | NR5G_fr1_20 | NR5G_fr1_25 | NR5G_fr1_30 | NR5G_fr1_35 | NR5G_fr1_40 | NR5G_fr1_45
	NR5G_fr1_5 | NR5G_fr1_50 | NR5G_fr1_60 | NR5G_fr1_70 | NR5G_fr1_80 | NR5G_fr1_90 | NR5G_fr2_100 | NR5G_fr2_200
	NR5G_fr2_400 | NR5G_fr2_50 | USER | WCDMa

TechnologyStandardB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TechnologyStandardB.AWLan
	# Last value:
	value = enums.TechnologyStandardB.WIMax
	# All values (60x):
	AWLan | BWLan | CDPD | D2CDma | EUTRa | F19Cdma | F1D100nr5g | F1D20nr5g
	F1U100nr5g | F1U20nr5g | F2D100nr5g | F2D200nr5g | F2U100nr5g | F2U200nr5g | F8CDma | FIS95a
	FIS95c0 | FIS95c1 | FJ008 | FTCDma | FW3Gppcdma | FWCDma | GSM | L03R
	L03S | L05R | L05S | L10R | L10S | L14R | L14S | L15R
	L15S | L20R | L20S | M2CDma | MSR | NONE | NR5G | NR5Glte
	PAPCo25 | PDC | PHS | R19Cdma | R8CDma | REUTra | RFID14443 | RIS95a
	RIS95c0 | RIS95c1 | RJ008 | RTCDma | RW3Gppcdma | RWCDma | S2CDma | TCDMa
	TETRa | USER | WIBRo | WIMax

TechnologyStandardDdem
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TechnologyStandardDdem.DECT
	# All values (3x):
	DECT | EDGE | GSM

Temperature
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Temperature.COLD
	# All values (2x):
	COLD | HOT

TimeFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeFormat.DE
	# All values (3x):
	DE | ISO | US

TimeLimitUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeLimitUnit.S
	# All values (2x):
	S | SYM

TimeOrder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeOrder.AFTer
	# All values (2x):
	AFTer | BEFore

TouchscreenState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TouchscreenState.FRAMe
	# All values (3x):
	FRAMe | OFF | ON

TraceFormat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TraceFormat.BERate
	# Last value:
	value = enums.TraceFormat.UPHase
	# All values (22x):
	BERate | BINary | COMP | CONF | CONS | COVF | DECimal | FEYE
	FREQuency | GDELay | HEXadecimal | IEYE | MAGNitude | MOVerview | NONE | OCTal
	PHASe | QEYE | RCONstell | RIMag | RSUMmary | UPHase

TraceModeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceModeA.AVERage
	# All values (6x):
	AVERage | MAXHold | MINHold | OFF | VIEW | WRITe

TraceModeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceModeB.AVERage
	# All values (4x):
	AVERage | MAXHold | MINHold | WRITe

TraceModeC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceModeC.AVERage
	# All values (6x):
	AVERage | BLANk | MAXHold | MINHold | VIEW | WRITe

TraceModeD
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceModeD.MAXHold
	# All values (2x):
	MAXHold | WRITe

TraceModeE
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceModeE.AVERage
	# All values (3x):
	AVERage | MAXHold | WRITe

TraceModeF
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceModeF.AVERage
	# All values (7x):
	AVERage | BLANk | DENSity | MAXHold | MINHold | VIEW | WRITe

TraceModeH
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceModeH.BLANk
	# All values (3x):
	BLANk | VIEW | WRITe

TraceNumber
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TraceNumber.BTOBits
	# Last value:
	value = enums.TraceNumber.TRACe6
	# All values (15x):
	BTOBits | BTOFm | ESPLine | HMAXhold | LIST | PSPectrum | SGRam | SPECtrogram
	SPURious | TRACe1 | TRACe2 | TRACe3 | TRACe4 | TRACe5 | TRACe6

TracePresetType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TracePresetType.ALL
	# All values (3x):
	ALL | MAM | MCM

TraceReference
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceReference.BURSt
	# All values (3x):
	BURSt | PATTern | TRIGger

TraceRefType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceRefType.ERRor
	# All values (4x):
	ERRor | MEAS | REF | TCAP

TraceSymbols
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceSymbols.BARS
	# All values (4x):
	BARS | DOTS | OFF | ON

TraceTypeDdem
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TraceTypeDdem.MSTRace
	# Last value:
	value = enums.TraceTypeDdem.TRACe6
	# All values (15x):
	MSTRace | PSTRace | STRace | TRACe1 | TRACe1i | TRACe1r | TRACe2 | TRACe2i
	TRACe2r | TRACe3 | TRACe3i | TRACe3r | TRACe4 | TRACe5 | TRACe6

TraceTypeK30
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceTypeK30.TRACe1
	# All values (4x):
	TRACe1 | TRACe2 | TRACe3 | TRACe4

TraceTypeK60
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceTypeK60.SGRam
	# All values (8x):
	SGRam | SPECtrogram | TRACe1 | TRACe2 | TRACe3 | TRACe4 | TRACe5 | TRACe6

TraceTypeList
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceTypeList.LIST
	# All values (7x):
	LIST | TRACe1 | TRACe2 | TRACe3 | TRACe4 | TRACe5 | TRACe6

TraceTypeNumeric
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceTypeNumeric.TRACe1
	# All values (6x):
	TRACe1 | TRACe2 | TRACe3 | TRACe4 | TRACe5 | TRACe6

TriggerOutType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerOutType.DEVice
	# All values (3x):
	DEVice | TARMed | UDEFined

TriggerPort
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerPort.EXT1
	# All values (4x):
	EXT1 | EXT2 | HOST | OFF

TriggerSeqSource
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TriggerSeqSource.ACVideo
	# Last value:
	value = enums.TriggerSeqSource.VIDeo
	# All values (40x):
	ACVideo | AF | AM | AMRelative | BBPower | EXT2 | EXT3 | EXT4
	EXTernal | FM | GP0 | GP1 | GP2 | GP3 | GP4 | GP5
	IFPower | IMMediate | INTernal | IQPower | LXI | MAGNitude | MAIT | MANual
	MASK | PM | PMTRigger | PSENsor | RFPower | SLEFt | SMONo | SMPX
	SPILot | SRDS | SRIGht | SSTereo | TDTRigger | TIME | TV | VIDeo

TriggerSourceB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TriggerSourceB.ACVideo
	# Last value:
	value = enums.TriggerSourceB.TV
	# All values (30x):
	ACVideo | AF | AM | AMRelative | BBPower | EXT2 | EXT3 | EXT4
	EXTernal | FM | GP0 | GP1 | GP2 | GP3 | GP4 | GP5
	IFPower | IMMediate | IQPower | MAGNitude | PM | RFPower | SLEFt | SMONo
	SMPX | SPILot | SRDS | SRIGht | SSTereo | TV

TriggerSourceD
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TriggerSourceD.EXT2
	# Last value:
	value = enums.TriggerSourceD.VIDeo
	# All values (10x):
	EXT2 | EXT3 | EXT4 | EXTernal | IFPower | IMMediate | IQPower | PSENsor
	RFPower | VIDeo

TriggerSourceK
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TriggerSourceK.EXT2
	# Last value:
	value = enums.TriggerSourceK.TIME
	# All values (9x):
	EXT2 | EXT3 | EXTernal | IFPower | IMMediate | IQPower | MANual | RFPower
	TIME

TriggerSourceL
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSourceL.EXT2
	# All values (8x):
	EXT2 | EXT3 | EXTernal | IFPower | IMMediate | IQPower | RFPower | TIME

TriggerSourceListPower
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TriggerSourceListPower.EXT2
	# Last value:
	value = enums.TriggerSourceListPower.VIDeo
	# All values (10x):
	EXT2 | EXT3 | EXT4 | EXTernal | IFPower | IMMediate | LINE | LXI
	RFPower | VIDeo

TriggerSourceMpower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSourceMpower.EXT2
	# All values (7x):
	EXT2 | EXT3 | EXT4 | EXTernal | IFPower | RFPower | VIDeo

TuningRange
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TuningRange.SMALl
	# All values (2x):
	SMALl | WIDE

UnitMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitMode.DB
	# All values (2x):
	DB | PCT

UpDownDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpDownDirection.DOWN
	# All values (2x):
	DOWN | UP

UserLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UserLevel.AUTH
	# All values (3x):
	AUTH | NOAuth | PRIV

WindowDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WindowDirection.ABOVe
	# All values (4x):
	ABOVe | BELow | LEFT | RIGHt

WindowDirReplace
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WindowDirReplace.ABOVe
	# All values (5x):
	ABOVe | BELow | LEFT | REPLace | RIGHt

WindowName
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WindowName.FOCus
	# All values (1x):
	FOCus

WindowTypeBase
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WindowTypeBase.Diagram=DIAGram
	# All values (5x):
	Diagram | MarkePeakList | MarkeTable | ResultSummary | Spectrogram

WindowTypeIq
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.WindowTypeIq.Diagram=DIAGram
	# Last value:
	value = enums.WindowTypeIq.Spectrum=FREQ
	# All values (9x):
	Diagram | IqImagReal | IqVector | Magnitude | MarkePeakList | MarkeTable | ResultSummary | Spectrogram
	Spectrum

WindowTypeK30
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.WindowTypeK30.CalPowerCold=CPCold
	# Last value:
	value = enums.WindowTypeK30.YfactorResult=YFACtor
	# All values (12x):
	CalPowerCold | CalPowerHot | CalYfactor | EnrMeasured | GainResult | MarkerTable | NoiseFigureResult | NoiseTemperatureResult
	PowerColdResult | PowerHotResult | ResultTable | YfactorResult

WindowTypeK40
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.WindowTypeK40.FrequencyDrift=FDRift
	# Last value:
	value = enums.WindowTypeK40.SweepResultList=SRESults
	# All values (9x):
	FrequencyDrift | MarkerTable | PhaseNoiseDiagram | ResidualNoiseTable | SpectrumMonitor | SpotNoiseTable | SpurList | StabilityFreqLevel
	SweepResultList

WindowTypeK50
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WindowTypeK50.MarkerTable=MTABle
	# All values (5x):
	MarkerTable | NoiseFloorEstimate | SpectralOverview | SpurDetectionSpectrum | SpurDetectionTable

WindowTypeK60
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.WindowTypeK60.ChirpRateTimeDomain=CRTime
	# Last value:
	value = enums.WindowTypeK60.StatisticsTable=STABle
	# All values (15x):
	ChirpRateTimeDomain | FmTimeDomain | FreqDeviationTimeDomain | IqTimeDomain | MarkerTable | ParameterDistribution | ParameterTrend | PhaseDeviationTimeDomain
	PmTimeDomain | PMTimeDomainWrapped | ResultsTable | RfPowerTimeDomain | RfSpectrum | Spectrogram | StatisticsTable

WindowTypeK7
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.WindowTypeK7.AmSpectrum='XTIM:AM:RELative:AFSPectrum'
	# Last value:
	value = enums.WindowTypeK7.RfTimeDomain='XTIM:AM'
	# All values (11x):
	AmSpectrum | AmTimeDomain | FmSpectrum | FmTimeDomain | MarkerPeakList | MarkerTable | PmSpectrum | PmTimeDomain
	ResultSummary | RfSpectrum | RfTimeDomain

WindowTypeK70
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.WindowTypeK70.CaptureBufferMagnAbs=CBUFfer
	# Last value:
	value = enums.WindowTypeK70.SymbolsHexa=SYMB
	# All values (9x):
	CaptureBufferMagnAbs | Equalizer | ErrorVectorMagnitude | MeasMagnRel | ModulationAccuracy | ModulationErrors | MultiSource | RefMagnRel
	SymbolsHexa

Xdistribution
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Xdistribution.BINCentered
	# All values (2x):
	BINCentered | STARtstop

XyDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.XyDirection.HORizontal
	# All values (2x):
	HORizontal | VERTical


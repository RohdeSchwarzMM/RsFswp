Center
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:AF:CENTer

.. code-block:: python

	SENSe:ADEMod:AF:CENTer



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Af.Center.CenterCls
	:members:
	:undoc-members:
	:noindex:
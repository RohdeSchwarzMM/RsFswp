State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:UPORt:STATe

.. code-block:: python

	OUTPut<OutputConnector>:UPORt:STATe



.. autoclass:: RsFswp.Implementations.Output.Uport.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:FLISt<FileList>:CATalog

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:FLISt<FileList>:CATalog



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Flist.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:
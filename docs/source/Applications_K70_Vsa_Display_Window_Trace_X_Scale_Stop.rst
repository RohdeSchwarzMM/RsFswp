Stop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:X:SCALe:STOP

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:X:SCALe:STOP



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Display.Window.Trace.X.Scale.Stop.StopCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:STATe

.. code-block:: python

	SENSe:PMETer<PowerMeter>:STATe



.. autoclass:: RsFswp.Implementations.Sense.Pmeter.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:IQ:BWIDth:MODE

.. code-block:: python

	SENSe:IQ:BWIDth:MODE



.. autoclass:: RsFswp.Implementations.Sense.Iq.Bandwidth.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
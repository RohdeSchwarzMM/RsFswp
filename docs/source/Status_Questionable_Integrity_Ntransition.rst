Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INTegrity:NTRansition

.. code-block:: python

	STATus:QUEStionable:INTegrity:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Integrity.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
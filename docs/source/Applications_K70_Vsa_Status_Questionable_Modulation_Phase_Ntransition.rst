Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:MODulation<Window>:PHASe:NTRansition

.. code-block:: python

	STATus:QUEStionable:MODulation<Window>:PHASe:NTRansition



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Status.Questionable.Modulation.Phase.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
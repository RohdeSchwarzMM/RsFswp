Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:IMMediate

.. code-block:: python

	INITiate:IMMediate



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Initiate.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:Y:SCALe:AUTO

.. code-block:: python

	DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:Y:SCALe:AUTO



.. autoclass:: RsFswp.Implementations.Display.Window.Subwindow.Trace.Y.Scale.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
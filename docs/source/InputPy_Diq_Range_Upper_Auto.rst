Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:DIQ:RANGe:UPPer:AUTO

.. code-block:: python

	INPut:DIQ:RANGe:UPPer:AUTO



.. autoclass:: RsFswp.Implementations.InputPy.Diq.Range.Upper.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
Connector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:CONNector

.. code-block:: python

	INPut:CONNector



.. autoclass:: RsFswp.Implementations.InputPy.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex:
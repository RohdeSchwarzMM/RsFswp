Tables
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:INTernal:COMMand:TABLes

.. code-block:: python

	SYSTem:COMMunicate:INTernal:COMMand:TABLes



.. autoclass:: RsFswp.Implementations.System.Communicate.Internal.Command.Tables.TablesCls
	:members:
	:undoc-members:
	:noindex:
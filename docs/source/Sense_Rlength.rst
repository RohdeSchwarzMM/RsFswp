Rlength
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:RLENgth

.. code-block:: python

	SENSe:RLENgth



.. autoclass:: RsFswp.Implementations.Sense.Rlength.RlengthCls
	:members:
	:undoc-members:
	:noindex:
Pexcursion
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:PEXCursion

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:PEXCursion



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Pexcursion.PexcursionCls
	:members:
	:undoc-members:
	:noindex:
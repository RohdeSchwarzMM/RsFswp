Hspeed
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:HSPeed

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:HSPeed



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Hspeed.HspeedCls
	:members:
	:undoc-members:
	:noindex:
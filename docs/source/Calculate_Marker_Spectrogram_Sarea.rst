Sarea
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:SPECtrogram:SARea

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:SPECtrogram:SARea



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Spectrogram.Sarea.SareaCls
	:members:
	:undoc-members:
	:noindex:
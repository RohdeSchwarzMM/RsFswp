State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FILTer<FilterPy>:CCIR:WEIGhted:STATe

.. code-block:: python

	SENSe:FILTer<FilterPy>:CCIR:WEIGhted:STATe



.. autoclass:: RsFswp.Implementations.Sense.FilterPy.Ccir.Weighted.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Trace<Trace>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Tr1 .. Tr16
	rc = driver.applications.k70Vsa.calculate.trace.repcap_trace_get()
	driver.applications.k70Vsa.calculate.trace.repcap_trace_set(repcap.Trace.Tr1)





.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.calculate.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_Calculate_Trace_Adjust.rst
	Applications_K70_Vsa_Calculate_Trace_Symbols.rst
	Applications_K70_Vsa_Calculate_Trace_Value.rst
Spurious
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:SPURious

.. code-block:: python

	INITiate:SPURious



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Initiate.Spurious.SpuriousCls
	:members:
	:undoc-members:
	:noindex:
Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMat:DATA

.. code-block:: python

	FORMat:DATA



.. autoclass:: RsFswp.Implementations.FormatPy.Data.DataCls
	:members:
	:undoc-members:
	:noindex:
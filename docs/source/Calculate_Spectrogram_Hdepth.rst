Hdepth
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:SPECtrogram:HDEPth

.. code-block:: python

	CALCulate<Window>:SPECtrogram:HDEPth



.. autoclass:: RsFswp.Implementations.Calculate.Spectrogram.Hdepth.HdepthCls
	:members:
	:undoc-members:
	:noindex:
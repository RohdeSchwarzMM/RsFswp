ScData
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:SELect:CHANnel:ITEM:SCData

.. code-block:: python

	MMEMory:SELect:CHANnel:ITEM:SCData



.. autoclass:: RsFswp.Implementations.MassMemory.Select.Channel.Item.ScData.ScDataCls
	:members:
	:undoc-members:
	:noindex:
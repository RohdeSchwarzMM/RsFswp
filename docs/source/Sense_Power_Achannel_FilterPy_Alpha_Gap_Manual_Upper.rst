Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:FILTer:ALPHa:GAP<GapChannel>:MANual:UPPer

.. code-block:: python

	SENSe:POWer:ACHannel:FILTer:ALPHa:GAP<GapChannel>:MANual:UPPer



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.FilterPy.Alpha.Gap.Manual.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex:
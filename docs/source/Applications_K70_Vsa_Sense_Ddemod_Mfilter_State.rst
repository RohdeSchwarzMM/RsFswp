State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:MFILter:STATe

.. code-block:: python

	SENSe:DDEMod:MFILter:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Mfilter.State.StateCls
	:members:
	:undoc-members:
	:noindex:
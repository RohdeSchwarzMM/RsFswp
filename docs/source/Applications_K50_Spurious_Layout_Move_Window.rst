Window
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:MOVE:WINDow

.. code-block:: python

	LAYout:MOVE:WINDow



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Layout.Move.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:
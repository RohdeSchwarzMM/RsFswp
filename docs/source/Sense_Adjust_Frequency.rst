Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:FREQuency

.. code-block:: python

	SENSe:ADJust:FREQuency



.. autoclass:: RsFswp.Implementations.Sense.Adjust.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:
Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:AVERage:COUNt

.. code-block:: python

	TRACe:IQ:AVERage:COUNt



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.Average.Count.CountCls
	:members:
	:undoc-members:
	:noindex:
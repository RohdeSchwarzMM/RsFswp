Pdefined
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:CMAP<Item>:PDEFined

.. code-block:: python

	DISPlay:CMAP<Item>:PDEFined



.. autoclass:: RsFswp.Implementations.Display.Cmap.Pdefined.PdefinedCls
	:members:
	:undoc-members:
	:noindex:
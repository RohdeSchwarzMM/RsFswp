Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ESPectrum<SubBlock>:MODE

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ESPectrum<SubBlock>:MODE



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Espectrum.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
Name
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PLUGin:APPStarter:NAME

.. code-block:: python

	SYSTem:PLUGin:APPStarter:NAME



.. autoclass:: RsFswp.Implementations.System.Plugin.AppStarter.Name.NameCls
	:members:
	:undoc-members:
	:noindex:
Text
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:ITEM:HEADer:LINE<Line>:TEXT

.. code-block:: python

	HCOPy:TREPort:ITEM:HEADer:LINE<Line>:TEXT



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Item.Header.Line.Text.TextCls
	:members:
	:undoc-members:
	:noindex:
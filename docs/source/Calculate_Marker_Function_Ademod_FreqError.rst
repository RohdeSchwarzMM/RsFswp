FreqError
----------------------------------------





.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Ademod.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.marker.function.ademod.freqError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Marker_Function_Ademod_FreqError_Result.rst
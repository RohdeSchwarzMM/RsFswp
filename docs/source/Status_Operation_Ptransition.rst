Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:PTRansition

.. code-block:: python

	STATus:OPERation:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Operation.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:GENerator:RLEVel:STATe

.. code-block:: python

	INSTrument:COUPle:GENerator:RLEVel:STATe



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Generator.RefLevel.State.StateCls
	:members:
	:undoc-members:
	:noindex:
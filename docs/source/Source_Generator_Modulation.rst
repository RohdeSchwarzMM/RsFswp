Modulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:GENerator:MODulation

.. code-block:: python

	SOURce:GENerator:MODulation



.. autoclass:: RsFswp.Implementations.Source.Generator.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:
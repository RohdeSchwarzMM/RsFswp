Presel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:PRESel

.. code-block:: python

	INSTrument:COUPle:PRESel



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Presel.PreselCls
	:members:
	:undoc-members:
	:noindex:
TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:STORe:TYPE

.. code-block:: python

	MMEMory:STORe:TYPE



.. autoclass:: RsFswp.Implementations.MassMemory.Store.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
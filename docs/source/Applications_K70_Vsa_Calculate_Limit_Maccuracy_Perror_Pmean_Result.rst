Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:PERRor:PMEan:RESult

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:PERRor:PMEan:RESult



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Perror.Pmean.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
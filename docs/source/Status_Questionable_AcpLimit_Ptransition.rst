Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:ACPLimit:PTRansition

.. code-block:: python

	STATus:QUEStionable:ACPLimit:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.AcpLimit.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
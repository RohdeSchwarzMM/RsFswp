Reference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:ADEMod:RESults:FM:DETector<Trace>:REFerence

.. code-block:: python

	CONFigure:ADEMod:RESults:FM:DETector<Trace>:REFerence



.. autoclass:: RsFswp.Implementations.Configure.Ademod.Results.Fm.Detector.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ademod.results.fm.detector.reference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ademod_Results_Fm_Detector_Reference_MeastoRef.rst
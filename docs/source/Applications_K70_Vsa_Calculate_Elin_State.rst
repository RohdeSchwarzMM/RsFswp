State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:ELIN:STATe

.. code-block:: python

	CALCulate<Window>:ELIN:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Elin.State.StateCls
	:members:
	:undoc-members:
	:noindex:
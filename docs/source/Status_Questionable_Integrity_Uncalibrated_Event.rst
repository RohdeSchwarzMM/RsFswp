Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INTegrity:UNCalibrated:EVENt

.. code-block:: python

	STATus:QUEStionable:INTegrity:UNCalibrated:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Integrity.Uncalibrated.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:MINFo:STATe

.. code-block:: python

	DISPlay:MINFo:STATe



.. autoclass:: RsFswp.Implementations.Display.Minfo.State.StateCls
	:members:
	:undoc-members:
	:noindex:
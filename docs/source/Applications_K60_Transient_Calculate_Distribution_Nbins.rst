Nbins
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DISTribution:NBINs

.. code-block:: python

	CALCulate<Window>:DISTribution:NBINs



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.Distribution.Nbins.NbinsCls
	:members:
	:undoc-members:
	:noindex:
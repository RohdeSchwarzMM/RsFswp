Cpresent
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:MSR:GSM:CPResent

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:MSR:GSM:CPResent



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Msr.Gsm.Cpresent.CpresentCls
	:members:
	:undoc-members:
	:noindex:
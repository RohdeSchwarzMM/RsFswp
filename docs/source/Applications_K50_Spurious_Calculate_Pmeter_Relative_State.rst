State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:PMETer<PowerMeter>:RELative:STATe

.. code-block:: python

	CALCulate<Window>:PMETer<PowerMeter>:RELative:STATe



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Pmeter.Relative.State.StateCls
	:members:
	:undoc-members:
	:noindex:
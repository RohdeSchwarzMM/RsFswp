Ports
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:PORTs

.. code-block:: python

	SENSe:MIXer:PORTs



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Sense.Mixer.Ports.PortsCls
	:members:
	:undoc-members:
	:noindex:
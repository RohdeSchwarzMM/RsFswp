Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:EVM:PPEak:RESult

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:EVM:PPEak:RESult



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Evm.Ppeak.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
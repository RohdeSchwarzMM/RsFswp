Pstate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:PSTate

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:PSTate



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Pstate.PstateCls
	:members:
	:undoc-members:
	:noindex:
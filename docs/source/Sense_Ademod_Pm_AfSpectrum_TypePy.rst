TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:PM:AFSPectrum:TYPE

.. code-block:: python

	SENSe:ADEMod:PM:AFSPectrum:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Pm.AfSpectrum.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
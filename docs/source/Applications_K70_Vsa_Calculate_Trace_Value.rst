Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:TRACe<Trace>:VALue

.. code-block:: python

	CALCulate<Window>:TRACe<Trace>:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Trace.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
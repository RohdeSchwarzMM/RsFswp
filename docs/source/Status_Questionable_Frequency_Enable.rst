Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:FREQuency:ENABle

.. code-block:: python

	STATus:QUEStionable:FREQuency:ENABle



.. autoclass:: RsFswp.Implementations.Status.Questionable.Frequency.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:
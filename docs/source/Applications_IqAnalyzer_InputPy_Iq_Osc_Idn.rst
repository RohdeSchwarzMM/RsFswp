Idn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:IQ:OSC:IDN

.. code-block:: python

	INPut<InputIx>:IQ:OSC:IDN



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.InputPy.Iq.Osc.Idn.IdnCls
	:members:
	:undoc-members:
	:noindex:
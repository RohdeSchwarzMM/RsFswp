Long
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:CATalog:LONG

.. code-block:: python

	MMEMory:CATalog:LONG



.. autoclass:: RsFswp.Implementations.MassMemory.Catalog.Long.LongCls
	:members:
	:undoc-members:
	:noindex:
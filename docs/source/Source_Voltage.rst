Voltage
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Voltage.VoltageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.voltage.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Voltage_Auxiliary.rst
	Source_Voltage_Channel.rst
	Source_Voltage_Control.rst
	Source_Voltage_Power.rst
	Source_Voltage_Sequence.rst
	Source_Voltage_State.rst
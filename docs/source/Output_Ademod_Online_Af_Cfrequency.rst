Cfrequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:ADEMod:ONLine:AF:CFRequency

.. code-block:: python

	OUTPut<OutputConnector>:ADEMod:ONLine:AF:CFRequency



.. autoclass:: RsFswp.Implementations.Output.Ademod.Online.Af.Cfrequency.CfrequencyCls
	:members:
	:undoc-members:
	:noindex:
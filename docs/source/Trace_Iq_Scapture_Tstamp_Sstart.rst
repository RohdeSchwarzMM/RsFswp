Sstart
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe<Window>:IQ:SCAPture:TSTamp:SSTart

.. code-block:: python

	TRACe<Window>:IQ:SCAPture:TSTamp:SSTart



.. autoclass:: RsFswp.Implementations.Trace.Iq.Scapture.Tstamp.Sstart.SstartCls
	:members:
	:undoc-members:
	:noindex:
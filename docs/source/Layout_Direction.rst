Direction
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:DIRection

.. code-block:: python

	LAYout:DIRection



.. autoclass:: RsFswp.Implementations.Layout.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex:
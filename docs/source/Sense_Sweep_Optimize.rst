Optimize
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:OPTimize

.. code-block:: python

	SENSe:SWEep:OPTimize



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Optimize.OptimizeCls
	:members:
	:undoc-members:
	:noindex:
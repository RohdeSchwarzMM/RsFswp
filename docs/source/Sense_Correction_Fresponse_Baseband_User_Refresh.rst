Refresh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:BASeband:USER:REFResh

.. code-block:: python

	SENSe:CORRection:FRESponse:BASeband:USER:REFResh



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.Baseband.User.Refresh.RefreshCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:WBANd:STATe

.. code-block:: python

	TRACe:IQ:WBANd:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Trace.Iq.Wband.State.StateCls
	:members:
	:undoc-members:
	:noindex:
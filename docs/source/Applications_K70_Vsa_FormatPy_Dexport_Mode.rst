Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMat:DEXPort:MODE

.. code-block:: python

	FORMat:DEXPort:MODE



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.FormatPy.Dexport.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
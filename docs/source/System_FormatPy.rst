FormatPy
----------------------------------------





.. autoclass:: RsFswp.Implementations.System.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.formatPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_FormatPy_Ident.rst
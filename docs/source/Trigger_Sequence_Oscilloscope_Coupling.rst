Coupling
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:OSCilloscope:COUPling

.. code-block:: python

	TRIGger:SEQuence:OSCilloscope:COUPling



.. autoclass:: RsFswp.Implementations.Trigger.Sequence.Oscilloscope.Coupling.CouplingCls
	:members:
	:undoc-members:
	:noindex:
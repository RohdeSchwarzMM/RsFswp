Ptransistion
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:PCALibration:PTRansistion

.. code-block:: python

	STATus:OPERation:PCALibration:PTRansistion



.. autoclass:: RsFswp.Implementations.Status.Operation.Pcalibration.Ptransistion.PtransistionCls
	:members:
	:undoc-members:
	:noindex:
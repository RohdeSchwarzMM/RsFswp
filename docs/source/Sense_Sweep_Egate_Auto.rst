Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:AUTO

.. code-block:: python

	SENSe:SWEep:EGATe:AUTO



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Egate.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
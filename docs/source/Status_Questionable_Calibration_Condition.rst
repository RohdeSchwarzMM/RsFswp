Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:CALibration:CONDition

.. code-block:: python

	STATus:QUEStionable:CALibration:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Calibration.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
Optimization
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:OPTimization

.. code-block:: python

	SENSe:DDEMod:OPTimization



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Optimization.OptimizationCls
	:members:
	:undoc-members:
	:noindex:
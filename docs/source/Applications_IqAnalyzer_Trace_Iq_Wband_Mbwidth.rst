Mbwidth
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:WBANd:MBWidth

.. code-block:: python

	TRACe:IQ:WBANd:MBWidth



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.Wband.Mbwidth.MbwidthCls
	:members:
	:undoc-members:
	:noindex:
Osc
----------------------------------------





.. autoclass:: RsFswp.Implementations.InputPy.Iq.Osc.OscCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputPy.iq.osc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputPy_Iq_Osc_Balanced.rst
	InputPy_Iq_Osc_Fullscale.rst
	InputPy_Iq_Osc_Skew.rst
	InputPy_Iq_Osc_State.rst
	InputPy_Iq_Osc_SymbolRate.rst
	InputPy_Iq_Osc_Tcpip.rst
	InputPy_Iq_Osc_TypePy.rst
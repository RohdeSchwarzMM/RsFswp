Rterminator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:GPIB:SELF:RTERminator

.. code-block:: python

	SYSTem:COMMunicate:GPIB:SELF:RTERminator



.. autoclass:: RsFswp.Implementations.System.Communicate.Gpib.Self.Rterminator.RterminatorCls
	:members:
	:undoc-members:
	:noindex:
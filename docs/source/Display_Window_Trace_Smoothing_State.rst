State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:SMOothing:STATe

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:SMOothing:STATe



.. autoclass:: RsFswp.Implementations.Display.Window.Trace.Smoothing.State.StateCls
	:members:
	:undoc-members:
	:noindex:
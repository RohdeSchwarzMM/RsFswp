State<Status>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.sense.average.state.repcap_status_get()
	driver.sense.average.state.repcap_status_set(repcap.Status.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:AVERage:STATe<Status>

.. code-block:: python

	SENSe:AVERage:STATe<Status>



.. autoclass:: RsFswp.Implementations.Sense.Average.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.average.state.clone()
Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:LOSS:CALibration:MODE

.. code-block:: python

	SENSe:CORRection:LOSS:CALibration:MODE



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Loss.Calibration.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
Patterns
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DSP:RESult:CAPTure:PATTerns

.. code-block:: python

	CALCulate<Window>:DSP:RESult:CAPTure:PATTerns



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Dsp.Result.Capture.Patterns.PatternsCls
	:members:
	:undoc-members:
	:noindex:
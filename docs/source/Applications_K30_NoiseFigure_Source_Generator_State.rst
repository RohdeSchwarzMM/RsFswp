State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:GENerator:STATe

.. code-block:: python

	SOURce:GENerator:STATe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Source.Generator.State.StateCls
	:members:
	:undoc-members:
	:noindex:
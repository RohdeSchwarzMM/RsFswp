State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:HUMS:STATe

.. code-block:: python

	DIAGnostic:HUMS:STATe



.. autoclass:: RsFswp.Implementations.Diagnostic.Hums.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Result<Trace>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Tr1 .. Tr16
	rc = driver.calculate.marker.function.ademod.sinad.result.repcap_trace_get()
	driver.calculate.marker.function.ademod.sinad.result.repcap_trace_set(repcap.Trace.Tr1)



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:ADEMod:SINad:RESult<Trace>

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:ADEMod:SINad:RESult<Trace>



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Ademod.Sinad.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.marker.function.ademod.sinad.result.clone()
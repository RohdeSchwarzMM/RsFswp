Espectrum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:ESPectrum

.. code-block:: python

	INITiate:ESPectrum



.. autoclass:: RsFswp.Implementations.Initiate.Espectrum.EspectrumCls
	:members:
	:undoc-members:
	:noindex:
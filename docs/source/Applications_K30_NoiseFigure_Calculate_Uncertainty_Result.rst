Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:RESult

.. code-block:: python

	CALCulate<Window>:UNCertainty:RESult



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
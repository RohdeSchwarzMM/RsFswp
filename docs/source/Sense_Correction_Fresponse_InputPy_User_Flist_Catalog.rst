Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:INPut<InputIx>:USER:FLISt<FileList>:CATalog

.. code-block:: python

	SENSe:CORRection:FRESponse:INPut<InputIx>:USER:FLISt<FileList>:CATalog



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.InputPy.User.Flist.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:
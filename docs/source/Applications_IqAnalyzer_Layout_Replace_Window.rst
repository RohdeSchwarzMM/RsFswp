Window
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:REPLace:WINDow

.. code-block:: python

	LAYout:REPLace:WINDow



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Layout.Replace.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:
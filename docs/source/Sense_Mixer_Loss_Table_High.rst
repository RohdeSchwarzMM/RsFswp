High
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:LOSS:TABLe:HIGH

.. code-block:: python

	SENSe:MIXer:LOSS:TABLe:HIGH



.. autoclass:: RsFswp.Implementations.Sense.Mixer.Loss.Table.High.HighCls
	:members:
	:undoc-members:
	:noindex:
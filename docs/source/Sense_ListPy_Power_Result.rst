Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:POWer:RESult

.. code-block:: python

	SENSe:LIST:POWer:RESult



.. autoclass:: RsFswp.Implementations.Sense.ListPy.Power.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
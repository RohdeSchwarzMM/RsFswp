Marker<Marker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.applications.k40PhaseNoise.calculate.marker.repcap_marker_get()
	driver.applications.k40PhaseNoise.calculate.marker.repcap_marker_set(repcap.Marker.Nr1)





.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Calculate.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k40PhaseNoise.calculate.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K40_PhaseNoise_Calculate_Marker_Y.rst
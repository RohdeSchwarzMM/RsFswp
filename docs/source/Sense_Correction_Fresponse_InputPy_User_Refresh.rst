Refresh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:INPut<InputIx>:USER:REFResh

.. code-block:: python

	SENSe:CORRection:FRESponse:INPut<InputIx>:USER:REFResh



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.InputPy.User.Refresh.RefreshCls
	:members:
	:undoc-members:
	:noindex:
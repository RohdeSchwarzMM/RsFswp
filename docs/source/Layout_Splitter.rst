Splitter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:SPLitter

.. code-block:: python

	LAYout:SPLitter



.. autoclass:: RsFswp.Implementations.Layout.Splitter.SplitterCls
	:members:
	:undoc-members:
	:noindex:
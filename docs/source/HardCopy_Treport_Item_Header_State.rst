State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:ITEM:HEADer:STATe

.. code-block:: python

	HCOPy:TREPort:ITEM:HEADer:STATe



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Item.Header.State.StateCls
	:members:
	:undoc-members:
	:noindex:
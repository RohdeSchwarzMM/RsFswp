Stype
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SSEarch:STYPe

.. code-block:: python

	SENSe:SSEarch:STYPe



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Ssearch.Stype.StypeCls
	:members:
	:undoc-members:
	:noindex:
Offset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:GENerator:RLEVel:OFFSet

.. code-block:: python

	INSTrument:COUPle:GENerator:RLEVel:OFFSet



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Generator.RefLevel.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:
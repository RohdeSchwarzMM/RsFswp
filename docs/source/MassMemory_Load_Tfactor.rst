Tfactor
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:LOAD<Window>:TFACtor

.. code-block:: python

	MMEMory:LOAD<Window>:TFACtor



.. autoclass:: RsFswp.Implementations.MassMemory.Load.Tfactor.TfactorCls
	:members:
	:undoc-members:
	:noindex:
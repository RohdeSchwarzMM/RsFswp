Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:LMARgin<Window>:PTRansition

.. code-block:: python

	STATus:QUEStionable:LMARgin<Window>:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Lmargin.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
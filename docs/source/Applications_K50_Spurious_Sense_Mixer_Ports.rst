Ports
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MIXer:PORTs

.. code-block:: python

	SENSe:MIXer:PORTs



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Mixer.Ports.PortsCls
	:members:
	:undoc-members:
	:noindex:
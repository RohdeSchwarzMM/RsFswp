Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BWIDth:LIST:DATA

.. code-block:: python

	SENSe:BWIDth:LIST:DATA



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Bandwidth.ListPy.Data.DataCls
	:members:
	:undoc-members:
	:noindex:
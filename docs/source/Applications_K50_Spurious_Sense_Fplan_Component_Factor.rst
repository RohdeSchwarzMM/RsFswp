Factor
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FPLan:COMPonent<Component>:FACTor

.. code-block:: python

	SENSe:FPLan:COMPonent<Component>:FACTor



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Fplan.Component.Factor.FactorCls
	:members:
	:undoc-members:
	:noindex:
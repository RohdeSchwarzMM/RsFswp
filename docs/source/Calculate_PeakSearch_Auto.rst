Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:PSEarch:AUTO

.. code-block:: python

	CALCulate<Window>:PSEarch:AUTO



.. autoclass:: RsFswp.Implementations.Calculate.PeakSearch.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
Start
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:TRACe<Trace>:STARt<GateRange>

.. code-block:: python

	SENSe:SWEep:EGATe:TRACe<Trace>:STARt<GateRange>



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Egate.Trace.Start.StartCls
	:members:
	:undoc-members:
	:noindex:
Limit<LimitIx>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.applications.k40PhaseNoise.calculate.limit.repcap_limitIx_get()
	driver.applications.k40PhaseNoise.calculate.limit.repcap_limitIx_set(repcap.LimitIx.Nr1)





.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Calculate.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k40PhaseNoise.calculate.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K40_PhaseNoise_Calculate_Limit_Active.rst
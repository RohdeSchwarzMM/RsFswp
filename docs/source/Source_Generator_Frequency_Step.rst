Step
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:GENerator:FREQuency:STEP

.. code-block:: python

	SOURce:GENerator:FREQuency:STEP



.. autoclass:: RsFswp.Implementations.Source.Generator.Frequency.Step.StepCls
	:members:
	:undoc-members:
	:noindex:
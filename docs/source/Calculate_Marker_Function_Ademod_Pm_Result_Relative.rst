Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:ADEMod:PM:RESult<Trace>:RELative

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:ADEMod:PM:RESult<Trace>:RELative



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Ademod.Pm.Result.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex:
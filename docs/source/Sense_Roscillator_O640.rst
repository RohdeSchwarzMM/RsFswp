O640
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ROSCillator:O640

.. code-block:: python

	SENSe:ROSCillator:O640



.. autoclass:: RsFswp.Implementations.Sense.Roscillator.O640.O640Cls
	:members:
	:undoc-members:
	:noindex:
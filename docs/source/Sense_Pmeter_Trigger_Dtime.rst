Dtime
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:TRIGger:DTIMe

.. code-block:: python

	SENSe:PMETer<PowerMeter>:TRIGger:DTIMe



.. autoclass:: RsFswp.Implementations.Sense.Pmeter.Trigger.Dtime.DtimeCls
	:members:
	:undoc-members:
	:noindex:
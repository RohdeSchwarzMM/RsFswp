Stop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:CHRDetection:PNOise:FREQuency:STOP

.. code-block:: python

	CALCulate<Window>:CHRDetection:PNOise:FREQuency:STOP



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.ChrDetection.Pnoise.Frequency.Stop.StopCls
	:members:
	:undoc-members:
	:noindex:
Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:SELect

.. code-block:: python

	INSTrument:SELect



.. autoclass:: RsFswp.Implementations.Instrument.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
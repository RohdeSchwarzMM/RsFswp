Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:LOWer:MODE

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:LOWer:MODE



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Lower.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
RsFswp Utilities
==========================

.. _Utilities:

.. autoclass:: RsFswp.CustomFiles.utilities.Utilities()
   :members:
   :undoc-members:
   :special-members: enable_properties
   :noindex:
   :member-order: bysource

Above
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:SPECtrogram:Y:MAXimum:ABOVe

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:SPECtrogram:Y:MAXimum:ABOVe



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Spectrogram.Y.Maximum.Above.AboveCls
	:members:
	:undoc-members:
	:noindex:
Y
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:Y

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:Y



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.DeltaMarker.Y.YCls
	:members:
	:undoc-members:
	:noindex:
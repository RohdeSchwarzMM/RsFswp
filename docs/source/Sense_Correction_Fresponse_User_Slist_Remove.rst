Remove
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:REMove

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:REMove



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Slist.Remove.RemoveCls
	:members:
	:undoc-members:
	:noindex:
CurrentDirectory
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:CDIRectory

.. code-block:: python

	MMEMory:CDIRectory



.. autoclass:: RsFswp.Implementations.MassMemory.CurrentDirectory.CurrentDirectoryCls
	:members:
	:undoc-members:
	:noindex:
TpiSample
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRACe:IQ:TPISample

.. code-block:: python

	TRACe:IQ:TPISample



.. autoclass:: RsFswp.Implementations.Applications.IqAnalyzer.Trace.Iq.TpiSample.TpiSampleCls
	:members:
	:undoc-members:
	:noindex:
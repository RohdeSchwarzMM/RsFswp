Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:DIQ:SRATe:AUTO

.. code-block:: python

	INPut:DIQ:SRATe:AUTO



.. autoclass:: RsFswp.Implementations.InputPy.Diq.SymbolRate.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
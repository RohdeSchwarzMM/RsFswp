Comment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:STANdard:COMMent

.. code-block:: python

	SENSe:DDEMod:STANdard:COMMent



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Standard.Comment.CommentCls
	:members:
	:undoc-members:
	:noindex:
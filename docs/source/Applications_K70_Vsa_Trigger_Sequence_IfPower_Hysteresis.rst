Hysteresis
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger<TriggerPort>:SEQuence:IFPower:HYSTeresis

.. code-block:: python

	TRIGger<TriggerPort>:SEQuence:IFPower:HYSTeresis



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Trigger.Sequence.IfPower.Hysteresis.HysteresisCls
	:members:
	:undoc-members:
	:noindex:
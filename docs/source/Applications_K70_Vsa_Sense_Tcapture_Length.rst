Length
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:TCAPture:LENGth

.. code-block:: python

	SENSe:TCAPture:LENGth



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Tcapture.Length.LengthCls
	:members:
	:undoc-members:
	:noindex:
Irejection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:IREJection

.. code-block:: python

	SENSe:CORRection:IREJection



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Irejection.IrejectionCls
	:members:
	:undoc-members:
	:noindex:
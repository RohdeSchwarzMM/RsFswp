Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:NTRansition

.. code-block:: python

	STATus:OPERation:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Operation.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
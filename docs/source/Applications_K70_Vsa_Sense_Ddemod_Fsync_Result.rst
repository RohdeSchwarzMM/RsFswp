Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:FSYNc:RESult

.. code-block:: python

	SENSe:DDEMod:FSYNc:RESult



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Fsync.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
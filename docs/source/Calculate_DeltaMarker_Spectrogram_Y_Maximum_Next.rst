Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:SPECtrogram:Y:MAXimum:NEXT

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:SPECtrogram:Y:MAXimum:NEXT



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Spectrogram.Y.Maximum.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
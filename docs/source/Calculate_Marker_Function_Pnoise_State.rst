State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:PNOise:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:PNOise:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Pnoise.State.StateCls
	:members:
	:undoc-members:
	:noindex:
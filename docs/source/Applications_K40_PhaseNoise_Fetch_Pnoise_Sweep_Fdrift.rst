Fdrift
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:PNOise<Trace>:SWEep:FDRift

.. code-block:: python

	FETCh:PNOise<Trace>:SWEep:FDRift



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Fetch.Pnoise.Sweep.Fdrift.FdriftCls
	:members:
	:undoc-members:
	:noindex:
Dtime
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:DTIMe

.. code-block:: python

	TRIGger:SEQuence:DTIMe



.. autoclass:: RsFswp.Implementations.Trigger.Sequence.Dtime.DtimeCls
	:members:
	:undoc-members:
	:noindex:
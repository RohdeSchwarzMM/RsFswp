Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:SELect

.. code-block:: python

	DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:SELect



.. autoclass:: RsFswp.Implementations.Display.Window.Subwindow.Trace.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
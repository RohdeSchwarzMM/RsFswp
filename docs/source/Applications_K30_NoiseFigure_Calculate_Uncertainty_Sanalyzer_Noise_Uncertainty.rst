Uncertainty
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNCertainty:SANalyzer:NOISe:UNCertainty

.. code-block:: python

	CALCulate<Window>:UNCertainty:SANalyzer:NOISe:UNCertainty



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Uncertainty.Sanalyzer.Noise.Uncertainty.UncertaintyCls
	:members:
	:undoc-members:
	:noindex:
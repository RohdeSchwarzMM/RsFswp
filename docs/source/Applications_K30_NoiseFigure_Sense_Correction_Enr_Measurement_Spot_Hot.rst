Hot
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:ENR:MEASurement:SPOT:HOT

.. code-block:: python

	SENSe:CORRection:ENR:MEASurement:SPOT:HOT



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Enr.Measurement.Spot.Hot.HotCls
	:members:
	:undoc-members:
	:noindex:
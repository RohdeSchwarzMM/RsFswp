Add
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SEARch:SYNC:PATTern:ADD

.. code-block:: python

	SENSe:DDEMod:SEARch:SYNC:PATTern:ADD



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Search.Sync.Pattern.Add.AddCls
	:members:
	:undoc-members:
	:noindex:
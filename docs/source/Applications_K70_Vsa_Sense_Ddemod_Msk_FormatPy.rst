FormatPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:MSK:FORMat

.. code-block:: python

	SENSe:DDEMod:MSK:FORMat



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Msk.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex:
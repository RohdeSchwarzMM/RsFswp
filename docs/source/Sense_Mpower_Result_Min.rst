Min
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:MPOWer:RESult:MIN

.. code-block:: python

	SENSe:MPOWer:RESult:MIN



.. autoclass:: RsFswp.Implementations.Sense.Mpower.Result.Min.MinCls
	:members:
	:undoc-members:
	:noindex:
FromPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:PORTs:FROM

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:PORTs:FROM



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Slist.Ports.FromPy.FromPyCls
	:members:
	:undoc-members:
	:noindex:
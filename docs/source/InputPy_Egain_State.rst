State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:EGAin:STATe

.. code-block:: python

	INPut<InputIx>:EGAin:STATe



.. autoclass:: RsFswp.Implementations.InputPy.Egain.State.StateCls
	:members:
	:undoc-members:
	:noindex:
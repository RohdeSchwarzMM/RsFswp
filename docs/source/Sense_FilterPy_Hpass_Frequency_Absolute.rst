Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FILTer<FilterPy>:HPASs:FREQuency:ABSolute

.. code-block:: python

	SENSe:FILTer<FilterPy>:HPASs:FREQuency:ABSolute



.. autoclass:: RsFswp.Implementations.Sense.FilterPy.Hpass.Frequency.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex:
Wselect
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WSELect

.. code-block:: python

	DISPlay:WSELect



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Display.Wselect.WselectCls
	:members:
	:undoc-members:
	:noindex:
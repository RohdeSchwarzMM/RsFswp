State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FILTer<FilterPy>:LPASs:STATe

.. code-block:: python

	SENSe:FILTer<FilterPy>:LPASs:STATe



.. autoclass:: RsFswp.Implementations.Sense.FilterPy.Lpass.State.StateCls
	:members:
	:undoc-members:
	:noindex:
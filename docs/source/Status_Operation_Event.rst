Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:EVENt

.. code-block:: python

	STATus:OPERation:EVENt



.. autoclass:: RsFswp.Implementations.Status.Operation.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
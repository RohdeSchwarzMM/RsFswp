State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:NDBDown:STATe

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:NDBDown:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.NdbDown.State.StateCls
	:members:
	:undoc-members:
	:noindex:
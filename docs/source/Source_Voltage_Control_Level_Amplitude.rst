Amplitude
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:CONTrol<Source>:LEVel:AMPLitude

.. code-block:: python

	SOURce:VOLTage:CONTrol<Source>:LEVel:AMPLitude



.. autoclass:: RsFswp.Implementations.Source.Voltage.Control.Level.Amplitude.AmplitudeCls
	:members:
	:undoc-members:
	:noindex:
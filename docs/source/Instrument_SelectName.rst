SelectName
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:SELect

.. code-block:: python

	INSTrument:SELect



.. autoclass:: RsFswp.Implementations.Instrument.SelectName.SelectNameCls
	:members:
	:undoc-members:
	:noindex:
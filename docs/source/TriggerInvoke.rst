TriggerInvoke
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: *TRG

.. code-block:: python

	*TRG



.. autoclass:: RsFswp.Implementations.TriggerInvoke.TriggerInvokeCls
	:members:
	:undoc-members:
	:noindex:
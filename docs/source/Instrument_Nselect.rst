Nselect
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:NSELect

.. code-block:: python

	INSTrument:NSELect



.. autoclass:: RsFswp.Implementations.Instrument.Nselect.NselectCls
	:members:
	:undoc-members:
	:noindex:
Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:COUNt

.. code-block:: python

	SENSe:SWEep:COUNt



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Sweep.Count.CountCls
	:members:
	:undoc-members:
	:noindex:
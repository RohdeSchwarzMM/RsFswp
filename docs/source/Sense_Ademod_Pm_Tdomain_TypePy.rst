TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:PM:TDOMain:TYPE

.. code-block:: python

	SENSe:ADEMod:PM:TDOMain:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Pm.Tdomain.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
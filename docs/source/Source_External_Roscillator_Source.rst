Source
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EXTernal<ExternalRosc>:ROSCillator:SOURce

.. code-block:: python

	SOURce:EXTernal<ExternalRosc>:ROSCillator:SOURce



.. autoclass:: RsFswp.Implementations.Source.External.Roscillator.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:
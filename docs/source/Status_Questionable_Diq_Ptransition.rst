Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:DIQ:PTRansition

.. code-block:: python

	STATus:QUEStionable:DIQ:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Diq.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
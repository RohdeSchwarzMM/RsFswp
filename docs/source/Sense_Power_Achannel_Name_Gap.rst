Gap<GapChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.sense.power.achannel.name.gap.repcap_gapChannel_get()
	driver.sense.power.achannel.name.gap.repcap_gapChannel_set(repcap.GapChannel.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:NAME:GAP<GapChannel>

.. code-block:: python

	SENSe:POWer:ACHannel:NAME:GAP<GapChannel>



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Name.Gap.GapCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.power.achannel.name.gap.clone()
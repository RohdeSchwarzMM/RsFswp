Exclusive
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ESPectrum<SubBlock>:PCLass<PowerClass>:EXCLusive

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ESPectrum<SubBlock>:PCLass<PowerClass>:EXCLusive



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Espectrum.Pclass.Exclusive.ExclusiveCls
	:members:
	:undoc-members:
	:noindex:
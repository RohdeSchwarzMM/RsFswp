Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:FREQuency:PTRansition

.. code-block:: python

	STATus:QUEStionable:FREQuency:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Frequency.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
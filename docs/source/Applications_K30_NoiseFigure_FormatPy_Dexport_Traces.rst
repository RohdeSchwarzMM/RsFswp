Traces
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMat:DEXPort:TRACes

.. code-block:: python

	FORMat:DEXPort:TRACes



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.FormatPy.Dexport.Traces.TracesCls
	:members:
	:undoc-members:
	:noindex:
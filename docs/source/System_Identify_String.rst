String
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:IDENtify:STRing

.. code-block:: python

	SYSTem:IDENtify:STRing



.. autoclass:: RsFswp.Implementations.System.Identify.String.StringCls
	:members:
	:undoc-members:
	:noindex:
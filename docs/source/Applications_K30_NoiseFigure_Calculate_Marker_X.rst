X
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:X

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:X



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Marker.X.XCls
	:members:
	:undoc-members:
	:noindex:
Spacing
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:X:SPACing

.. code-block:: python

	DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:X:SPACing



.. autoclass:: RsFswp.Implementations.Display.Window.Subwindow.Trace.X.Spacing.SpacingCls
	:members:
	:undoc-members:
	:noindex:
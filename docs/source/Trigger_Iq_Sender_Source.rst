Source
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:IQ:SENDer:SOURce

.. code-block:: python

	TRIGger:IQ:SENDer:SOURce



.. autoclass:: RsFswp.Implementations.Trigger.Iq.Sender.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:
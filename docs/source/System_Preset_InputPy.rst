InputPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PRESet:INPut

.. code-block:: python

	SYSTem:PRESet:INPut



.. autoclass:: RsFswp.Implementations.System.Preset.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex:
Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:MERRor:RPEak:VALue

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:MERRor:RPEak:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Merror.Rpeak.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
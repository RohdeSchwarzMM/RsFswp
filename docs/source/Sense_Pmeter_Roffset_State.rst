State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:ROFFset:STATe

.. code-block:: python

	SENSe:PMETer<PowerMeter>:ROFFset:STATe



.. autoclass:: RsFswp.Implementations.Sense.Pmeter.Roffset.State.StateCls
	:members:
	:undoc-members:
	:noindex:
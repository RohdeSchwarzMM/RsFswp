Freference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:FREFerence

.. code-block:: python

	SENSe:CREFerence:FREFerence



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Freference.FreferenceCls
	:members:
	:undoc-members:
	:noindex:
Z
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:Z

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:Z



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Z.ZCls
	:members:
	:undoc-members:
	:noindex:
TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:AVERage:TYPE

.. code-block:: python

	SENSe:AVERage:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Average.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
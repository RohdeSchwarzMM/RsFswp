Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:TOI:RESult:MINimum

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:TOI:RESult:MINimum



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Toi.Result.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:
Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:EXTended:INFO:NTRansition

.. code-block:: python

	STATus:QUEStionable:EXTended:INFO:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Extended.Info.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
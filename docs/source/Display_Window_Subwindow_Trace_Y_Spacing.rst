Spacing
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:Y:SPACing

.. code-block:: python

	DISPlay:WINDow<Window>:SUBWindow<SubWindow>:TRACe<Trace>:Y:SPACing



.. autoclass:: RsFswp.Implementations.Display.Window.Subwindow.Trace.Y.Spacing.SpacingCls
	:members:
	:undoc-members:
	:noindex:
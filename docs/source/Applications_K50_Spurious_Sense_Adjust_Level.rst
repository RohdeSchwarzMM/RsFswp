Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:LEVel

.. code-block:: python

	SENSe:ADJust:LEVel



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Adjust.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:
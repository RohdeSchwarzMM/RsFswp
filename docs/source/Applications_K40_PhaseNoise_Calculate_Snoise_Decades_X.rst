X
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:SNOise<Marker>:DECades:X

.. code-block:: python

	CALCulate<Window>:SNOise<Marker>:DECades:X



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Calculate.Snoise.Decades.X.XCls
	:members:
	:undoc-members:
	:noindex:
File
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Trace.Iq.File.FileCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.trace.iq.file.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_Trace_Iq_File_Repetition.rst
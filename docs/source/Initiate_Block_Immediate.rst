Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:BLOCk:IMMediate

.. code-block:: python

	INITiate:BLOCk:IMMediate



.. autoclass:: RsFswp.Implementations.Initiate.Block.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
Nsamples
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:STATistics:NSAMples

.. code-block:: python

	CALCulate<Window>:STATistics:NSAMples



.. autoclass:: RsFswp.Implementations.Calculate.Statistics.Nsamples.NsamplesCls
	:members:
	:undoc-members:
	:noindex:
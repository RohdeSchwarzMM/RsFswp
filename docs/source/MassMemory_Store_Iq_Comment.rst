Comment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:STORe<Store>:IQ:COMMent

.. code-block:: python

	MMEMory:STORe<Store>:IQ:COMMent



.. autoclass:: RsFswp.Implementations.MassMemory.Store.Iq.Comment.CommentCls
	:members:
	:undoc-members:
	:noindex:
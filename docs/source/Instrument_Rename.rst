Rename
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:REName

.. code-block:: python

	INSTrument:REName



.. autoclass:: RsFswp.Implementations.Instrument.Rename.RenameCls
	:members:
	:undoc-members:
	:noindex:
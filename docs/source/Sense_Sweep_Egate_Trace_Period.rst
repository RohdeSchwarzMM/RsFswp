Period
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:TRACe<Trace>:PERiod

.. code-block:: python

	SENSe:SWEep:EGATe:TRACe<Trace>:PERiod



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Egate.Trace.Period.PeriodCls
	:members:
	:undoc-members:
	:noindex:
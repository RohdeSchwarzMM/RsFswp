State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:BASeband:USER:SLISt<TouchStone>:STATe

.. code-block:: python

	SENSe:CORRection:FRESponse:BASeband:USER:SLISt<TouchStone>:STATe



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.Baseband.User.Slist.State.StateCls
	:members:
	:undoc-members:
	:noindex:
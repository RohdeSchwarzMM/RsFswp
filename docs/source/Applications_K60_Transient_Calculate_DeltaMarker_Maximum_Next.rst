Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:MAXimum:NEXT

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:MAXimum:NEXT



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.DeltaMarker.Maximum.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
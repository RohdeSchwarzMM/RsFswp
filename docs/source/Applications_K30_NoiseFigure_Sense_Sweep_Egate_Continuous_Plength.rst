Plength
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:CONTinuous:PLENgth

.. code-block:: python

	SENSe:SWEep:EGATe:CONTinuous:PLENgth



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Sweep.Egate.Continuous.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex:
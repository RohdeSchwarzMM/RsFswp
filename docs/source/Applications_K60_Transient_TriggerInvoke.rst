TriggerInvoke
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: *TRG

.. code-block:: python

	*TRG



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.TriggerInvoke.TriggerInvokeCls
	:members:
	:undoc-members:
	:noindex:
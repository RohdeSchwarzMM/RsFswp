Sanalyzer
----------------------------------------





.. autoclass:: RsFswp.Implementations.InputPy.Sanalyzer.SanalyzerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputPy.sanalyzer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputPy_Sanalyzer_Attenuation.rst
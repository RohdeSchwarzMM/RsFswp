State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:TBAR:STATe

.. code-block:: python

	DISPlay:TBAR:STATe



.. autoclass:: RsFswp.Implementations.Display.Tbar.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PLUGin:APPStarter:SELect

.. code-block:: python

	SYSTem:PLUGin:APPStarter:SELect



.. autoclass:: RsFswp.Implementations.System.Plugin.AppStarter.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
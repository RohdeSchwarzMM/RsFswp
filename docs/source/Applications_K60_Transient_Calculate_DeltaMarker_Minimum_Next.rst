Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:MINimum:NEXT

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:MINimum:NEXT



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.DeltaMarker.Minimum.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
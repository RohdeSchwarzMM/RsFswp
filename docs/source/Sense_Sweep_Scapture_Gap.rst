Gap
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:SCAPture:GAP

.. code-block:: python

	SENSe:SWEep:SCAPture:GAP



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Scapture.Gap.GapCls
	:members:
	:undoc-members:
	:noindex:
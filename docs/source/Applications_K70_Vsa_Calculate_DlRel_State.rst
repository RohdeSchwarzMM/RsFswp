State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DLRel:STATe

.. code-block:: python

	CALCulate<Window>:DLRel:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.DlRel.State.StateCls
	:members:
	:undoc-members:
	:noindex:
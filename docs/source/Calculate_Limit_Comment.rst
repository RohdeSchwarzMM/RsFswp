Comment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:COMMent

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:COMMent



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Comment.CommentCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:SEARch:BURSt:STATe

.. code-block:: python

	SENSe:DDEMod:SEARch:BURSt:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Search.Burst.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:FACTory:VALue

.. code-block:: python

	SENSe:DDEMod:FACTory:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Factory.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
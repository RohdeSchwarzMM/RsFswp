Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:AVERage:COUNt

.. code-block:: python

	SENSe:AVERage:COUNt



.. autoclass:: RsFswp.Implementations.Sense.Average.Count.CountCls
	:members:
	:undoc-members:
	:noindex:
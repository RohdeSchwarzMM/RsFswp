Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INTegrity:EVENt

.. code-block:: python

	STATus:QUEStionable:INTegrity:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Integrity.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
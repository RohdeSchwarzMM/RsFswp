Aoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:AOFF

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:AOFF



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Marker.Aoff.AoffCls
	:members:
	:undoc-members:
	:noindex:
Spurious
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:SPURious

.. code-block:: python

	INITiate:SPURious



.. autoclass:: RsFswp.Implementations.Initiate.Spurious.SpuriousCls
	:members:
	:undoc-members:
	:noindex:
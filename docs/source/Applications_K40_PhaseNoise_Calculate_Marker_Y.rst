Y
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:Y

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:Y



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Calculate.Marker.Y.YCls
	:members:
	:undoc-members:
	:noindex:
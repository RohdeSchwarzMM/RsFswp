Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:STATistics:SCALe:Y:LOWer

.. code-block:: python

	CALCulate<Window>:STATistics:SCALe:Y:LOWer



.. autoclass:: RsFswp.Implementations.Calculate.Statistics.Scale.Y.Lower.LowerCls
	:members:
	:undoc-members:
	:noindex:
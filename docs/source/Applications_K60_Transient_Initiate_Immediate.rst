Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:IMMediate

.. code-block:: python

	INITiate:IMMediate



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Initiate.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
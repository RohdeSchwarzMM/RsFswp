Output
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:GENerator:PULSe:TRIGger:OUTPut

.. code-block:: python

	SOURce:GENerator:PULSe:TRIGger:OUTPut



.. autoclass:: RsFswp.Implementations.Source.Generator.Pulse.Trigger.Output.OutputCls
	:members:
	:undoc-members:
	:noindex:
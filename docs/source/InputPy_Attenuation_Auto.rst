Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:ATTenuation:AUTO

.. code-block:: python

	INPut:ATTenuation:AUTO



.. autoclass:: RsFswp.Implementations.InputPy.Attenuation.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
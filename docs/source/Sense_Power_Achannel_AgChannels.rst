AgChannels
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:AGCHannels

.. code-block:: python

	SENSe:POWer:ACHannel:AGCHannels



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.AgChannels.AgChannelsCls
	:members:
	:undoc-members:
	:noindex:
Orientation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:PAGE:ORIentation

.. code-block:: python

	HCOPy:PAGE:ORIentation



.. autoclass:: RsFswp.Implementations.HardCopy.Page.Orientation.OrientationCls
	:members:
	:undoc-members:
	:noindex:
Macros
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:SELect:ITEM:MACRos

.. code-block:: python

	MMEMory:SELect:ITEM:MACRos



.. autoclass:: RsFswp.Implementations.MassMemory.Select.Item.Macros.MacrosCls
	:members:
	:undoc-members:
	:noindex:
Define
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MATH:EXPRession:DEFine

.. code-block:: python

	CALCulate<Window>:MATH:EXPRession:DEFine



.. autoclass:: RsFswp.Implementations.Calculate.Math.Expression.Define.DefineCls
	:members:
	:undoc-members:
	:noindex:
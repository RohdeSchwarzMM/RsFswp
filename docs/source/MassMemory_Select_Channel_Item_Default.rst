Default
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:SELect:CHANnel:ITEM:DEFault

.. code-block:: python

	MMEMory:SELect:CHANnel:ITEM:DEFault



.. autoclass:: RsFswp.Implementations.MassMemory.Select.Channel.Item.Default.DefaultCls
	:members:
	:undoc-members:
	:noindex:
Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:EATT:AUTO

.. code-block:: python

	INPut:EATT:AUTO



.. autoclass:: RsFswp.Implementations.InputPy.Eatt.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
Spurious
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:STORe<Store>:SPURious

.. code-block:: python

	MMEMory:STORe<Store>:SPURious



.. autoclass:: RsFswp.Implementations.MassMemory.Store.Spurious.SpuriousCls
	:members:
	:undoc-members:
	:noindex:
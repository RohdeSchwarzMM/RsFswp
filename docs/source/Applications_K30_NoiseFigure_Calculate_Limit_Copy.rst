Copy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:COPY

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:COPY



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Limit.Copy.CopyCls
	:members:
	:undoc-members:
	:noindex:
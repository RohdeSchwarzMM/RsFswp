Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:HOLDoff:TIME

.. code-block:: python

	TRIGger:SEQuence:HOLDoff:TIME



.. autoclass:: RsFswp.Implementations.Trigger.Sequence.Holdoff.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:
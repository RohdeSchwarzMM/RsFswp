Scenter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:SCENter

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:SCENter



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Scenter.ScenterCls
	:members:
	:undoc-members:
	:noindex:
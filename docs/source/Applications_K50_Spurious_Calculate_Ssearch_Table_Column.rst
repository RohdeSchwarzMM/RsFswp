Column
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:SSEarch:TABLe:COLumn

.. code-block:: python

	CALCulate:SSEarch:TABLe:COLumn



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Ssearch.Table.Column.ColumnCls
	:members:
	:undoc-members:
	:noindex:
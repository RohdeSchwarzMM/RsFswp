State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:GENerator:STATe

.. code-block:: python

	INSTrument:COUPle:GENerator:STATe



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Generator.State.StateCls
	:members:
	:undoc-members:
	:noindex:
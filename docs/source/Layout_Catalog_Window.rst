Window
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:CATalog:WINDow

.. code-block:: python

	LAYout:CATalog:WINDow



.. autoclass:: RsFswp.Implementations.Layout.Catalog.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:KDATa:STATe

.. code-block:: python

	SENSe:DDEMod:KDATa:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Kdata.State.StateCls
	:members:
	:undoc-members:
	:noindex:
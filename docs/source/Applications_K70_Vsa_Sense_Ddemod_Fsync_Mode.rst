Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:FSYNc:MODE

.. code-block:: python

	SENSe:DDEMod:FSYNc:MODE



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Fsync.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
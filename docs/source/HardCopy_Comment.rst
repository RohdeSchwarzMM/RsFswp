Comment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:COMMent

.. code-block:: python

	HCOPy:COMMent



.. autoclass:: RsFswp.Implementations.HardCopy.Comment.CommentCls
	:members:
	:undoc-members:
	:noindex:
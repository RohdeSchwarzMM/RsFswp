State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:PERRor:PPEak:STATe

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:PERRor:PPEak:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Perror.Ppeak.State.StateCls
	:members:
	:undoc-members:
	:noindex:
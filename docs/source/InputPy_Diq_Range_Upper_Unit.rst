Unit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:DIQ:RANGe:UPPer:UNIT

.. code-block:: python

	INPut:DIQ:RANGe:UPPer:UNIT



.. autoclass:: RsFswp.Implementations.InputPy.Diq.Range.Upper.Unit.UnitCls
	:members:
	:undoc-members:
	:noindex:
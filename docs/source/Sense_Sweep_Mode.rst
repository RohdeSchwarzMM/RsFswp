Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:MODE

.. code-block:: python

	SENSe:SWEep:MODE



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
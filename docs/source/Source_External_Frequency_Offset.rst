Offset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EXTernal<ExternalGen>:FREQuency:OFFSet

.. code-block:: python

	SOURce:EXTernal<ExternalGen>:FREQuency:OFFSet



.. autoclass:: RsFswp.Implementations.Source.External.Frequency.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:
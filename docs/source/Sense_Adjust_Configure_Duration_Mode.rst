Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:CONFigure:DURation:MODE

.. code-block:: python

	SENSe:ADJust:CONFigure:DURation:MODE



.. autoclass:: RsFswp.Implementations.Sense.Adjust.Configure.Duration.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:
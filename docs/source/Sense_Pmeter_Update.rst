Update
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.Pmeter.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.pmeter.update.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Pmeter_Update_State.rst
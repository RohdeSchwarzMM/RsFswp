Magnitude
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:PMETer<PowerMeter>:RELative:MAGNitude

.. code-block:: python

	CALCulate<Window>:PMETer<PowerMeter>:RELative:MAGNitude



.. autoclass:: RsFswp.Implementations.Calculate.Pmeter.Relative.Magnitude.MagnitudeCls
	:members:
	:undoc-members:
	:noindex:
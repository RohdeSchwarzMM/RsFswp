Manual
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:POWer:ACHannel:REFerence:TXCHannel:MANual

.. code-block:: python

	SENSe:POWer:ACHannel:REFerence:TXCHannel:MANual



.. autoclass:: RsFswp.Implementations.Sense.Power.Achannel.Reference.TxChannel.Manual.ManualCls
	:members:
	:undoc-members:
	:noindex:
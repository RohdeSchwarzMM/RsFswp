Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:OOFFset:PEAK:VALue

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:OOFFset:PEAK:VALue



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Ooffset.Peak.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:INPut:SELect

.. code-block:: python

	DIAGnostic:SERVice:INPut:SELect



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.InputPy.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:
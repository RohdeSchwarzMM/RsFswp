Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:HARMonics:BANDwidth:AUTO

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:HARMonics:BANDwidth:AUTO



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Harmonics.Bandwidth.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:TRIGger<TriggerPort>:LEVel

.. code-block:: python

	OUTPut:TRIGger<TriggerPort>:LEVel



.. autoclass:: RsFswp.Implementations.Output.Trigger.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:
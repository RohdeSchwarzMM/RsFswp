Function
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WINDow<Window>:DETector<Trace>:FUNCtion

.. code-block:: python

	SENSe:WINDow<Window>:DETector<Trace>:FUNCtion



.. autoclass:: RsFswp.Implementations.Sense.Window.Detector.Function.FunctionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.window.detector.function.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Window_Detector_Function_Auto.rst
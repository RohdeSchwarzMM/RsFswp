Control<Source>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.source.voltage.control.repcap_source_get()
	driver.source.voltage.control.repcap_source_set(repcap.Source.Nr1)





.. autoclass:: RsFswp.Implementations.Source.Voltage.Control.ControlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.voltage.control.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Voltage_Control_Level.rst
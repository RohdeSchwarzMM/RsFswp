Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FORMAT REAL,32;SENSe:CORRection:FRESponse:USER:IQ:DATA:FREQuency

.. code-block:: python

	FORMAT REAL,32;SENSe:CORRection:FRESponse:USER:IQ:DATA:FREQuency



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Iq.Data.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:
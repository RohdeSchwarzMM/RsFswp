Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ACPower:ACHannel:RESult:ABSolute

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ACPower:ACHannel:RESult:ABSolute



.. autoclass:: RsFswp.Implementations.Calculate.Limit.AcPower.Achannel.Result.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex:
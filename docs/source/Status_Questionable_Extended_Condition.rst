Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:EXTended:CONDition

.. code-block:: python

	STATus:QUEStionable:EXTended:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Extended.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
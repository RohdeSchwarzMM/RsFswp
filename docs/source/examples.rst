Examples
======================

For more examples, visit our `Rohde & Schwarz Github repository <https://github.com/Rohde-Schwarz/Examples/tree/main/SpectrumAnalyzers/Python/RsFswp_ScpiPackage>`_.



.. literalinclude:: RsFswp_GettingStarted_Example.py


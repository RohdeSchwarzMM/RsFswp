Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEue:NEXT

.. code-block:: python

	STATus:QUEue:NEXT



.. autoclass:: RsFswp.Implementations.Status.Queue.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
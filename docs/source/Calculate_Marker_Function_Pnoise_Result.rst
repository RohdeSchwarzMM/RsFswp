Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:PNOise:RESult

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:PNOise:RESult



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Pnoise.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
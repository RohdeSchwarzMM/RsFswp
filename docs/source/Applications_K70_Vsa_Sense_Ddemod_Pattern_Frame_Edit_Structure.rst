Structure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:FRAMe:EDIT:STRucture

.. code-block:: python

	SENSe:DDEMod:PATTern:FRAMe:EDIT:STRucture



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.Frame.Edit.Structure.StructureCls
	:members:
	:undoc-members:
	:noindex:
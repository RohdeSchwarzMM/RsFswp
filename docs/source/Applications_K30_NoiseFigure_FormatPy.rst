FormatPy
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k30NoiseFigure.formatPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K30_NoiseFigure_FormatPy_Dexport.rst
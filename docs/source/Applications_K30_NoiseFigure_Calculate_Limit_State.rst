State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:STATe

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:STATe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Calculate.Limit.State.StateCls
	:members:
	:undoc-members:
	:noindex:
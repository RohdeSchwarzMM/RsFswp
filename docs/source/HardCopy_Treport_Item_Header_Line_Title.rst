Title
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:ITEM:HEADer:LINE<Line>:TITLe

.. code-block:: python

	HCOPy:TREPort:ITEM:HEADer:LINE<Line>:TITLe



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Item.Header.Line.Title.TitleCls
	:members:
	:undoc-members:
	:noindex:
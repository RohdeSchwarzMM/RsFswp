Sequence
----------------------------------------





.. autoclass:: RsFswp.Implementations.Source.Current.Sequence.SequenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.current.sequence.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Current_Sequence_Result.rst
TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:AM:ABSolute:TDOMain:TYPE

.. code-block:: python

	SENSe:ADEMod:AM:ABSolute:TDOMain:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Am.Absolute.Tdomain.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
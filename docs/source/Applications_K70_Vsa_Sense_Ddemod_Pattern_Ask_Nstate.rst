Nstate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:ASK:NSTate

.. code-block:: python

	SENSe:DDEMod:PATTern:ASK:NSTate



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.Ask.Nstate.NstateCls
	:members:
	:undoc-members:
	:noindex:
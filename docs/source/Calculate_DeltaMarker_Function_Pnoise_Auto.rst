Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:PNOise:AUTO

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:PNOise:AUTO



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Function.Pnoise.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADJust:CARRier

.. code-block:: python

	SENSe:ADJust:CARRier



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Adjust.Carrier.CarrierCls
	:members:
	:undoc-members:
	:noindex:
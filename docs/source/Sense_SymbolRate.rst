SymbolRate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SRATe

.. code-block:: python

	SENSe:SRATe



.. autoclass:: RsFswp.Implementations.Sense.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex:
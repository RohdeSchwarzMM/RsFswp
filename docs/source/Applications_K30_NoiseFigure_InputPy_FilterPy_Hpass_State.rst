State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:FILTer:HPASs:STATe

.. code-block:: python

	INPut<InputIx>:FILTer:HPASs:STATe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.InputPy.FilterPy.Hpass.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Offset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:OFFSet

.. code-block:: python

	SENSe:FREQuency:OFFSet



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Frequency.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:
Band
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:MSR:BAND

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:MSR:BAND



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Msr.Band.BandCls
	:members:
	:undoc-members:
	:noindex:
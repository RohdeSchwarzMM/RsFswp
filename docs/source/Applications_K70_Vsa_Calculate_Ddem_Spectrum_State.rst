State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DDEM:SPECtrum:STATe

.. code-block:: python

	CALCulate<Window>:DDEM:SPECtrum:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Ddem.Spectrum.State.StateCls
	:members:
	:undoc-members:
	:noindex:
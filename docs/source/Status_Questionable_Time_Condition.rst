Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TIME:CONDition

.. code-block:: python

	STATus:QUEStionable:TIME:CONDition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Time.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
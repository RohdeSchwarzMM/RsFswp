State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:ACPower:GAP<GapChannel>:AUTO:ACLR:RELative:STATe

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:ACPower:GAP<GapChannel>:AUTO:ACLR:RELative:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Limit.AcPower.Gap.Auto.Aclr.Relative.State.StateCls
	:members:
	:undoc-members:
	:noindex:
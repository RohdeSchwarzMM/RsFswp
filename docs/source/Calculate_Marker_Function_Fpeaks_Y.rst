Y
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:FPEaks:Y

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:FPEaks:Y



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Fpeaks.Y.YCls
	:members:
	:undoc-members:
	:noindex:
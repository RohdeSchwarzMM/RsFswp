Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe:Y:SCALe:AUTO

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe:Y:SCALe:AUTO



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Display.Window.Trace.Y.Scale.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
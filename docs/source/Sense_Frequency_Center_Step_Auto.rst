Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FREQuency:CENTer:STEP:AUTO

.. code-block:: python

	SENSe:FREQuency:CENTer:STEP:AUTO



.. autoclass:: RsFswp.Implementations.Sense.Frequency.Center.Step.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
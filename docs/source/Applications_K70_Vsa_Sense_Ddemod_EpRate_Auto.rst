Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:EPRate:AUTO

.. code-block:: python

	SENSe:DDEMod:EPRate:AUTO



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.EpRate.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
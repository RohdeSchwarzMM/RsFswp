Fail
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:FAIL

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:FAIL



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Limit.Fail.FailCls
	:members:
	:undoc-members:
	:noindex:
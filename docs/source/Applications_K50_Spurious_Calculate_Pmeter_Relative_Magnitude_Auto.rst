Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:PMETer<PowerMeter>:RELative:MAGNitude:AUTO

.. code-block:: python

	CALCulate<Window>:PMETer<PowerMeter>:RELative:MAGNitude:AUTO



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Pmeter.Relative.Magnitude.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
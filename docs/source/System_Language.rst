Language
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:LANGuage

.. code-block:: python

	SYSTem:LANGuage



.. autoclass:: RsFswp.Implementations.System.Language.LanguageCls
	:members:
	:undoc-members:
	:noindex:
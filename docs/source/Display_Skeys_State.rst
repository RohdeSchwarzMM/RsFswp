State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:SKEYs:STATe

.. code-block:: python

	DISPlay:SKEYs:STATe



.. autoclass:: RsFswp.Implementations.Display.Skeys.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Reference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:FILTer:REFerence

.. code-block:: python

	SENSe:DDEMod:FILTer:REFerence



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.FilterPy.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex:
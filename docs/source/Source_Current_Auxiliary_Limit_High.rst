High
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:AUX:LIMit:HIGH

.. code-block:: python

	SOURce:CURRent:AUX:LIMit:HIGH



.. autoclass:: RsFswp.Implementations.Source.Current.Auxiliary.Limit.High.HighCls
	:members:
	:undoc-members:
	:noindex:
Magnitude
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:FLISt<FileList>:DATA:MAGNitude

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:FLISt<FileList>:DATA:MAGNitude



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Flist.Data.Magnitude.MagnitudeCls
	:members:
	:undoc-members:
	:noindex:
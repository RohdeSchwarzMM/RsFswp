Source
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger<TriggerPort>:SEQuence:SOURce

.. code-block:: python

	TRIGger<TriggerPort>:SEQuence:SOURce



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Trigger.Sequence.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:
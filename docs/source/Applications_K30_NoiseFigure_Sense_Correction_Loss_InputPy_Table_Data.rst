Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:LOSS:INPut:TABLe:DATA

.. code-block:: python

	SENSe:CORRection:LOSS:INPut:TABLe:DATA



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Loss.InputPy.Table.Data.DataCls
	:members:
	:undoc-members:
	:noindex:
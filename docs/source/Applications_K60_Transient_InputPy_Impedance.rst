Impedance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:IMPedance

.. code-block:: python

	INPut:IMPedance



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.InputPy.Impedance.ImpedanceCls
	:members:
	:undoc-members:
	:noindex:
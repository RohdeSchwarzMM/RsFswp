RefLevel
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.InputPy.User.Adjust.RefLevel.RefLevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.correction.fresponse.inputPy.user.adjust.refLevel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Correction_Fresponse_InputPy_User_Adjust_RefLevel_State.rst
Attenuation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DIRected:INPut:ATTenuation

.. code-block:: python

	SENSe:DIRected:INPut:ATTenuation



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Directed.InputPy.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex:
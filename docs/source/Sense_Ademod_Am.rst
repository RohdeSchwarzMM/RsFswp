Am
----------------------------------------





.. autoclass:: RsFswp.Implementations.Sense.Ademod.Am.AmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ademod.am.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Ademod_Am_Absolute.rst
	Sense_Ademod_Am_Relative.rst
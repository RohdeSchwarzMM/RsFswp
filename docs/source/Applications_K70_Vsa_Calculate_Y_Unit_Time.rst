Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:Y:UNIT:TIME

.. code-block:: python

	CALCulate<Window>:Y:UNIT:TIME



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Y.Unit.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:
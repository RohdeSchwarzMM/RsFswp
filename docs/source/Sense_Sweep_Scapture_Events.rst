Events
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:SCAPture:EVENts

.. code-block:: python

	SENSe:SWEep:SCAPture:EVENts



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Scapture.Events.EventsCls
	:members:
	:undoc-members:
	:noindex:
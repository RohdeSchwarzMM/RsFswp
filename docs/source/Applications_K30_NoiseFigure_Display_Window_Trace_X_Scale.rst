Scale
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:X:SCALe

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:X:SCALe



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Display.Window.Trace.X.Scale.ScaleCls
	:members:
	:undoc-members:
	:noindex:
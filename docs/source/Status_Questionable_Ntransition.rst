Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:NTRansition

.. code-block:: python

	STATus:QUEStionable:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
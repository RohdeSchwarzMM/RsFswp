Y
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:Y

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:Y



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Marker.Y.YCls
	:members:
	:undoc-members:
	:noindex:
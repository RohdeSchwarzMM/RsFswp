Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:TRIGger<TriggerPort>:PULSe:IMMediate

.. code-block:: python

	OUTPut<OutputConnector>:TRIGger<TriggerPort>:PULSe:IMMediate



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Output.Trigger.Pulse.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:
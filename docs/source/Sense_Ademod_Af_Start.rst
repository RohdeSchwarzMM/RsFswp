Start
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:AF:STARt

.. code-block:: python

	SENSe:ADEMod:AF:STARt



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Af.Start.StartCls
	:members:
	:undoc-members:
	:noindex:
Connector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:CONNector

.. code-block:: python

	INPut:CONNector



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.InputPy.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex:
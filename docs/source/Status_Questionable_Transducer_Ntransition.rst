Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:TRANsducer:NTRansition

.. code-block:: python

	STATus:QUEStionable:TRANsducer:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Transducer.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
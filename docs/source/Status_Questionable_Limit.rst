Limit<Window>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.status.questionable.limit.repcap_window_get()
	driver.status.questionable.limit.repcap_window_set(repcap.Window.Nr1)





.. autoclass:: RsFswp.Implementations.Status.Questionable.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.status.questionable.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Questionable_Limit_Condition.rst
	Status_Questionable_Limit_Enable.rst
	Status_Questionable_Limit_Event.rst
	Status_Questionable_Limit_Ntransition.rst
	Status_Questionable_Limit_Ptransition.rst
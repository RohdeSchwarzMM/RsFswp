Magnitude
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:DATA:MAGNitude<SPortPair>

.. code-block:: python

	SENSe:CORRection:FRESponse:USER:SLISt<TouchStone>:DATA:MAGNitude<SPortPair>



.. autoclass:: RsFswp.Implementations.Sense.Correction.Fresponse.User.Slist.Data.Magnitude.MagnitudeCls
	:members:
	:undoc-members:
	:noindex:
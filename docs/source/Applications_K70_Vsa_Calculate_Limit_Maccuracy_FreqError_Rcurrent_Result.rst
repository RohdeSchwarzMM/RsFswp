Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:FERRor:RCURrent:RESult

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:FERRor:RCURrent:RESult



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.FreqError.Rcurrent.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
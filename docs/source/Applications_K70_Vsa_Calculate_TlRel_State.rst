State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:TLRel:STATe

.. code-block:: python

	CALCulate<Window>:TLRel:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.TlRel.State.StateCls
	:members:
	:undoc-members:
	:noindex:
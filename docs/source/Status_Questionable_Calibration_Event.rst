Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:CALibration:EVENt

.. code-block:: python

	STATus:QUEStionable:CALibration:EVENt



.. autoclass:: RsFswp.Implementations.Status.Questionable.Calibration.Event.EventCls
	:members:
	:undoc-members:
	:noindex:
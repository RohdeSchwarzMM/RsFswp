Magnitude
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:PMETer<PowerMeter>:RELative:MAGNitude

.. code-block:: python

	CALCulate<Window>:PMETer<PowerMeter>:RELative:MAGNitude



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Calculate.Pmeter.Relative.Magnitude.MagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.calculate.pmeter.relative.magnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Calculate_Pmeter_Relative_Magnitude_Auto.rst
Coupling
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:GENerator:CHANnel:COUPling

.. code-block:: python

	SOURce:GENerator:CHANnel:COUPling



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Source.Generator.Channel.Coupling.CouplingCls
	:members:
	:undoc-members:
	:noindex:
Length
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:TRIGger<TriggerPort>:PULSe:LENGth

.. code-block:: python

	OUTPut:TRIGger<TriggerPort>:PULSe:LENGth



.. autoclass:: RsFswp.Implementations.Output.Trigger.Pulse.Length.LengthCls
	:members:
	:undoc-members:
	:noindex:
Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:COUNt:FREQuency

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:COUNt:FREQuency



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Count.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:
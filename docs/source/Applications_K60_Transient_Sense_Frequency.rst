Frequency
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Sense.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k60Transient.sense.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K60_Transient_Sense_Frequency_Center.rst
	Applications_K60_Transient_Sense_Frequency_Offset.rst
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:CFERror:PEAK:STATe

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:CFERror:PEAK:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.CfError.Peak.State.StateCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit:MACCuracy:MERRor:PPEak:STATe

.. code-block:: python

	CALCulate<Window>:LIMit:MACCuracy:MERRor:PPEak:STATe



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Limit.Maccuracy.Merror.Ppeak.State.StateCls
	:members:
	:undoc-members:
	:noindex:
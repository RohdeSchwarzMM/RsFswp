Mtime
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:MTIMe

.. code-block:: python

	SENSe:ADEMod:MTIMe



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Mtime.MtimeCls
	:members:
	:undoc-members:
	:noindex:
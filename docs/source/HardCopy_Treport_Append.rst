Append
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:TREPort:APPend

.. code-block:: python

	HCOPy:TREPort:APPend



.. autoclass:: RsFswp.Implementations.HardCopy.Treport.Append.AppendCls
	:members:
	:undoc-members:
	:noindex:
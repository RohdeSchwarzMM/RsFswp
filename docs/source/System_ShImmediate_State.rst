State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:SHIMmediate:STATe

.. code-block:: python

	SYSTem:SHIMmediate:STATe



.. autoclass:: RsFswp.Implementations.System.ShImmediate.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Exec
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:PRESet:CHANnel:EXEC

.. code-block:: python

	SYSTem:PRESet:CHANnel:EXEC



.. autoclass:: RsFswp.Implementations.System.Preset.Channel.Exec.ExecCls
	:members:
	:undoc-members:
	:noindex:
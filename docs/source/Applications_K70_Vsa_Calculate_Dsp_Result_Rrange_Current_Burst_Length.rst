Length
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DSP:RESult:RRANge:CURRent:BURSt:LENGth

.. code-block:: python

	CALCulate<Window>:DSP:RESult:RRANge:CURRent:BURSt:LENGth



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Dsp.Result.Rrange.Current.Burst.Length.LengthCls
	:members:
	:undoc-members:
	:noindex:
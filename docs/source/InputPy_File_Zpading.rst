Zpading
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:FILE:ZPADing

.. code-block:: python

	INPut<InputIx>:FILE:ZPADing



.. autoclass:: RsFswp.Implementations.InputPy.File.Zpading.ZpadingCls
	:members:
	:undoc-members:
	:noindex:
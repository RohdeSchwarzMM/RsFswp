ConMeas
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:CONMeas

.. code-block:: python

	INITiate:CONMeas



.. autoclass:: RsFswp.Implementations.Initiate.ConMeas.ConMeasCls
	:members:
	:undoc-members:
	:noindex:
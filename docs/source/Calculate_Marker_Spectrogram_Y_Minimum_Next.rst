Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:SPECtrogram:Y:MINimum:NEXT

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:SPECtrogram:Y:MINimum:NEXT



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Spectrogram.Y.Minimum.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
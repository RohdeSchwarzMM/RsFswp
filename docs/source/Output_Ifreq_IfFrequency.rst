IfFrequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut<OutputConnector>:IF:IFFRequency

.. code-block:: python

	OUTPut<OutputConnector>:IF:IFFRequency



.. autoclass:: RsFswp.Implementations.Output.Ifreq.IfFrequency.IfFrequencyCls
	:members:
	:undoc-members:
	:noindex:
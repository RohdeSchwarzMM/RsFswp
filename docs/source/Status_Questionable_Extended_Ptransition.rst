Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:EXTended:PTRansition

.. code-block:: python

	STATus:QUEStionable:EXTended:PTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Extended.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex:
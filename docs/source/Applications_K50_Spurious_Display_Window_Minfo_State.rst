State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:MINFo:STATe

.. code-block:: python

	DISPlay:WINDow<Window>:MINFo:STATe



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Display.Window.Minfo.State.StateCls
	:members:
	:undoc-members:
	:noindex:
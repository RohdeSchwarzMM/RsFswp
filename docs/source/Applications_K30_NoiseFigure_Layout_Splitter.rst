Splitter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:SPLitter

.. code-block:: python

	LAYout:SPLitter



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Layout.Splitter.SplitterCls
	:members:
	:undoc-members:
	:noindex:
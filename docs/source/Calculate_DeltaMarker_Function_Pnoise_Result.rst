Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:PNOise:RESult

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:PNOise:RESult



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Function.Pnoise.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
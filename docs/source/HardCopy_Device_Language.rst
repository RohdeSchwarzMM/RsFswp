Language
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:DEVice:LANGuage

.. code-block:: python

	HCOPy:DEVice:LANGuage



.. autoclass:: RsFswp.Implementations.HardCopy.Device.Language.LanguageCls
	:members:
	:undoc-members:
	:noindex:
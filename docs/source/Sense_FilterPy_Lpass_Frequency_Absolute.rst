Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FILTer<FilterPy>:LPASs:FREQuency:ABSolute

.. code-block:: python

	SENSe:FILTer<FilterPy>:LPASs:FREQuency:ABSolute



.. autoclass:: RsFswp.Implementations.Sense.FilterPy.Lpass.Frequency.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex:
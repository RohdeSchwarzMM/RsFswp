Value
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:UPORt:VALue

.. code-block:: python

	INPut<InputIx>:UPORt:VALue



.. autoclass:: RsFswp.Implementations.InputPy.Uport.Value.ValueCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:PMETer<PowerMeter>:UPDate:STATe

.. code-block:: python

	SENSe:PMETer<PowerMeter>:UPDate:STATe



.. autoclass:: RsFswp.Implementations.Sense.Pmeter.Update.State.StateCls
	:members:
	:undoc-members:
	:noindex:
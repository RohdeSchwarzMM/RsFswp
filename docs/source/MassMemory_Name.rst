Name
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:NAME

.. code-block:: python

	MMEMory:NAME



.. autoclass:: RsFswp.Implementations.MassMemory.Name.NameCls
	:members:
	:undoc-members:
	:noindex:
State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:FILTer:YIG:STATe

.. code-block:: python

	INPut:FILTer:YIG:STATe



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.InputPy.FilterPy.Yig.State.StateCls
	:members:
	:undoc-members:
	:noindex:
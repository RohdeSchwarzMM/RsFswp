Scount
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ESPectrum<SubBlock>:SCOunt

.. code-block:: python

	SENSe:ESPectrum<SubBlock>:SCOunt



.. autoclass:: RsFswp.Implementations.Sense.Espectrum.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex:
Comment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:CVL:COMMent

.. code-block:: python

	SENSe:CORRection:CVL:COMMent



.. autoclass:: RsFswp.Implementations.Sense.Correction.Cvl.Comment.CommentCls
	:members:
	:undoc-members:
	:noindex:
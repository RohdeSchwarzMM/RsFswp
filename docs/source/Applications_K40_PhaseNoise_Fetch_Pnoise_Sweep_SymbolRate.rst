SymbolRate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:PNOise<Trace>:SWEep:SRATe

.. code-block:: python

	FETCh:PNOise<Trace>:SWEep:SRATe



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Fetch.Pnoise.Sweep.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex:
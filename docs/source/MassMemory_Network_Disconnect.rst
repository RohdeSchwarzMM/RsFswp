Disconnect
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:NETWork:DISConnect

.. code-block:: python

	MMEMory:NETWork:DISConnect



.. autoclass:: RsFswp.Implementations.MassMemory.Network.Disconnect.DisconnectCls
	:members:
	:undoc-members:
	:noindex:
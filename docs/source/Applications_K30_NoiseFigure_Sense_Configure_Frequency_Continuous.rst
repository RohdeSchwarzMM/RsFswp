Continuous
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CONFigure:FREQuency:CONTinuous

.. code-block:: python

	SENSe:CONFigure:FREQuency:CONTinuous



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Configure.Frequency.Continuous.ContinuousCls
	:members:
	:undoc-members:
	:noindex:
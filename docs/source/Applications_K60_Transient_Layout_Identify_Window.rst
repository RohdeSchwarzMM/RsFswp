Window
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LAYout:IDENtify:WINDow

.. code-block:: python

	LAYout:IDENtify:WINDow



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Layout.Identify.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:
Detector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:DETector

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:DETector



.. autoclass:: RsFswp.Implementations.Sense.ListPy.Range.Detector.DetectorCls
	:members:
	:undoc-members:
	:noindex:
Mnumber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CREFerence:HARMonics:MNUMber

.. code-block:: python

	SENSe:CREFerence:HARMonics:MNUMber



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Creference.Harmonics.Mnumber.MnumberCls
	:members:
	:undoc-members:
	:noindex:
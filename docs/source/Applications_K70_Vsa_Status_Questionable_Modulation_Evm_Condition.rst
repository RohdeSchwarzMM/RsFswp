Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:MODulation<Window>:EVM:CONDition

.. code-block:: python

	STATus:QUEStionable:MODulation<Window>:EVM:CONDition



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Status.Questionable.Modulation.Evm.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:
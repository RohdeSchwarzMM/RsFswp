Peak
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:SPECtrogram:Y:MINimum:PEAK

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:SPECtrogram:Y:MINimum:PEAK



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Spectrogram.Y.Minimum.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:
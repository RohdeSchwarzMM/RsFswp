Port<Port>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.applications.k50Spurious.sense.fplan.component.port.repcap_port_get()
	driver.applications.k50Spurious.sense.fplan.component.port.repcap_port_set(repcap.Port.Nr1)





.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Fplan.Component.Port.PortCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.sense.fplan.component.port.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Sense_Fplan_Component_Port_Frequency.rst
	Applications_K50_Spurious_Sense_Fplan_Component_Port_Mharmonic.rst
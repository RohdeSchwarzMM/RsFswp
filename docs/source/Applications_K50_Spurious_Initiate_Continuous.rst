Continuous
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:CONTinuous

.. code-block:: python

	INITiate:CONTinuous



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Initiate.Continuous.ContinuousCls
	:members:
	:undoc-members:
	:noindex:
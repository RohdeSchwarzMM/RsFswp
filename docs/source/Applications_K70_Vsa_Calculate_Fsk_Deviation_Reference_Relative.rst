Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:FSK:DEViation:REFerence:RELative

.. code-block:: python

	CALCulate<Window>:FSK:DEViation:REFerence:RELative



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Fsk.Deviation.Reference.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex:
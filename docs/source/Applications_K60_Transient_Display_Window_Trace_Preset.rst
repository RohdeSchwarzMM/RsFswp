Preset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:WINDow<Window>:TRACe<Trace>:PRESet

.. code-block:: python

	DISPlay:WINDow<Window>:TRACe<Trace>:PRESet



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Display.Window.Trace.Preset.PresetCls
	:members:
	:undoc-members:
	:noindex:
Next
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:SPECtrogram:Y:MINimum:NEXT

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:SPECtrogram:Y:MINimum:NEXT



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.DeltaMarker.Spectrogram.Y.Minimum.Next.NextCls
	:members:
	:undoc-members:
	:noindex:
Frame
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:SPECtrogram:FRAMe

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:SPECtrogram:FRAMe



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Spectrogram.Frame.FrameCls
	:members:
	:undoc-members:
	:noindex:
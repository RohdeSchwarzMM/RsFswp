Start
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:PNOise<Trace>:SWEep:STARt

.. code-block:: python

	FETCh:PNOise<Trace>:SWEep:STARt



.. autoclass:: RsFswp.Implementations.Applications.K40_PhaseNoise.Fetch.Pnoise.Sweep.Start.StartCls
	:members:
	:undoc-members:
	:noindex:
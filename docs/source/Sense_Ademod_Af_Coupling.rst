Coupling
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:AF:COUPling

.. code-block:: python

	SENSe:ADEMod:AF:COUPling



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Af.Coupling.CouplingCls
	:members:
	:undoc-members:
	:noindex:
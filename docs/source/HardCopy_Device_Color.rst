Color
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: HCOPy:DEVice:COLor

.. code-block:: python

	HCOPy:DEVice:COLor



.. autoclass:: RsFswp.Implementations.HardCopy.Device.Color.ColorCls
	:members:
	:undoc-members:
	:noindex:
LinkTo
----------------------------------------





.. autoclass:: RsFswp.Implementations.Calculate.Marker.LinkTo.LinkToCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.marker.linkTo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Marker_LinkTo_Marker.rst
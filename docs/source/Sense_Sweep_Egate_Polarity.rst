Polarity
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:EGATe:POLarity

.. code-block:: python

	SENSe:SWEep:EGATe:POLarity



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Egate.Polarity.PolarityCls
	:members:
	:undoc-members:
	:noindex:
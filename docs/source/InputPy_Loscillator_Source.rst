Source
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut<InputIx>:LOSCillator:SOURce

.. code-block:: python

	INPut<InputIx>:LOSCillator:SOURce



.. autoclass:: RsFswp.Implementations.InputPy.Loscillator.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.inputPy.loscillator.source.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	InputPy_Loscillator_Source_External.rst
Aoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FILTer<FilterPy>:AOFF

.. code-block:: python

	SENSe:FILTer<FilterPy>:AOFF



.. autoclass:: RsFswp.Implementations.Sense.FilterPy.Aoff.AoffCls
	:members:
	:undoc-members:
	:noindex:
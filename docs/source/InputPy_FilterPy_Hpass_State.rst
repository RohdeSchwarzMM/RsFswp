State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INPut:FILTer:HPASs:STATe

.. code-block:: python

	INPut:FILTer:HPASs:STATe



.. autoclass:: RsFswp.Implementations.InputPy.FilterPy.Hpass.State.StateCls
	:members:
	:undoc-members:
	:noindex:
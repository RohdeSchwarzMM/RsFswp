Bordering
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:BORDering

.. code-block:: python

	SENSe:DDEMod:BORDering



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Bordering.BorderingCls
	:members:
	:undoc-members:
	:noindex:
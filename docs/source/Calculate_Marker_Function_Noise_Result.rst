Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:NOISe:RESult

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:NOISe:RESult



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Noise.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
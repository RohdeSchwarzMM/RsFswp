Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EXTernal<ExternalGen>:POWer:LEVel

.. code-block:: python

	SOURce:EXTernal<ExternalGen>:POWer:LEVel



.. autoclass:: RsFswp.Implementations.Source.External.Power.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:
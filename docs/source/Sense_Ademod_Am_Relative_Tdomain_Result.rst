Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ADEMod:AM:RELative:TDOMain:RESult

.. code-block:: python

	SENSe:ADEMod:AM:RELative:TDOMain:RESult



.. autoclass:: RsFswp.Implementations.Sense.Ademod.Am.Relative.Tdomain.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
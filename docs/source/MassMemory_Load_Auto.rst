Auto
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MMEMory:LOAD:AUTO 1,

.. code-block:: python

	MMEMory:LOAD:AUTO 1,



.. autoclass:: RsFswp.Implementations.MassMemory.Load.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex:
Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:TRANsducer:DATA

.. code-block:: python

	SENSe:CORRection:TRANsducer:DATA



.. autoclass:: RsFswp.Implementations.Sense.Correction.Transducer.Data.DataCls
	:members:
	:undoc-members:
	:noindex:
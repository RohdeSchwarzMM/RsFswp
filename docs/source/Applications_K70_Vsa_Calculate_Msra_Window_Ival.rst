Ival
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MSRA:WINDow:IVAL

.. code-block:: python

	CALCulate<Window>:MSRA:WINDow:IVAL



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Msra.Window.Ival.IvalCls
	:members:
	:undoc-members:
	:noindex:
TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BWIDth:DEMod:TYPE

.. code-block:: python

	SENSe:BWIDth:DEMod:TYPE



.. autoclass:: RsFswp.Implementations.Sense.Bandwidth.Demod.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex:
Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:PPEak:RESult

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:FUNCtion:SUMMary:PPEak:RESult



.. autoclass:: RsFswp.Implementations.Calculate.Marker.Function.Summary.Ppeak.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:
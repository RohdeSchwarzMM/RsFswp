Cbar
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:ANNotation:CBAR

.. code-block:: python

	DISPlay:ANNotation:CBAR



.. autoclass:: RsFswp.Implementations.Display.Annotation.Cbar.CbarCls
	:members:
	:undoc-members:
	:noindex:
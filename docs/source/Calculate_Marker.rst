Marker<Marker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.calculate.marker.repcap_marker_get()
	driver.calculate.marker.repcap_marker_set(repcap.Marker.Nr1)





.. autoclass:: RsFswp.Implementations.Calculate.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calculate.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calculate_Marker_Aoff.rst
	Calculate_Marker_Count.rst
	Calculate_Marker_Function.rst
	Calculate_Marker_Link.rst
	Calculate_Marker_LinkTo.rst
	Calculate_Marker_LoExclude.rst
	Calculate_Marker_Maximum.rst
	Calculate_Marker_Minimum.rst
	Calculate_Marker_Pexcursion.rst
	Calculate_Marker_Spectrogram.rst
	Calculate_Marker_State.rst
	Calculate_Marker_Trace.rst
	Calculate_Marker_X.rst
	Calculate_Marker_Y.rst
	Calculate_Marker_Z.rst
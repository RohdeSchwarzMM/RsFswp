RsFswp Events
==========================

.. _Events:

Check the usage in the Getting Started chapter :ref:`here <GetingStarted_Events>`.

.. autoclass:: RsFswp.CustomFiles.events.Events()
   :members:
   :undoc-members:
   :special-members: enable_properties
   :noindex:

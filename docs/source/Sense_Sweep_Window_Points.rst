Points
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:WINDow<Window>:POINts

.. code-block:: python

	SENSe:SWEep:WINDow<Window>:POINts



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Window.Points.PointsCls
	:members:
	:undoc-members:
	:noindex:
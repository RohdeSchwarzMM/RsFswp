InputPy<InputIx>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.applications.k70Vsa.inputPy.repcap_inputIx_get()
	driver.applications.k70Vsa.inputPy.repcap_inputIx_set(repcap.InputIx.Nr1)





.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k70Vsa.inputPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K70_Vsa_InputPy_Attenuation.rst
	Applications_K70_Vsa_InputPy_Coupling.rst
	Applications_K70_Vsa_InputPy_Dpath.rst
	Applications_K70_Vsa_InputPy_FilterPy.rst
	Applications_K70_Vsa_InputPy_Gain.rst
	Applications_K70_Vsa_InputPy_Select.rst
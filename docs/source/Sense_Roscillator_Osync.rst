Osync
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:ROSCillator:OSYNc

.. code-block:: python

	SENSe:ROSCillator:OSYNc



.. autoclass:: RsFswp.Implementations.Sense.Roscillator.Osync.OsyncCls
	:members:
	:undoc-members:
	:noindex:
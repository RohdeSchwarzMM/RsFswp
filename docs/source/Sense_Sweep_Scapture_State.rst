State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:SWEep:SCAPture:STATe

.. code-block:: python

	SENSe:SWEep:SCAPture:STATe



.. autoclass:: RsFswp.Implementations.Sense.Sweep.Scapture.State.StateCls
	:members:
	:undoc-members:
	:noindex:
Component<Component>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix32
	rc = driver.applications.k50Spurious.sense.fplan.component.repcap_component_get()
	driver.applications.k50Spurious.sense.fplan.component.repcap_component_set(repcap.Component.Ix1)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:FPLan:COMPonent<Component>:DELete

.. code-block:: python

	SENSe:FPLan:COMPonent<Component>:DELete



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Fplan.Component.ComponentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k50Spurious.sense.fplan.component.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K50_Spurious_Sense_Fplan_Component_Add.rst
	Applications_K50_Spurious_Sense_Fplan_Component_Bcenter.rst
	Applications_K50_Spurious_Sense_Fplan_Component_Bspan.rst
	Applications_K50_Spurious_Sense_Fplan_Component_Factor.rst
	Applications_K50_Spurious_Sense_Fplan_Component_Identity.rst
	Applications_K50_Spurious_Sense_Fplan_Component_Port.rst
	Applications_K50_Spurious_Sense_Fplan_Component_TypePy.rst
Right
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:X:SLIMits:RIGHt

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:X:SLIMits:RIGHt



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Marker.X.Slimits.Right.RightCls
	:members:
	:undoc-members:
	:noindex:
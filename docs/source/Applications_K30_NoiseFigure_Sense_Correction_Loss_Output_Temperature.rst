Temperature
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:LOSS:OUTPut:TEMPerature

.. code-block:: python

	SENSe:CORRection:LOSS:OUTPut:TEMPerature



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Loss.Output.Temperature.TemperatureCls
	:members:
	:undoc-members:
	:noindex:
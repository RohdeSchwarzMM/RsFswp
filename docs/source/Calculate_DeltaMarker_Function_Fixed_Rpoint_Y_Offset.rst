Offset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:FIXed:RPOint:Y:OFFSet

.. code-block:: python

	CALCulate<Window>:DELTamarker<DeltaMarker>:FUNCtion:FIXed:RPOint:Y:OFFSet



.. autoclass:: RsFswp.Implementations.Calculate.DeltaMarker.Function.Fixed.Rpoint.Y.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex:
Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CORRection:LOSS:OUTPut:TABLe:DATA

.. code-block:: python

	SENSe:CORRection:LOSS:OUTPut:TABLe:DATA



.. autoclass:: RsFswp.Implementations.Applications.K30_NoiseFigure.Sense.Correction.Loss.Output.Table.Data.DataCls
	:members:
	:undoc-members:
	:noindex:
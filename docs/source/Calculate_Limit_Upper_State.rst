State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:LIMit<LimitIx>:UPPer:STATe

.. code-block:: python

	CALCulate<Window>:LIMit<LimitIx>:UPPer:STATe



.. autoclass:: RsFswp.Implementations.Calculate.Limit.Upper.State.StateCls
	:members:
	:undoc-members:
	:noindex:
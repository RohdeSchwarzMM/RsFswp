Calculate<Window>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.applications.k60Transient.calculate.repcap_window_get()
	driver.applications.k60Transient.calculate.repcap_window_set(repcap.Window.Nr1)





.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.CalculateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k60Transient.calculate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K60_Transient_Calculate_ChrDetection.rst
	Applications_K60_Transient_Calculate_DeltaMarker.rst
	Applications_K60_Transient_Calculate_Distribution.rst
	Applications_K60_Transient_Calculate_HopDetection.rst
	Applications_K60_Transient_Calculate_Marker.rst
	Applications_K60_Transient_Calculate_Spectrogram.rst
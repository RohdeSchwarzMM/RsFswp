Bandwidth
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:COUPle:BWIDth

.. code-block:: python

	INSTrument:COUPle:BWIDth



.. autoclass:: RsFswp.Implementations.Instrument.Couple.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex:
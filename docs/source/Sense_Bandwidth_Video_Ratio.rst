Ratio
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BWIDth:VIDeo:RATio

.. code-block:: python

	SENSe:BWIDth:VIDeo:RATio



.. autoclass:: RsFswp.Implementations.Sense.Bandwidth.Video.Ratio.RatioCls
	:members:
	:undoc-members:
	:noindex:
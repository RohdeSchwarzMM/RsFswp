Segment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:TRANsfer:SEGMent

.. code-block:: python

	SENSe:TRANsfer:SEGMent



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.Transfer.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:
Nstate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:DDEMod:PATTern:PSK:NSTate

.. code-block:: python

	SENSe:DDEMod:PATTern:PSK:NSTate



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Sense.Ddemod.Pattern.Psk.Nstate.NstateCls
	:members:
	:undoc-members:
	:noindex:
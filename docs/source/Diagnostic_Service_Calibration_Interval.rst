Interval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:CALibration:INTerval

.. code-block:: python

	DIAGnostic:SERVice:CALibration:INTerval



.. autoclass:: RsFswp.Implementations.Diagnostic.Service.Calibration.Interval.IntervalCls
	:members:
	:undoc-members:
	:noindex:
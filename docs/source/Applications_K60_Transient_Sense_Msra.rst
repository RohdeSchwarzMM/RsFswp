Msra
----------------------------------------





.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Sense.Msra.MsraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.applications.k60Transient.sense.msra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Applications_K60_Transient_Sense_Msra_Capture.rst
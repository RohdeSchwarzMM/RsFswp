Pexcursion
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:LIST:RANGe<RangePy>:PEXCursion

.. code-block:: python

	SENSe:LIST:RANGe<RangePy>:PEXCursion



.. autoclass:: RsFswp.Implementations.Applications.K50_Spurious.Sense.ListPy.Range.Pexcursion.PexcursionCls
	:members:
	:undoc-members:
	:noindex:
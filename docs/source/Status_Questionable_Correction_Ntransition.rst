Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:CORRection:NTRansition

.. code-block:: python

	STATus:QUEStionable:CORRection:NTRansition



.. autoclass:: RsFswp.Implementations.Status.Questionable.Correction.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex:
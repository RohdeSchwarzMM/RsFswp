Right
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:MARKer<Marker>:MINimum:RIGHt

.. code-block:: python

	CALCulate<Window>:MARKer<Marker>:MINimum:RIGHt



.. autoclass:: RsFswp.Implementations.Applications.K60_Transient.Calculate.Marker.Minimum.Right.RightCls
	:members:
	:undoc-members:
	:noindex:
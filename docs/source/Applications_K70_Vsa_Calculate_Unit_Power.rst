Power
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate<Window>:UNIT:POWer

.. code-block:: python

	CALCulate<Window>:UNIT:POWer



.. autoclass:: RsFswp.Implementations.Applications.K70_Vsa.Calculate.Unit.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: